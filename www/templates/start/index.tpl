<!DOCTYPE html>
<html lang="en">
<head>
    <title>AutoTech R&D of electronic systems and devices</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Sansita" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="[SRC_TEMPLATE_PATH]/style.css">
    <link rel="stylesheet" type="text/css" href="[SRC_TEMPLATE_PATH]/private.css">
    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/react/0.14.0/react.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/react/0.14.0/react-dom.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/babel-core/5.6.15/browser.js"></script>
    <script src="[SRC_TEMPLATE_PATH]/js/function.js"></script>
    <script src="https://www.google.com/recaptcha/api.js?hl=[SITE_LANG]"></script>
</head>
<body id="myPage" data-spy="scroll" data-target=".navbar" data-offset="60">

    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a href="[MY_PAGE][GET_LNG]">
                    <img src="[SRC_TEMPLATE_PATH]/img/AT_small.png" width="43" height="43" alt="AutoTech" style="margin-top:3px" />
                </a>

                    <a class="navbar-brand" href="#myPage"></a>
            </div>
            <div class="collapse navbar-collapse" id="myNavbar">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="[START_NAVI][GET_LNG]#about">[M_ABOUT]</a></li>
                    <!--<li><a href="#services">SERVICES</a></li>-->
                    <!--<li><a href="#portfolio">ПОРТФОЛИО</a></li>
                    <li><a href="#pricing">ЦЕНЫ</a></li>-->
                    <li><a href="[START_NAVI][GET_LNG]#contact">[M_CONTACTS]</a></li>
                    <li data-point="news"><a href="/news/[GET_LNG]">[M_NEWS]</a></li>
                    <li data-point="developments"><a href="/developments/[GET_LNG]">[M_DEVS]</a></li>
                    [M_ACCOUNT]
                    [M_ADMIN]
                    <li data-point="[M_LOG_IN_LNK]"><a href="/[M_LOG_IN_LNK]/[GET_LNG]">[M_LOG_IN]</a></li>
                    <li><a class="capittext" href="?lng=en">ENG</a></li>
                    <li><a class="capittext" href="?lng=ru">РУС</a></li>
                    <!--<li><a href="/en">ENGLISH</a></li>-->
                </ul>
            </div>
        </div>
    </nav>
[CONTENT]
    <footer class="container-fluid text-center">
        [TO_TOP]
        <p>[FOLLOW_US]</p>
        <p><a href="https://www.facebook.com/autotech39/" title="Folow us on Facebook"><img src="[SRC_TEMPLATE_PATH]/img/facebook.png" width="50" height="50" /></a></p>
        <p>АВТОТЕХ 2017</p>
    </footer>
    <script>
        var path = location.pathname;
        var pathArr = path.split('/');
        var point = pathArr[0]==''?pathArr[1]:pathArr[0];
        //alert(point);
        $('[data-point="'+point+'"]').addClass('active');
        //alert($('[data-point = '+point+']').html());
    </script>

</body>
</html>
