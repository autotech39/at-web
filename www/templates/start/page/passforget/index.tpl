<div class="page_container">
    <h2 class="grey-text">[RESET_PASSWORD]</h2>
    [ERROR_TEXT]
    <form action="[ACTION]" name="login-form" id="login-form" method="post" onsubmit="return sendForm(this);">
        <div class="row">
            <div class="col-md-12 text-center center-group">
                <div style="display: inline-block; max-width: 270px;">
                <div class="form-group">
                    <div class="input-group" style="width: 280px;text-align: right;">
                        <span class="input-group-addon">@</span>
                        <input type="email" class="form-control" name="email" id="email" placeholder="[EMAIL]" />
                    </div>
                </div>
                <div class="form-group">
                    <div class="rcpts[SEC_ERROR_CLASS]">
                        <div class="g-recaptcha" data-size="normal" style="transform:scaleX(0.92);transform-origin:0 0;" data-sitekey="[RECAPTCHA]"></div>
                    </div>
                </div>
                <div class="form-group">
                    <input type="hidden" name="referer" id="referer" value="[REFERER]" />
                    <input class="btn btn-primary wide-button-fix200 top-10" type="submit" name="signin" id="signin" value="[RESET_PASSWORD]">
                </div>
                </div>
            </div>
        </div>
    </form>
    <input type="hidden" id="checkcpt" value="0" />
</div>
<script>
function sendForm(th) {
    //var thid = th.id;
    //alert(th.id);
    var inputs = document.querySelectorAll('#'+th.id+' input');
    var j = 0;
    for (var i=0; i<inputs.length; i++) {
        var input = inputs[i].value;
        if (isEmpty(input)) {
            //alert(inputs[i].id);
            inputs[i].classList.add('red-light');
            if (inputs[i].id!='referer')  {
                j++;
            }
        }
    }
    //alert(j);
    //alert($('#password').val()+' || '+$('#re-password').val());
    //return false;
    if (j>0) {
        return false;
    }
    else {
        return true;
    }
    
    //alert(window.checkCaptcha);
    //return false;
}
$('input.signform').on('change',function(){
    if($(this).val().length>0) {
        $(this).removeClass('red-light');
    }
    //console.log($(this).val().length);
});
</script>