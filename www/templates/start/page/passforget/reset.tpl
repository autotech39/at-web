<div class="page_container">
    <h2 class="grey-text">[RESET_PASSWORD]</h2>
    [ERROR_TEXT]
    <form action="[ACTION]" name="login-form" id="login-form" method="post" onsubmit="return sendForm(this);">
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="form-group" style="width: 280px;margin: auto;">
                    <input type="password" class="form-control" name="password" id="password" placeholder="[PASSWORD]" />
                    <input type="password" class="form-control" name="re_password" id="re_password" placeholder="[RE_PASSWORD]" />
                    <br />
                    <div class='rcpts[SEC_ERROR_CLASS]'>
                        <div class="g-recaptcha" data-size="normal" style="transform:scaleX(0.92);transform-origin:0 0;" data-sitekey="[RECAPTCHA]"></div>
                    </div>
                    <input type="hidden" name="restore" id="restore" value="[RESTORE]" />
                    <input type="submit" class="btn btn-primary wide-button-fix200" name="signin" id="signin" value="[SUBMIT]" />
                </div>
            </div>
        </div>
    </form>
    <input type="hidden" id="checkcpt" value="0" />
</div>
<script>
function sendForm(th) {
    //var thid = th.id;
    //alert(th.id);
    var inputs = document.querySelectorAll('#'+th.id+' input');
    var j = 0;
    for (var i=0; i<inputs.length; i++) {
        var input = inputs[i].value;
        if (isEmpty(input)) {
            //alert(inputs[i].id);
            inputs[i].classList.add('red-light');
            if (inputs[i].id!='referer')  {
                j++;
            }
        }
    }
    if (checkPassLength($('#password').val())===false) {
        $('#password').addClass('red-light');
        alert('[TOOSHORTPATH] (<6)');
        j++;
    }
    //alert(j);
    //alert($('#password').val()+' || '+$('#re-password').val());
    //return false;
    if (j>0) {
        return false;
    }
    else {
        return true;
    }
    
    //alert(window.checkCaptcha);
    //return false;
}
$('input.signform').on('change',function(){
    if($(this).val().length>0) {
        $(this).removeClass('red-light');
    }
    //console.log($(this).val().length);
});
</script>