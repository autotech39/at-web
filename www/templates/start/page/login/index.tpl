<div class="page_container">
    <h2 class="grey-text">[LOG_IN_PA]</h2>
    [ERROR_TEXT]
    <form action="[ACTION]" name="login-form" id="login-form" method="post" onsubmit="return sendForm(this);" role="form">
        <div class="row">
            <div class="col-md-3 col-md-offset-3 col-sm-12 col-xs-12">
                <div class="form-group left-group" style="max-width: 270px;">
                    <div class="input-group">
                        <span class="input-group-addon glyphicon glyphicon-user preinput"></span>
                        <input type="text" class="form-control[UP_ERROR_CLASS]"  name="login" id="login" placeholder="[LOGIN]" value="[UNAME]" />
                    </div>
                    <input type="password" class="form-control[UP_ERROR_CLASS] top-10" name="password" id="password" placeholder="[PASSWORD]">
                </div>
            </div>
            <div class="col-md-3 col-sm-12 col-xs-12">
                <div class="form-group">
                    <div class='rcpts[SEC_ERROR_CLASS]'>
                        <div class="g-recaptcha" data-size="normal" style="transform:scaleX(0.92);transform-origin:0 0;" data-sitekey="[RECAPTCHA]"></div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <input type="hidden" name="referer" id="referer" value="[REFERER]" />
            </div>
        </div>
        <div class="row top-10">
            <div class="col-md-12 text-center">
                <div class="form-group">
                    <input class="btn btn-primary wide-button-fix200" type="submit" name="signin" id="signin" value="[SIGN_IN]">
                </div>
            </div>
        </div>
    </form>
    <input type="hidden" id="checkcpt" value="0" />    
    <div class="row">
        <div class="col-md-12 text-center">
            <p>
                <a class="link-button red-text wide-button-fix200" href="/passforget/">[PASS_FORGET]</a>
            </p>
            <input type="hidden" id="checkcpt" value="0" />
            <a class="btn btn-default wide-button-fix200" href="/registration/">[SIGN_UP]</a>
        </div>
    </div>
</div>
<script>
function sendForm(th) {
    //var thid = th.id;
    //alert(th.id);
    var inputs = document.querySelectorAll('#'+th.id+' input');
    var j = 0;
    for (var i=0; i<inputs.length; i++) {
        var input = inputs[i].value;
        if (isEmpty(input)) {
            //alert(inputs[i].id);
            inputs[i].classList.add('red-light');
            if (inputs[i].id!='referer')  {
                j++;
            }
        }
    }
    //alert(j);
    //alert($('#password').val()+' || '+$('#re-password').val());
    //return false;
    if (j>0) {
        return false;
    }
    else {
        return true;
    }
    
    //alert(window.checkCaptcha);
    //return false;
}
$('input.signform').on('change',function(){
    if($(this).val().length>0) {
        $(this).removeClass('red-light');
    }
    //console.log($(this).val().length);
});
</script>