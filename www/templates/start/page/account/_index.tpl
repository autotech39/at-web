<div class="page_container" style="padding: 50px 0px 0px 0px;">
    
    <div id="accountbox">
        <div id="accountmenu">
            <div class="menucontent">
                <ul>
                    <li class="menubuttonblock">
                        <span class="menubutton">&#9776;</span>
                    </li>
                    <li class="getcontent" data-menu="message">
                        <span>[MESSAGESOFASO]</span>
                        <div class="accmenu-icons icon-message"></div>
                    </li>
                    <li class="getcontent" data-menu="status">
                        <span>[STATUSESOFASO]</span>
                        <div class="accmenu-icons icon-status"></div>
                    </li>
                    <li class="getcontent" data-menu="update">
                        <span>[SOFTWAREUPDATE]</span>
                        <div class="accmenu-icons icon-update"></div>
                    </li>
                    <li class="getcontent" data-menu="sending">
                        <span>[DISTRIBUTION_OPTIONS]</span>
                        <div class="accmenu-icons icon-sending"></div>
                    </li>
                    <li class="getcontent" data-menu="settings">
                        <span>[SETTINGS]</span>
                        <div class="accmenu-icons icon-setting"></div>
                    </li>
                </ul>
            </div>
        </div>
        <div id="accountblock">
        </div>
    </div>
    
</div>
<script>
function setContentBoxHeight() {
    var height = document.documentElement.clientHeight-225;
    $('#accountbox').height(height+'px');
}
setContentBoxHeight();
$(window).resize(function(){
    setContentBoxHeight();
});
var hash = window.location.hash;
var names = ['settings','message','status','update','sending'];
var name = '';
if(hash==='') {
    name = 'settings';
}
else {
    var hashArr = hash.split('#');
    var preNameArr = hashArr[1].split(';');
    if (names.indexOf(preNameArr[0])>-1) {
        name = preNameArr[0];
    }
    else {
        name = 'settings';
    }
}
$('li.getcontent').removeClass('red-bg');
$('li.getcontent[data-menu="'+name+'"]').addClass('red-bg');
$('#accountblock').load('/ajax/account/'+name+'.php');
$('li.getcontent').click(function(){
    var name = $(this).attr('data-menu');
    window.location.hash = name;
    $('#accountblock').html('Load...');
    $('li.getcontent').removeClass('red-bg');
    $(this).addClass('red-bg');
    $('#accountblock').load('/ajax/account/'+name+'.php');
});

</script>