<div class="page_container">
    <h2 class="grey-text">[REGISTRATION_RESULT]</h2>
    <p>
        [REG_RESULT_TEXT]
    </p>
    <!--<a class="white-button signform" href="/login/">[SIGN_IN]</a>-->
    <div class="row">
        <div class="col-md-12 text-center">
            <div class="form-group">
                <a href="[BUTTON_LNK]" class="btn [BUTTON_CLASS] wide-button">[BUTTON_TEXT]</a>
            </div>
        </div>
    </div>
</div>