<div class="page_container" style="text-align: center;">
    <h2 class="grey-text">[SIGN_UP]</h2>
    <form action="[ACTION]" name="registration-form" id="registration-form" method="post" onsubmit="return sendForm(this);" role="form">
        <div class="row" style="margin-bottom: 20px;">
            <div class="col-md-3 col-md-offset-3 col-sm-12 col-xs-12">
                <div class="form-group left-group" style="max-width: 270px;">
                    <div class="input-group">
                        <span class="input-group-addon glyphicon glyphicon-user preinput"></span>
                        <input type="text" class="form-control"  name="login" id="login" placeholder="[LOGIN]">
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">@</span>
                        <input type="email" class="form-control" name="email" id="email" placeholder="[EMAIL]">
                    </div>
                    <input type="password" class="form-control" name="password" id="password" placeholder="[PASSWORD]">
                    <input type="password" class="form-control" name="re-password" id="re-password" placeholder="[RE_PASSWORD]" />
                </div>
            </div>
            <div class="col-md-3 col-sm-12 col-xs-12 text-left">
                <div class="form-group">
                    <div class='rcpt'>
                        <div class="g-recaptcha" data-size="compact" data-sitekey="[RECAPTCHA]"></div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="form-group">
                    <input class="btn btn-primary wide-button-fix200" type="submit" name="signup" id="signup" value="[SIGN_UP]">
                </div>
            </div>
        </div>
    </form>
    <input type="hidden" id="checkcpt" value="0" />
</div>
<script>
/*$('#recaptcha-anchor').click(function(){
    //alert('!');
    console.log('!!');
});*/
//$('iframe').addClass('ifr');
/*$('.ifr').load(function(){
    $(this).contents().find('#recaptcha-anchor').click(function(){
        console.log('clicked');
    });
});*/

function sendForm(th) {
    //var thid = th.id;
    var inputs = document.querySelectorAll('#'+th.id+' input');
    var j = 0;
    for (var i=0; i<inputs.length; i++) {
        var input = inputs[i].value;
        if (isEmpty(input)) {
            //alert(inputs[i].id);
            inputs[i].classList.add('red-light');
            j++;
        }
    }
    //alert($('#password').val()+' || '+$('#re-password').val());
    //return false;
    if ($('#password').val()!=$('#re-password').val()||checkPassLength($('#password').val())===false) {
        $('#password').addClass('red-light');
        $('#re-password').addClass('red-light');
        j++;
        if (checkPassLength($('#password').val())===false) {
            alert('[TOOSHORTPATH] (<6)');
        }        
    }
    if (j>0) {
        return false;
    }
    else {
        /*var formId = th.id;
        var param = $('#'+formId).serialize();
        var src = '[AJAX_ACTION]';
        var result = syncAjax (src,param,'post');
        if (result=='1') {
            $('.g-recaptcha').removeClass('red-light');
            return true;
        }
        else {
            $('.g-recaptcha').addClass('red-light');
            setTimeout(function(){
                $('.g-recaptcha').removeClass('red-light');
            }, 4000);
            return false;
        }*/
        return true;
    }
    
    //alert(window.checkCaptcha);
    //return false;
}
$('input.signform').on('change',function(){
    if($(this).val().length>0) {
        $(this).removeClass('red-light');
    }
    //console.log($(this).val().length);
});
/*function checkPass(pass) {
    if (checkPassLength(pass))
}*/

</script>