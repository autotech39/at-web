<div class="jumbotron text-center bg-grey">
        <h1>Автомобильная система оповещений</h1>
        <!--<img src="autotech.png" width="300" height="300" alt="AutoTech" style="margin-top:3px" /> -->
        <!--
        <p>Исследования и разработки электронных систем и программного обеспечения</br>в области безопасности и коммуникации на транспорте</p>
        <p></p>
        --!>
        <form>
            <!--
            <div class="input-group">
                <input type="email" class="form-control" size="50" placeholder="Email Address" required>
                <div class="input-group-btn">
                    <button type="button" class="btn btn-danger">Подписаться</button>
                </div>
            </div>

        </form>-->
    </div>

    <!-- Container (About Section) -->
    <div id="why" class="container-fluid">
        <div class="row">
            <div class="col-sm-8">
                
                <h2>О ПРОДУКТЕ</h2><br>
                <h4>СИСТЕМА АВАРИЙНЫХ АВТОМОБИЛЬНЫХ ОПОВЕЩЕНИЙ, СОВМЕСТИМАЯ С СИСТЕМОЙ ВЫЗОВА ЭКСТРЕННЫХ СЛУЖБ ГЛОНАСС-GPS</h4><br>
                <span class="glyphicon glyphicon-question-sign logo-small slideanim"></span>
                <h2>Почему мы решили это сделать?</h2>
                <p>
                    Требования технического регламента Таможенного союза «О безопасности колесных транспортных средств»
                </p>
                <p> <strong>А так же...</strong></p>
                <p>Часто припаркованный автомобиль мешает проезду во дворах или узких улицах…</p>
                <p>   Иногда автомобиль эвакуируют вопреки правилам…</p>
                <p>   Не всегда водитель может сам вызвать помочь при ДТП…</p>
                <p>    При пожаре в автомобиле владелец слишком поздно узнает о происшествии…</p>
                <p>   Иногда требуется дополнительные доказательства в спорных ситуациях…</p>
                <p>    В чужой стране или чужом городе необходимо знать о состоянии автомобиля…</p>
                <p>Невозможность отслеживать текущее состояние автомобиля в реальном времени</p>
                
                <!--<br><button class="btn btn-default btn-lg">Get in Touch</button>-->
            </div>
            <div class="col-sm-4">
               
                <img src="/[TEMPLATE_PATH]/img/car-1.png" width="408" height="229" alt="car" style="margin-top:200px" />
            </div>
        </div>
    </div>
    
    <div class="container-fluid bg-grey">
        <div class="row">
            <div class="col-sm-4">
                <span class="glyphicon glyphicon-info-sign logo slideanim"></span>
            </div>
            <div class="col-sm-8">
                <h2>Что мы решили сделать...</h2><br>
                <p>
                    Оповестить владельца авто о том, что его вызывают к машине (Автомобиль мешает проезду или хотят эвакуировать)
                </p>
                <p>
                    Отправить сообщение владельцу авто об угрозе эвакуации
                </p>
                <p>
                    Сохранять GPS метки мест. с которых эвакуирован автомобиль и куда эвакуирован
                </p>
                <p>
                    Контролировать бортовую сеть на возникновение дуги (плохого контакта) – предпосылки пожара
                </p>
                <p>
                    Хранить метки GPS перемещения автомобиля в личном кабинете на Интернет-портале и мобильном приложении
                </p>
                <p>
                    Применять автодозвон на заранее запрограммированные телефонные номера при определенных событиях
                </p>
                <p>
                    Самостоятельно вызвать оперативные службы в соответствии с ГОСТ Р 54620-2011
                </p>
                <p>
                    Система полного логирования поведения и состояния автомобиля
                </p>
                <p>
                    Система прогнозирования состояний узлов и агрегатов ТС
                </p>
                <p>
                    Публичные ссылки на историю владения ТС
                </p>
                <p>     Управление настройками устройства с мобильного приложения или онлайн-сервиса</p>


            </div>
        </div>
    </div>


    <div id="did" class="container-fluid">
        <div class="row">
            <div class="col-sm-8">

                <span class="glyphicon glyphicon-ok-sign logo-small slideanim"></span>
                <h2>Что мы предлагаем...</h2>
                <p>Устройство не имеет привязки к марке и комплектации автомобиля</p>
                <p>Высокий потенциал развития  функциональных возможностей комплекса</p>
                <p>Устройство может использоваться на любом транспорте! На легковом, грузовом и даже общественном!</p>
                <p>Не имеет ценности для грабителей</p>
                <p>Соответствие ГОСТ Р 54620-2011</p>
                <p>Личный кабинет на онлайн-сервисе</p>
                <p>Мобильное приложение</p>
                <p>НИЗКАЯ СТОИМОСТЬ!</p>

                <!--<br><button class="btn btn-default btn-lg">Get in Touch</button>-->
            </div>
            <div class="col-sm-4">
                <img src="/[TEMPLATE_PATH]/img/soft.png" width="400" height="500" alt="soft" />

            </div>
        </div>
    </div>
    <footer class="container-fluid text-center">
        <h2>Свяжитесь с нами для получения подробной информации</h2>
        <a href="#myPage" title="To Top">
            <span class="glyphicon glyphicon-chevron-up"></span>
        </a>
        
    </footer>
    <script>
        $(document).ready(function () {
            // Add smooth scrolling to all links in navbar + footer link
            /*$(".navbar a, footer a[href='#myPage']").on('click', function (event) {
                // Make sure this.hash has a value before overriding default behavior
                if (this.hash !== "") {
                    // Prevent default anchor click behavior
                    event.preventDefault();

                    // Store hash
                    var hash = this.hash;

                    // Using jQuery's animate() method to add smooth page scroll
                    // The optional number (900) specifies the number of milliseconds it takes to scroll to the specified area
                    $('html, body').animate({
                        scrollTop: $(hash).offset().top
                    }, 900, function () {

                        // Add hash (#) to URL when done scrolling (default click behavior)
                        window.location.hash = hash;
                    });
                } // End if
            });*/

            $(window).scroll(function () {
                $(".slideanim").each(function () {
                    var pos = $(this).offset().top;

                    var winTop = $(window).scrollTop();
                    if (pos < winTop + 600) {
                        $(this).addClass("slide");
                    }
                });
            });
        })
    </script>