function isEmpty(val) {
    if (val===''||val===undefined||val=='undefined'||val===false||val===0||val===null) {
        return true;
    }
    else {
        return false;
    }
}

function aXhr () {
    try {
        xhr = new ActiveXObject("Msxml2.XMLHTTP");
    } catch (e) {
        try {
            xhr = new ActiveXObject("Microsoft.XMLHTTP");
        } catch (E) {
            xhr = false;
        }
    }
    if (!xhr && typeof XMLHttpRequest!='undefined') {
        xhr = new XMLHttpRequest();
    }
    return xhr;
}


function getAjax (src,param,method='get',async=false) {
    if (method!='post'&&method!='POST') {
        method = 'get';
        src = src+'?'+param;
    }
    return new Promise(function(resolve, reject) {
		var axhr = aXhr();
		axhr.open(method, src, async);
		axhr.onreadystatechange = function() {
			if(axhr.readyState === 4) {
				resolve(axhr.responseText);
			}
		};
        if(method=='get'||method=='GET') {
            console.log(src);
            axhr.send(null);
        }
        else {
            console.log(param);
            axhr.send(param);
        }
	});
}

function syncAjax (src,param,meth) { //Синхронный ajax-запрос
    var aXhr = window.aXhr();
    var sajax_result;
    if (meth===''||meth=='POST'||meth=='post'||meth=='undefined'||meth=='null'||meth=='false'||meth===undefined||meth===null||meth===false) {
        aXhr.onload = aXhr.onerror = function() {
            if (this.status == 200) {
                sajax_result = this.responseText;
            } else {
                sajax_result = false;
            }
        }
        aXhr.open("POST", src, false);
        aXhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        aXhr.send(param);
    }
    else if (meth=='get'||meth=='GET') {
        if (param!==''&&param!='undefined'&&param!==undefined) {
            param = '?'+param;
        }
        aXhr.onload = aXhr.onerror = function() {
            if (this.status == 200) {
                sajax_result = this.responseText;
              
            } else {
                sajax_result = false;
            }
        }
        aXhr.open("GET", src+param, false);
        aXhr.send(null);
    }
    return sajax_result;
}

function checkPassLength(pass,min=6) {
    var ln = 0;
    if (!isEmpty(pass)) {
        ln = pass.length;
    }
    var result = false;
    if (ln>=min) {
        result = true;
    }
    return result;
}


function showSubPage(sub,devid,devnum) {
    $('#datepickers-container').detach();
    $('#account-content-app-box').html(ln.load);
    if (sub=='basic') {
        localStorage.removeItem('start');
        localStorage.removeItem('end');
    }
    var content = syncAjax('/ajax/account/app/app-content-'+sub+'.php?devid='+devid+'&devnum='+devnum);
    $('#account-content-app-box').html(content);
}