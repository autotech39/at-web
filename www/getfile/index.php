<?if (empty($_REQUEST['type'])):?>
<?
    header('Content-disposition: attachment; filename='.$_REQUEST['file'].'.txt');
    header('Content-type: text/plain');
    if(!empty($_REQUEST['data'])) {
        $newline=chr(10);
        //echo $_REQUEST['data'];
        $dataArr = explode('\r\n',$_REQUEST['data']);
        foreach ($dataArr as $data) {
            echo $data.$newline;
        }
    }
?>
<?elseif ($_REQUEST['type']=='xls'):?>
<?
    header('Cache-Control: no-store, no-cache, must-revalidate');
    header('Cache-Control: post-check=0, pre-check=0', FALSE);
    header('Pragma: no-cache');
    header('Content-transfer-encoding: binary');
    header('Content-Disposition: attachment; filename='.$_REQUEST['file'].'.xls');
    header('Content-Type: application/vnd.ms-excel');
?>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=Windows-1251">
    <title><?=iconv('UTF-8','Windows-1251','Дебиторы-Кредиторы');?></title>
    <style>
        .bold {
            font-weight: bold;
        }
        .border_one td {
            border: 1px solid #000;
        }
    </style>
</head>
<body>
    <?=iconv('UTF-8','Windows-1251',$_REQUEST['data']);?>
</body>
</html>
<?elseif ($_REQUEST['type']=='xml'):?>
<?
    header("Content-Type: text/xml");
    header("Cache-Control: no-cache, must-revalidate");
    header("Cache-Control: post-check=0,pre-check=0");
    header("Cache-Control: max-age=0");
    header("Pragma: no-cache");
    header('Content-Disposition: attachment; filename='.$_REQUEST['file'].'.xml');
    echo '<?xml version="1.0" encoding="WINDOWS-1251" ?>';
?>
<!--<xsl:stylesheet xmlns:xsl="http://www.w3.org/TR/WD-xsl" version="1.0">
<xsl:template match="/">-->
    <?=iconv('UTF-8','Windows-1251',$_REQUEST['data']);?>
<!--</xsl:template>
</xsl:stylesheet>-->
<?endif;?>
<?
//print_r($_REQUEST);
?>