<?
include('core/config.php');
$core = new core;
$page_core = new core;

$inclURL = url_parser();

$page_output = '';
$admin_block = '';
$show_admin = 0;


if(file_exists($inclURL['INCL'])) {
	ob_start();
	$page_output = incl_page($inclURL['OUT_FILE'],$inclURL['OUT_PATH'],$page_core);
	ob_end_clean();
}
elseif ($_SERVER['REQUEST_URI']=='/'&&file_exists(ROOT_PATH.'/'.PAGE_PATH.'/'.START_PAGE.'.php')) {
	ob_start();
	$page_output = incl_page(START_PAGE.'.php','/'.PAGE_PATH,$page_core);
	ob_end_clean();
}
?>