<?
class core {
	private $input = array();
	public $data;
	public function read($template) {
		if(empty($template) || !file_exists($template)){
			//exit("Not found template '".str_replace(ROOT_PATH,'',$template)."'");
			exit('Template not found');
		}
		else {
			$this->data = file_get_contents($template);
		}
	}
	public function set_data($key,$value) {
		$this->input[$key] = $value;
	}
	public function write() {
		foreach($this->input as $data_key => $data_value) {
			$this->data = str_replace($data_key, $data_value, $this->data);
		}
		//$cacheFile = cacheStart();
		print $this->data;
		//cacheConfirm($cacheFile);
	}
}
class modules {
	public $onlist = '';
	public $styles = '';
	public $includes = '';
	public $usecss = false;
	public function On ($path="modules") {
		$onArray = array();
		if (!empty($this->onlist)) {
			$onArray = explode(',',$this->onlist);
		}
		//print_r($onArray);
		foreach ($onArray as $module) {
			if (file_exists(ROOT_PATH.'/'.$path.'/'.$module.'/index.php')) {
				$this->includes .= "include_once('".ROOT_PATH."/".$path.'/'.$module."/index.php');";
				if ($this->usecss===true&&file_exists(ROOT_PATH.'/'.$path.'/'.$module.'/style.css')) {
					$this->styles .= '<link rel="stylesheet" type="text/css" href="http://'.SITE_URL.'/'.$path.'/'.$module.'/style.css">';
				}
			}
			
		}
		return $this->includes;
	}
}

class pageinfo {
	public $uri;
	public $id;
	public $title;
	public $description;
	public $keywords;
	public $canonical;
	public $head = '';
	private $data;
	
	private function get() {
		if (!empty($this->id)) {
			$where = "`id` = '".$this->id."'";
		}
		elseif (!empty($this->uri)) {
			if($this->uri!='/'&&substr($this->uri,-1,1)=='/') {
				$this->uri = substr($this->uri,0,-1);
			}
			$where = "`uri` = '".$this->uri."'";
		}
		else {
			return false;
		}
		if (class_exists('dbconnect')) {
			$MyDB = new dbconnect;
			$MyDB->Connect();
			$MyDB->Text = "SELECT `title`, `description`, `keywords`, `canonical` FROM `page` WHERE ".$where;
			$MyDB->Query();
			$MyDB->Assoc();
			$this->data = $MyDB->Data;
			
			return $this->data;
		}
		else {
			return false;
		}
		
	}
	
	public function header() {
		if($this->get()) {
			$this->title = $this->data[0]['title'];
			$this->description = $this->data[0]['description'];
			$this->keywords = $this->data[0]['keywords'];
			$this->canonical = $this->data[0]['canonical'];
			if(!empty($this->title)) {
				$this->head .= '<title>'.$this->title.'</title>';
			}
			if(!empty($this->description)) {
				$this->head .= '<meta name="description" content="'.$this->description.'">';
			}
			if(!empty($this->keywords)) {
				$this->head .= '<meta name="keywords" content="'.$this->keywords.'">';
			}
			if(!empty($this->canonical)) {
				$this->head .= '<link rel="canonical" href="'.$this->canonical.'">';
			}
		}
		return $this->head;
	}
}

?>