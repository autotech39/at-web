<?php
ini_set('session.gc_maxlifetime', 2592000);
ini_set('session.cookie_lifetime', 2592000);
if(empty($_SESSION)) {
	session_start();
}
define('ROOT_PATH',$_SERVER['DOCUMENT_ROOT']);
//define('LANG_PATH','lang');
include_once('core.php');

define('LANGUAGES','en,ru');
$lngs = explode(',',LANGUAGES);

$lng = 'ru';

if(!empty($_REQUEST['lng'])&&in_array($_REQUEST['lng'],$lngs)) {
	$lng = $_REQUEST['lng'];
	$_SESSION['LNG'] = $lng;
	$_COOKIE['LNG'] = $lng;
	//setcookie("LNG", $lng, time()+2592000);
}
elseif(!empty($_COOKIE['LNG'])) {
	$lng = $_COOKIE['LNG'];
	$_SESSION['LNG'] = $_COOKIE['LNG'];
}
elseif(!empty($_SESSION['LNG'])) {
	$lng = $_SESSION['LNG'];
	$_COOKIE['LNG'] = $lng;
	//setcookie("LNG", $lng, time()+2592000);
}
/*elseif ($locale = locale_parse(NULL)) {
	$lng = $locale['language'];
	$_SESSION['LNG'] = $lng;
	$_COOKIE['LNG'] = $lng;
}*/
else {
	$_SESSION['LNG'] = $lng;
	$_COOKIE['LNG'] = $lng;
}

/*if(!empty($_COOKIE['LNG'])) {
	$_SESSION['LNG'] = $_COOKIE['LNG'];
}

if(!empty($_REQUEST['lng'])&&in_array($_REQUEST['lng'],$lngs)) {
	$_SESSION['LNG'] = $_REQUEST['lng'];
}

$locale = locale_parse(NULL);
$lng = $locale['language'];
if(!empty($_SESSION['LNG'])) {
	$lng = $_SESSION['LNG'];
}
if(!empty($_REQUEST['lng'])&&in_array($_REQUEST['lng'],$lngs))  {
	$lng = $_REQUEST['lng'];
}
$_COOKIE['LNG'] = $lng;
//setcookie('LNG',$lng,time()+COOKIE_TIME,"/", ".".$_SERVER['SERVER_NAME']);
*/

define('DBHOST','10.78.101.143');
define('DBNAMELIST','autotech_base');
define('DBUSERLIST','autotech_web');
define('DBPASSLIST','xt&ku^y@Uo$#');
define('DBPREFLIST','');
define('SITE_NAME', 'Start');
define('SITE_URL', 'test.autotech39.com');
define('SITE_LANG', $lng);
define('LANG_PATH','language');
define('COOKIE_TIME', 2592000);
define('LOG_WRITE', 1);
define('LOG_PATH', 'log');
define('LOG_EXTENSION', 'log');
define('LOG_PREFIX', '%DATE%_');
define('LOG_SUFFIX', '');
define('LOG_DATETIMEFORMAT', 'Y.m.d H:i:s.u');
define('PAGE_PATH','page');
define('START_PAGE', 'index');
define('START_PAGE_BY_NAME',false);
define('TEMPLATES_PATH','templates');
define('TEMPLATE_NAME', 'start');										//имя текущего шаблона
define('TEMPLATE_PATH',TEMPLATES_PATH.'/'.TEMPLATE_NAME);					//папка текущего шаблона
define('SRC_TEMPLATE_PATH','/'.TEMPLATES_PATH.'/'.TEMPLATE_NAME);
define('TEMPLATE_INDEX',TEMPLATE_PATH.'/index.tpl');
define('ERROR_PATH', 'error');
define('NAME_404', '404');
define('FILE_404', ERROR_PATH.'/'.NAME_404);
define('UPLOAD_PATH','upload/');
define('SOFTWARE_PATH','software/');

define('RECAPTCHA1','6Ld9WikUAAAAAK6e_YMf3KcnCy8e6h_b66nZoLub');
define('RECAPTCHA2','6Ld9WikUAAAAAOUIuOzy15LzT-hkyvrb79YGhOQ8');

if (file_exists(ROOT_PATH.'/'.LANG_PATH.'/'.SITE_LANG.'.php')) {
	include_once(ROOT_PATH.'/'.LANG_PATH.'/'.SITE_LANG.'.php');
}

include_once('dbclass.php');
include_once('corefunc.php');
?>
