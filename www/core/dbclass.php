<?php
class dbconnect {
	private $DBhost = DBHOST;
	private $DBname = array();
	private $DBuser = array();
	private $DBpass = array();
	private $DBpref = array();
	private $DB;
	public $Text;
	public $Data = array();
	public $FetchData;
	public $Result;
	public $Error;
	public $ErrorTxt;
	public $ConnectResult;
	private $DBnum = 0;
	public function Connect($i=0) {
		//error_reporting(0);
		$this->DBname = explode(',',DBNAMELIST);
		$this->DBuser = explode(',',DBUSERLIST);
		$this->DBpass = explode(',',DBPASSLIST);
		//var_dump($this->DBname);
		if (count($this->DBname)==1) {
			$i = 0;
		}
		elseif (count($this->DBname)<=$i) {
			exit("Unable to connect to database: wrong parameter of \"Connect()\"");
		}
		$this->DBnum = $i;
		$user = $this->DBuser;
		$pass = $this->DBpass;
		$name = $this->DBname;
		$this->DB = mysqli_connect($this->DBhost,$user[$i],$pass[$i]);
		mysqli_select_db ($this->DB,$name[$i]);
		mysqli_query($this->DB,'SET character_set_database = utf8');
		mysqli_query($this->DB,'SET NAMES utf8');
		if (mysqli_errno($this->DB)!=0) {
			exit("MySQL not connect!");
		}
	}
	public function TransactStart() {
		$this->Result = mysqli_query($this->DB,'START TRANSACTION');
	}
	public function Commit() {
		$this->Result = mysqli_query($this->DB,'COMMIT');
	}
	public function Rollback() {
		$this->Result = mysqli_query($this->DB,'ROLLBACK');
	}
	public function Query($use_pref=true) {
		if ($use_pref) {
			$this->DBpref = explode(',',DBPREFLIST);
			$j = $this->DBnum;
			$this->Text = preg_replace('/\s\s+/',' ',$this->Text);
			$this->Text = str_replace('FROM `','FROM `'.$this->DBpref[$j],$this->Text);
			$this->Text = str_replace('INTO `','INTO `'.$this->DBpref[$j],$this->Text);
			$this->Text = str_replace('UPDATE `','UPDATE `'.$this->DBpref[$j],$this->Text);
			$this->Text = str_replace('TABLE `','TABLE `'.$this->DBpref[$j],$this->Text);
		}
		$this->Result = mysqli_query($this->DB,$this->Text);
	}
	public function Fetch() {
		$this->Data=array();
		while ($FetchData = mysqli_fetch_array($this->Result)) {
			array_push($this->Data,$FetchData);
		}
		return $this->Data;
	}
	public function Assoc() {
		$this->Data=array();
		while ($FetchData = mysqli_fetch_assoc($this->Result)) {
			array_push($this->Data,$FetchData);
		}
		return $this->Data;
	}
	public function ErrorNum() {
		$this->Error = mysqli_errno($this->DB);
		return $this->Error;
	}
	public function ErrorText() {
		$this->ErrorTxt = mysqli_error($this->DB);
		return $this->ErrorTxt;
	}
	public function NumRows() {
		$this->QuantRows = mysqli_num_rows($this->Result);
		return $this->QuantRows;
	}
	public function LastId() {
		$this->LastInsertId = mysqli_insert_id($this->DB);
		return $this->LastInsertId;
	}
}
$MyDB = new dbconnect;
?>