<?php

function url_parser() {
    $requestUri = $_SERVER['REQUEST_URI'];
	if ($requestUri != '/') {
		if ($requestUri==$_SERVER['REQUEST_URI']) {
			$clearpath = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
		}
		else {
			$clearpath = $requestUri;
		}
	}
	$err = 0;
	$pathArr = explode('/', $clearpath);
	if(empty($pathArr[0])) {
		array_shift($pathArr);
	}
			$clearpath = implode('/',$pathArr);
	if (is_dir(ROOT_PATH.'/'.PAGE_PATH.'/'.$clearpath)){
		if (file_exists(ROOT_PATH.'/'.PAGE_PATH.'/'.$clearpath.'/'.'index.php')) {
			$outfile = ROOT_PATH.'/'.PAGE_PATH.'/'.$clearpath.'/'.'index.php';
			$resFile = 'index.php';
		}
		else {
			$outfile = ROOT_PATH.'/'.FILE_404.'.php';
			$err = 404;
		}
	}
	else {
		if (file_exists(ROOT_PATH.'/'.PAGE_PATH.'/'.$clearpath.'.php')) {
			$outfile = ROOT_PATH.'/'.PAGE_PATH.'/'.$clearpath.'.php';
			$resFile = array_pop($pathArr);
			$resFile = $resFile.'.php';
			$clearpath = implode('/',$pathArr);
		}
		else {
			$outfile = ROOT_PATH.'/'.FILE_404.'.php';
			$err = 404;
		}
	}
    $urlOut = array('OUT_PATH'=>$clearpath, 'OUT_FILE'=>$resFile, 'INCL'=>$outfile,  'ERROR'=>$err);
	return $urlOut;
}

function getTplFileName($page) {
	if (substr($page,-4,4)=='.php') {
		$tplPage = str_replace('.php','.tpl',$page);
	}
	else {
		$tplPage = $page.'tpl';
	}
	return $tplPage;
}

function incl_page($page,$path,$page_core) {
	$tplPage = getTplFileName($page);
	if(file_exists(ROOT_PATH.'/'.PAGE_PATH.'/'.$path.'/'.$page)) {
		include ROOT_PATH.'/core/incl_prepare.php';
	}
	else {
		$page_core->read(ROOT_PATH.'/'.TEMPLATES_PATH.'/'.TEMPLATE_NAME.'/'.FILE_404.'.tpl');
		include ROOT_PATH.'/'.FILE_404.'.php';
	}
	$output = ob_get_contents();
	return $output;
}


function writeLog ($name,$event,$subfolder='',$agent=false) {
	$islog = LOG_WRITE;
	if ($islog == 1 || $islog == 'true' || $islog == 'TRUE') {
		if (substr($subfolder,0,1)=='/') {
			$subfolder = substr_replace($subfolder,'',0,1);
		}
		if (substr($subfolder,-1,1)=='/') {
			$subfolder = substr_replace($subfolder,'',-1,1);
		}
		if (!is_dir(ROOT_PATH.'/'.LOG_PATH.'/'.$subfolder)) {
			mkdir(ROOT_PATH.'/'.LOG_PATH.'/'.$subfolder, 0777, true);
		}
		$extension = LOG_EXTENSION;
		if (substr($extension,0,1)!='.') {
			$extension = '.'.$extension;
		}
		$dtFormat = LOG_DATETIMEFORMAT;
		if ($dtFormat=='') {
			$dtFormat = 'Y.m.d H:i:s.u';
		}
		$datetime = date($dtFormat);
		if (substr($datetime,-7,7)=='.000000') {
			$microsec = microtime();
			$datetime = substr_replace($datetime,substr($microsec,2,6),-6,6);
		}
		$addr = $_SERVER["REMOTE_ADDR"];
		$event = $datetime.' '.$addr.' '.$event;
		if ($agent) {
			$event .= ' User agent:'.$_SERVER['HTTP_USER_AGENT'];
		}
		$prefix = LOG_PREFIX;
		$suffix = LOG_SUFFIX;
		$prefix = str_replace('%DATE%',date('Ymd'),$prefix);
		$prefix = str_replace('%DATETIME%',date('Ymd_His'),$prefix);
		$prefix = str_replace('%TIME%',date('His'),$prefix);
		$suffix = str_replace('%DATE%',date('Ymd'),$suffix);
		$suffix = str_replace('%DATETIME%',date('Ymd_His'),$suffix);
		$suffix = str_replace('%TIME%',date('His'),$suffix);
		$fileName =$prefix.$name.$suffix.$extension;
		$logFile = fopen(ROOT_PATH.'/'.LOG_PATH.'/'.$subfolder.'/'.$fileName, 'a');  
		fwrite ($logFile, $event."\n");
		fclose ($logFile);
	}
}

function getArticles ($quantity,$category=1,$type=0,$lng='en',$sort='ASC',$page=1) {
	if (empty($MyDB)) {
		$MyDB = new dbconnect;
		$MyDB->Connect();
	}
	$MyDB->Text = "SELECT a.`id`, 
		`title`, 
		`timestamp`, 
		`datestart`, 
		`dateend`, 
        `description`,
		`content`,
		`image`, 
		`category`,
		ac.`name` AS category_name,
		`type`, 
		atp.`name` AS type_name,
		`language`,
		l.`name` AS language_name,
		l.`code` AS language_code
	
	FROM `article` a
	LEFT JOIN `article_category` ac
	ON a.`category` = ac.`id`
	LEFT JOIN `article_category` atp
	ON a.`category` = atp.`id`
	LEFT JOIN `language` l
	ON a.`language` = l.`id`
	WHERE a.`status`=1
	AND `datestart`<='".date('Y-m-d H:i:s')."'
	AND (`dateend` IS NULL OR `dateend`='' OR `dateend`> '".date('Y-m-d H:i:s')."')";
	
	if (!empty($category)&&$category!='') {
		$MyDB->Text .= " 
		AND `category` = '".$category."' ";
	}
	if (!empty($type)&&$type!='') {
		$MyDB->Text .= " 
		AND `type` = '".$type."' ";
	}
	if (!empty($lng)&&$lng!='') {
		$MyDB->Text .= " 
		AND l.`code` = '".$lng."' ";
	}
	if($sort=='ASC'||$sort=='DESC') {
		$MyDB->Text .= "
		ORDER BY `timestamp` ".$sort." ";
	}
	
	$quantity = $quantity*1;
	if(!is_int($quantity)) {
		$quantity = 0;
	}
	$page = $page*1;
	if(!is_int($page)) {
		$page = 1;
	}
	$start = ($page-1)*$quantity;
	if (!empty($quantity)) {
		$MyDB->Text .= " 
		LIMIT ".$start.", ".$quantity;
	}
	//echo $MyDB->Text;
	$MyDB->Query();
	$MyDB->Assoc();
	foreach($MyDB->Data as $k => $val) {
		$MyDB->Data[$k]['content'] = htmlspecialchars_decode($val['content']);
	}
	return $MyDB->Data;
}

function getArticleById ($id,$type=0) {
	if (empty($MyDB)) {
		$MyDB = new dbconnect;
		$MyDB->Connect();
	}
	$typeWhere = '';
	if($type>0) {
		$typeWhere = "
		AND `type`='".$type."'";
	}
	$MyDB->Text = "SELECT a.`id`, 
		`title`, 
		`timestamp`, 
		`datestart`, 
		`dateend`, 
        `description`,
		`content`,
		`image`, 
		`category`,
		ac.`name` AS category_name,
		`type`, 
		atp.`name` AS type_name,
		`language`,
		l.`name` AS language_name,
		l.`code` AS language_code
	
	FROM `article` a
	LEFT JOIN `article_category` ac
	ON a.`category` = ac.`id`
	LEFT JOIN `article_category` atp
	ON a.`category` = atp.`id`
	LEFT JOIN `language` l
	ON a.`language` = l.`id`
	WHERE a.`status`=1
	AND `datestart`<='".date('Y-m-d H:i:s')."'
	AND (`dateend` IS NULL OR `dateend`='' OR `dateend`> '".date('Y-m-d H:i:s')."')
	AND a.`id`='".$id."' ".$typeWhere."
	LIMIT 1";
	
	$MyDB->Query();
	$MyDB->Assoc();
	foreach($MyDB->Data as $k => $val) {
		$MyDB->Data[$k]['content'] = htmlspecialchars_decode($val['content']);
	}
	return $MyDB->Data[0];
}

function contentFilter($content,$part='full',$delim='[---]') {
	$result = '';
	$contentArr = explode($delim,$content);
	//echo $part;
	if ($part == 'full') {
		//echo 'aaa';
		$result = implode('',$contentArr);
	}
	elseif (!empty($contentArr[$part-1])) {
		//echo 'sss';
		$result = $contentArr[$part-1];
	}
	else {
		$result = false;
	}
	return $result;
}

function sendEmail ($template,$email=array(),$replace=array(),$lng=SITE_LANG,$extra='') {
	$result = false;
	if (!empty($email)) {
		if (empty($MyDB)) {
			$MyDB = new dbconnect;
			$MyDB->Connect();
		}
		$MyDB->Text = "SELECT d.`id`, 
			d.`name`, 
			d.`title`, 
			d.`text`, 
			d.`signature`, 
			l.code AS language 
		FROM `dispatch_template` d
		LEFT JOIN `language` l
		ON d.`language` = l.`id`
		WHERE l.`code` = '".$lng."'
		AND d.`name` = '".$template."'
		LIMIT 1";
		$MyDB->Query();
		$MyDB->Assoc();
		if (empty($MyDB->Data)) {
			$result = 'Template not found';
		}
		$title = $MyDB->Data[0]['title'];
		$text = htmlspecialchars_decode($MyDB->Data[0]['text']);
		$signature = htmlspecialchars_decode($MyDB->Data[0]['signature']);
		if (is_array($replace)) {
			foreach ($replace as $k => $value) {
				$text = str_replace($value['search'],$value['replace'],$text);
			}
		}
		if (is_array($email)) {
			$r = 0;
			$s = 0;
			foreach ($email as $e => $addr) {
				$text .= ' '.$extra;
				$headers  = 'MIME-Version: 1.0' . "\r\n";
				$headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
				$headers .= 'To: <'.$addr.'>' . "\r\n";
				$headers .= 'From: AutoTech <noreply@autotech39.com>' . "\r\n";
				
				$send = mail($addr, $title, $text, $headers);
				if(!$send) {
					$r++;
				}
				$s++;
			}
			if ($r>0&&$r<$s) {
				$result = 'Sending with errors';
			}
			elseif ($r>0&&$r==$s) {
				$result = 'Not send';
			}
		}
		else {
			$text .= ' '.$extra;
			$text .= ' '.$signature;
			$headers  = 'MIME-Version: 1.0' . "\r\n";
			$headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
			$headers .= 'To: <'.$email.'>' . "\r\n";
			$headers .= 'From: AutoTech <noreply@autotech39.com>' . "\r\n";
			
			$send = mail($email, $title, $text, $headers);
			if(!$send) {
				$result = 'Not send';
			}
		}
	}
	else {
		$result = 'E-Mail not found';
	}
	return $result;
}

function checkDevice($sn,$key) {
	if (empty($MyDB)) {
		$MyDB = new dbconnect;
		$MyDB->Connect();
	}
	$MyDB->Text = "SELECT `id` FROM `devices` 
	WHERE `serial`= '".$sn."' AND `secretkey`='".$key."'";
	$MyDB->Query();
	$MyDB->Assoc();
	if (empty($MyDB->Data)) {
		$result = false;
	}
	else {
		$result = $MyDB->Data[0]['id'];
	}
	return $result;
}

function getUserByDeviceId($id) {
	if (empty($MyDB)) {
		$MyDB = new dbconnect;
		$MyDB->Connect();
	}
	$MyDB->Text = "SELECT `user` FROM `userdevices` 
	WHERE `device`= '".$id."'";
	$MyDB->Query();
	$MyDB->Assoc();
	if (empty($MyDB->Data)) {
		$result = false;
	}
	else {
		$result = $MyDB->Data[0]['user'];
	}
	return $result;
}
function getMaxVersion() {
	$result = false;
	if (empty($MyDB)) {
	$MyDB = new dbconnect;
	$MyDB->Connect();
	}
	$MyDB->Text = "SELECT `id`, `version`
	FROM `software`";
	$MyDB->Query();
	$MyDB->Assoc();
	$vDataArr = $MyDB->Data;
	if (!empty($vDataArr)) {
		$sVersArr = explode('.',$vDataArr[0]['version']);
		$nVers = $vDataArr[0]['version'];
		//print_r($vDataArr);
		foreach ($vDataArr as $vk => $vData) {
			$cVersArr = explode('.',$vData['version']);
			if ($cVersArr[0]>$sVersArr[0]) {
				$sVersArr = $cVersArr;
				$nVers = $vData['version'];
			}
			elseif ($cVersArr[0]==$sVersArr[0]) {
				if ($cVersArr[1]>$sVersArr[1]) {
					$sVersArr = $cVersArr;
					$nVers = $vData['version'];
				}
				elseif ($cVersArr[1]==$sVersArr[1]) {
					if ($cVersArr[2]>$sVersArr[2]) {
						$sVersArr = $cVersArr;
						$nVers = $vData['version'];
					}
				}
			}
		}
		$result = $nVers;
	}
	return $result;
}
function compareVersion($v1,$v2) {
	$result = false;
	if (!empty($v1)&&!empty($v2)) {
		$sVersArr = explode('.',$v1);
		$cVersArr = explode('.',$v2);
		if(count($sVersArr)>=3&&count($cVersArr)>=3) {
			$result = 1;
			if ($cVersArr[0]>$sVersArr[0]) {
				$sVersArr = $cVersArr;
				//$nVers = $vData['version'];
				$result = 2;
			}
			elseif ($cVersArr[0]==$sVersArr[0]) {
				if ($cVersArr[1]>$sVersArr[1]) {
					$sVersArr = $cVersArr;
					//$nVers = $vData['version'];
					$result = 2;
				}
				elseif ($cVersArr[1]==$sVersArr[1]) {
					if ($cVersArr[2]>$sVersArr[2]) {
						$sVersArr = $cVersArr;
						//$nVers = $vData['version'];
						$result = 2;
					}
				}
			}
		}
	}
	return $result;
}
function checkVersion($vDev) {
	$result = false;
	$vDB = getMaxVersion();
	if($vDB) {
		$vs = compareVersion($vDev,$vDB);
		if ($vs == 2) {
			$result = true;
		}
	}
	return $result;
}
function maxIntVersion() {
	$result = false;
	if (empty($MyDB)) {
	$MyDB = new dbconnect;
	$MyDB->Connect();
	}
	$MyDB->Text = "SELECT MAX(`version`) AS version FROM `software`";
	$MyDB->Query();
	$MyDB->Assoc();
	$vDataArr = $MyDB->Data;
	if(!empty($vDataArr)) {
		$result = $vDataArr[0]['version'];
	}
	return $result;
}

function checkIntVersion($vDev) {
	$result = false;
	$vDB = getMaxVersion();
	if($vDB>$vDev) {
		$result = true;
	}
	return $result;
}
function getDownloadFile($file) {
  if (file_exists($file)) {
    // сбрасываем буфер вывода PHP, чтобы избежать переполнения памяти выделенной под скрипт
    // если этого не сделать файл будет читаться в память полностью!
    if (ob_get_level()) {
      ob_end_clean();
    }
    // заставляем браузер показать окно сохранения файла
    header('Content-Description: File Transfer');
    header('Content-Type: application/octet-stream');
    header('Content-Disposition: attachment; filename=' . basename($file));
    header('Content-Transfer-Encoding: binary');
    header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    header('Content-Length: ' . filesize($file));
    // читаем файл и отправляем его пользователю
    if ($fd = fopen($file, 'rb')) {
      while (!feof($fd)) {
        print fread($fd, 1024);
      }
      fclose($fd);
    }
    exit;
  }
}
function file_force_download($file) {
  if (file_exists($file)) {
    // сбрасываем буфер вывода PHP, чтобы избежать переполнения памяти выделенной под скрипт
    // если этого не сделать файл будет читаться в память полностью!
    if (ob_get_level()) {
      ob_end_clean();
    }
    // заставляем браузер показать окно сохранения файла
    header('Content-Description: File Transfer');
    header('Content-Type: application/octet-stream');
    header('Content-Disposition: attachment; filename=' . basename($file));
    header('Content-Transfer-Encoding: binary');
    header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    header('Content-Length: ' . filesize($file));
    // читаем файл и отправляем его пользователю
    readfile($file);
    exit;
  }
}
?>