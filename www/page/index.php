<?
$page_core->set_data('[TEMPLATE_PATH]',TEMPLATE_PATH);
$page_core->set_data('[SRC_TEMPLATE_PATH]',SRC_TEMPLATE_PATH);
$start_articles = getArticles (1,4,1,SITE_LANG);
$page_core->set_data('[START_TEXT]',$start_articles[0]['content']);

$mainpage_articles = getArticles (3,2,1,SITE_LANG);

$page_core->set_data('[MAINPAGE_TEXT1]',$mainpage_articles[0]['content']);
$page_core->set_data('[MAINPAGE_TEXT2]',$mainpage_articles[1]['content']);
$page_core->set_data('[MAINPAGE_TEXT3]',$mainpage_articles[2]['content']);

$start_file = ROOT_PATH.'/'.LANG_PATH.'/start.'.SITE_LANG.'.tpl';
if (file_exists($start_file)) {
    $content = file_get_contents($start_file);
    $content = str_replace('[TEMPLATE_PATH]',TEMPLATE_PATH,$content);
    $content = str_replace('[SRC_TEMPLATE_PATH]',SRC_TEMPLATE_PATH,$content);
    echo $content;
}

?>