<?if(!empty($_GET['article'])):?>
<?
$article = getArticleById($_GET['article'],3);
$isBuy = false;
?>
<div class="page_container">
    <!--<h3><?//=$article['title'];?></h3>-->
    <h2 class="grey-text"><?=OUR_DEVS;?></h2>
    <div class="articles-content-box">        
        <a href="/developments/" class="link-button back-button red-text"><?=TO_BACK;?></a>
        <div class="row">
            <div class="col-md-12 text-center">
                
                <div class="single-article-box">
                    <div class="single-article-preview" style="background-image: url(<?=$article['image']?>);"></div>
                    <div class="single-article-block">
                        <?if($isBuy):?>
                        <div class="single-article-buy-box">
                            <a class="btn btn-default" href=""><?=BUY;?></a>
                        </div>
                        <?endif;?>
                        <h3><?=$article['title'];?></h3>
                        <span><?=contentFilter($article['content']);?></span>
                    </div>
                    <div class="single-article-button-box">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?endif;?>