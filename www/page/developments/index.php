<?
$page_core->set_data('[DEVS]',M_DEVS);
$page_core->set_data('[SRC_TEMPLATE_PATH]',SRC_TEMPLATE_PATH);
?>
<?if(empty($_GET['article'])):?>
<?
$news_articles = getArticles (5,5,3,SITE_LANG,'DESC');

$isBuy = false;
?>
<?
//print_r($_REQUEST);
?>
<div class="page_container">
    <h2 class="grey-text"><?=OUR_DEVS;?></h2>
    <div class="newslist">
        <div class="articles-content-box">
            <div class="row">
    <?foreach ($news_articles as $key => $news):?>
        <?
        $a++;
        ?>
        <?if($a%3==1 && $a>3):?>
            </div>
        </div>
        <div class="articles-content-box">
            <div class="row">
        <?endif;?>
                <div class="col-md-4 text-center">
                    <div class="item-article-box">
                        <div class="item-article-preview" style="background-image: url(<?=$news['image']?>);"></div>
                        <div class="item-article-block">
                            <h3><?=$news['title'];?></h3>
                            <span><?=contentFilter($news['content'],1);?></span>
                        </div>
                        <div class="item-article-button-box">
                            <a class="btn btn-primary" href="/developments/?article=<?=$news['id']?>"><?=DETAIL;?></a>
                            <?if($isBuy):?>
                            <div class="item-article-buy-box">
                                <a class="btn btn-default" href="/developments/?article=<?=$news['id']?>"><?=BUY;?></a>
                            </div>
                            <?endif;?>
                        </div>
                    </div>
                </div>

    <?endforeach;?>

            </div>
        </div>
    </div>
</div>
<?else:?>
<?include('article.php');?>
<?endif;?>