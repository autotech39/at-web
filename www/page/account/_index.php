<?
if(empty($_SESSION['USER_ID'])) {
    header('Location: /');
    exit;
}
$page_core->set_data('[MESSAGESOFASO]',MESSAGESOFASO);
$page_core->set_data('[STATUSESOFASO]',STATUSESOFASO);
$page_core->set_data('[SOFTWAREUPDATE]',SOFTWAREUPDATE);
$page_core->set_data('[DISTRIBUTION_OPTIONS]',DISTRIBUTION_OPTIONS);
$page_core->set_data('[SETTINGS]',SETTINGS);
?>
