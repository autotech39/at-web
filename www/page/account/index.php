<?
if(empty($_SESSION['USER_ID'])) {
    header('Location: /');
    exit;
}
?>
<script src="/admin/js/lng/<?=SITE_LANG;?>.js"></script>
<?
if (empty($MyDB)) {
    $MyDB = new dbconnect;
}
$MyDB->Connect();
$MyDB->Text = "SELECT `id`, `device`, `name`, `serial`
FROM `userdevices`
WHERE `user`='".$_SESSION['USER_ID']."'
ORDER BY `name`";
$MyDB->Query();
$MyDB->Assoc();
?>
<div class="page_container">
    <h2 class="grey-text"><?=PERSONALAREA;?></h2>
    <div class="account-content-box">
        <button id="settingbutton" class="btn btn-primary button-fix160">Настройки</button>
        <div id="account-selectdevice">
    <?if (empty($MyDB->Data)):?>
            <span class="no-data"><?=REGDEVICENOTFOUND;?></span>
    <?else:?>
        <?
        $aa_DevNum = $MyDB->Data[0]['serial'];
        $aa_DevName = $MyDB->Data[0]['name'];
        $aa_DevId = $MyDB->Data[0]['device'];
        $jsArr = array();
        foreach ($MyDB->Data as $dk => $messages) {
            $jsArr[$messages['id']]['name'] = $messages['name'];
            $jsArr[$messages['id']]['number'] = $messages['serial'];
        }
        $jsArrStr = json_encode($jsArr);
        ?>
            <select id="deviceselect" class="form-control">
        <?if(count($MyDB->Data)>1):?>
                <option value="NULL"><?=SELECTDEVICE;?></option>
        <?elseif(count($MyDB->Data)==1):?>
            <script>
                /*var devnum = '<?//=$aa_DevNum;?>';
                var devname = '<?//=$aa_DevName;?>';
                var devid = '<?//=$aa_DevId;?>';
                $('#account-content-app-box').html(ln.load);
                var content = syncAjax('/ajax/account/app/app-content-basic.php?devnum='+devnum+'&devname='+devname+'&devid='+devid);
                $('#account-content-app-box').html(content);*/
                
            </script>
        <?endif;?>
        <?foreach($MyDB->Data as $n => $device):?>
                <option value="<?=$device['device'];?>"><?=$device['name'];?></option>
        <?endforeach;?>
            </select>
    <?endif;?>
        </div>
        <div id="account-content-app-box">
    <?
    if(!empty($aa_DevNum)&&count($MyDB->Data)==1) {
        include_once(ROOT_PATH.'/ajax/account/app/app-content-basic.php');
    }
    ?>
        </div>
    </div>
</div>
<script>
$('#deviceselect').on('change',function(){
    var jsonStr = '<?=$jsArrStr;?>';
    var devid = $(this).val();
    if (!isEmpty(jsonStr)) {
        var param = JSON.parse(jsonStr);
        $('#account-content-app-box').html(ln.load);
        var content = syncAjax('/ajax/account/app/app-content-basic.php?devnum='+param[devid]['number']+'&devname='+param[devid]['name']+'&devid='+devid);
        $('#account-content-app-box').html(content);
    }
});

/*var hash = window.location.hash;
var names = ['settings','message','status','update','sending'];
var name = '';
var startLoad = 0;
if(hash==='') {
    name = 'basic';
}
else {
    var hashArr = hash.split('#');
    var preNameArr = hashArr[1].split(';');
    if (names.indexOf(preNameArr[0])>-1) {
        name = preNameArr[0];
    }
    else {
        name = 'basic';
    }
    startLoad = 1;
}*/

</script>