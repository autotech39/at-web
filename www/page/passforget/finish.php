<?
if(empty($_SESSION)) {
    session_start();
}
//print_r($_POST);
$username = empty($_POST['login']) ? '' : $_POST['login'];
$postdata = http_build_query(
    array(
        'response' => $_POST['g-recaptcha-response'],
        'secret' => RECAPTCHA2
    )
);

$opts = array('http' =>
    array(
        'method'  => 'POST',
        'header'  => 'Content-type: application/x-www-form-urlencoded',
        'content' => $postdata
    )
);

$context  = stream_context_create($opts);

$result = file_get_contents('https://www.google.com/recaptcha/api/siteverify', false, $context);

$final = json_decode($result,true);
//print_r( $final);
if ($final['success']) {
    require_once(ROOT_PATH.'/api/classes/sign.php');
    $auth = new RegAuth;
    if (empty($_REQUEST['password'])||$_REQUEST['password']!=$_REQUEST['re_password']) {
        header('Location: /passforget/reset?password=0&rp='.$_REQUEST['restore']);
    }
    else{
        $res = $auth->SetPasswordRestore($_REQUEST['restore'],$_REQUEST['password']);
        if ($res === false) {
            header('Location: /passforget/reset?password=-1&rp='.$_REQUEST['restore']);
        }
        else {
            $page_core->set_data('[RESET_PASSWORD]',RESET_PASSWORD);
            $page_core->set_data('[PASSISCHANGED]',PASSISCHANGED);
        }
    }
}
else {
    //echo 222;
    /*$page_core->set_data('[AUTH_ERROR]',AUTH_ERROR);
    $page_core->set_data('[GO_TO_LOGIN]',GO_TO_LOGIN);*/
    header('Location: /passforget/reset?sec=0&rp='.$_REQUEST['restore']);
}
?>