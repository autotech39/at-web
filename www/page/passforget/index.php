<?
if (strripos($_SERVER['HTTP_REFERER'],'auto.klassen-art.com/login')) {
    $referer = 'http://auto.klassen-art.com';
}
else {
    $referer = $_SERVER['HTTP_REFERER'];
}
$errortext = '';
if (isset($_GET['sec'])&&$_GET['sec']==0) {
    $errortext = '<span class="red-text">'.SEC_CHECK_FAILED.'</span>';
}
if (isset($_GET['email'])&&$_GET['email']<=0) {
    $errortext = '<span class="red-text">'.EMAIL_NOT_FOUND.'</span>';
}
$page_core->set_data('[EMAIL]',EMAIL);
$page_core->set_data('[RESET_PASSWORD]',RESET_PASSWORD);
$page_core->set_data('[UEMAIL]',!empty($_GET['email'])?$_GET['email']:'');
$page_core->set_data('[SEC_ERROR_CLASS]',isset($_GET['sec'])&&$_GET['sec']==0?' red-light':'');
$page_core->set_data('[EMAIL_ERROR_CLASS]',isset($_GET['email'])&&$_GET['email']<=0?' red-light':'');
//$page_core->set_data('[SEC_ERROR_TEXT]',isset($_GET['sec'])&&$_GET['sec']==0?SEC_CHECK_FAILED:'
$page_core->set_data('[ERROR_TEXT]',$errortext);
$page_core->set_data('[ACTION]','http://'.SITE_URL.'/passforget/send');
$page_core->set_data('[AJAX_ACTION]','http://'.SITE_URL.'/ajax/checkcpt.php');
$page_core->set_data('[REFERER]',$referer);
$page_core->set_data('[RECAPTCHA]',RECAPTCHA1);
?>