<?
if(empty($_GET['rp'])) {
    header('Location: /');
}
else {
    $errortext = '';
    if (isset($_GET['sec'])&&$_GET['sec']==0) {
        $errortext = '<span class="red-text">'.SEC_CHECK_FAILED.'</span>';
    }
    if (isset($_GET['password'])&&$_GET['password']==0) {
        $errortext = '<span class="red-text">'.PASS_NOT_MATCH.'</span>';
    }
    if (isset($_GET['password'])&&$_GET['password']==-1) {
        $errortext = '<span class="red-text">'.PASSISNOTCHANGED.'</span>';
    }
    $page_core->set_data('[RESET_PASSWORD]',RESET_PASSWORD);
    $page_core->set_data('[ENTER_NEWPASS]',ENTER_NEWPASS);
    $page_core->set_data('[PASSWORD]',PASSWORD);
    $page_core->set_data('[RE_PASSWORD]',RE_PASSWORD);
    $page_core->set_data('[SUBMIT]',SUBMIT);
    $page_core->set_data('[ERROR_TEXT]',$errortext);
    $page_core->set_data('[RECAPTCHA]',RECAPTCHA1);
    $page_core->set_data('[ACTION]','http://'.SITE_URL.'/passforget/finish');
    $page_core->set_data('[RESTORE]',$_GET['rp']);
    $page_core->set_data('[TOOSHORTPATH]',TOOSHORTPATH);
    /*require_once(ROOT_PATH.'/api/classes/sign.php');
    $error = ERROR;
    $auth = new RegAuth;
    $reset = $auth->SetPasswordRestore($_GET['rp']);*/
}

?>