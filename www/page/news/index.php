<?
$page_core->set_data('[NEWS]',M_NEWS);
$page_core->set_data('[SRC_TEMPLATE_PATH]',SRC_TEMPLATE_PATH);
?>
<?if(empty($_GET['article'])):?>
<?
$news_articles = getArticles (5,3,2,SITE_LANG,'DESC');

?>
<?
//print_r($_REQUEST);
?>
<div class="page_container">
    <h2 class="grey-text"><?=OUR_NEWS;?></h2>
    <div class="newslist">
        <div class="articles-content-box">
            <div class="row">
    <?
    $a = 0;
    ?>
    <?foreach ($news_articles as $key => $news):?>
        <?
        $a++;
        ?>
        <?if($a%3==1 && $a>3):?>
            </div>
        </div>
        <div class="articles-content-box">
            <div class="row">
        <?endif;?>
                <div class="col-md-4 text-center">
                    <div class="item-article-box">
                        <div class="item-article-preview" style="background-image: url(<?=$news['image']?>);"></div>
                        <div class="item-article-block">
                            <h3><?=$news['title'];?></h3>
                            <span><?=contentFilter($news['content'],1);?></span>
                        </div>
                        <div class="item-article-button-box">
                            <a class="btn btn-primary" href="/news/?article=<?=$news['id']?>"><?=READ;?></a>
                            <div class="item-article-share-box">
								<a class="fb-share-button" onclick="Share.facebook('http://<?=$_SERVER['SERVER_NAME'];?>/news/?article=<?=$news['id']?>','<?=$news['title'];?>','http://<?=$_SERVER['SERVER_NAME'].$news['image']?>','');"></a>
                                <a class="vk-share-button" onclick="Share.vkontakte('http://<?=$_SERVER['SERVER_NAME'];?>/news/?article=<?=$news['id']?>','<?=$news['title'];?>','http://<?=$_SERVER['SERVER_NAME'].$news['image']?>','')"></a>
                            </div>
                        </div>
                    </div>
                </div>

    <?endforeach;?>
            </div>
        </div>
    </div>
</div>

<script>
Share = {
	vkontakte: function(purl, ptitle, pimg, text) {
		url  = 'http://vkontakte.ru/share.php?';
		url += 'url='          + encodeURIComponent(purl);
		url += '&title='       + encodeURIComponent(ptitle);
		url += '&description=' + encodeURIComponent(text);
		url += '&image='       + encodeURIComponent(pimg);
		url += '&noparse=true';
        console.log(purl);
		Share.popup(url);
	},
	odnoklassniki: function(purl, text) {
		url  = 'http://www.odnoklassniki.ru/dk?st.cmd=addShare&st.s=1';
		url += '&st.comments=' + encodeURIComponent(text);
		url += '&st._surl='    + encodeURIComponent(purl);
		Share.popup(url);
	},
	facebook: function(purl, ptitle, pimg, text) {
		url  = 'http://www.facebook.com/sharer.php?s=100';
		url += '&p[title]='     + encodeURIComponent(ptitle);
		url += '&p[summary]='   + encodeURIComponent(text);
		url += '&p[url]='       + encodeURIComponent(purl);
		url += '&p[images][0]=' + encodeURIComponent(pimg);
		Share.popup(url);
	},
	twitter: function(purl, ptitle) {
		url  = 'http://twitter.com/share?';
		url += 'text='      + encodeURIComponent(ptitle);
		url += '&url='      + encodeURIComponent(purl);
		url += '&counturl=' + encodeURIComponent(purl);
		Share.popup(url);
	},
	mailru: function(purl, ptitle, pimg, text) {
		url  = 'http://connect.mail.ru/share?';
		url += 'url='          + encodeURIComponent(purl);
		url += '&title='       + encodeURIComponent(ptitle);
		url += '&description=' + encodeURIComponent(text);
		url += '&imageurl='    + encodeURIComponent(pimg);
		Share.popup(url)
	},

	popup: function(url) {
		window.open(url,'','toolbar=0,status=0,width=626,height=436');
	}
};
</script>
<?else:?>
<?include('article.php');?>
<?endif;?>