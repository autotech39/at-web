<?if(!empty($_GET['article'])):?>
<?
$article = getArticleById($_GET['article'],2);
?>
<div class="page_container">
    <h3 class="grey-text"><?=$article['title'];?></h3>
    <div class="articles-content-box">        
        <a href="/news/" class="link-button back-button red-text"><?=TO_BACK;?></a>
        <div class="row">
            <div class="col-md-12 text-center">
                
                <div class="single-article-box">
                    <div class="single-article-preview" style="background-image: url(<?=$article['image']?>);"></div>
                    <div class="single-article-block">
                        <h3><?=$article['title'];?></h3>
                        <span><?=contentFilter($article['content']);?></span>
                    </div>
                    <div class="single-article-button-box">
                        <div class="item-article-share-box">
							<a class="fb-share-button" onclick="Share.facebook('http://<?=$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];?>','<?=$article['title'];?>','http://<?=$_SERVER['SERVER_NAME'].$article['image']?>','');"></a>
                            <a class="vk-share-button" onclick="Share.vkontakte('http://<?=$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];?>','<?=$article['title'];?>','http://<?=$_SERVER['SERVER_NAME'].$article['image']?>','')"></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?
//print_r($_SERVER);
?>
<script>
Share = {
	vkontakte: function(purl, ptitle, pimg, text) {
		url  = 'http://vkontakte.ru/share.php?';
		url += 'url='          + encodeURIComponent(purl);
		url += '&title='       + encodeURIComponent(ptitle);
		url += '&description=' + encodeURIComponent(text);
		url += '&image='       + encodeURIComponent(pimg);
		url += '&noparse=true';
        console.log(purl);
		Share.popup(url);
	},
	odnoklassniki: function(purl, text) {
		url  = 'http://www.odnoklassniki.ru/dk?st.cmd=addShare&st.s=1';
		url += '&st.comments=' + encodeURIComponent(text);
		url += '&st._surl='    + encodeURIComponent(purl);
		Share.popup(url);
	},
	facebook: function(purl, ptitle, pimg, text) {
		url  = 'http://www.facebook.com/sharer.php?s=100';
		url += '&p[title]='     + encodeURIComponent(ptitle);
		url += '&p[summary]='   + encodeURIComponent(text);
		url += '&p[url]='       + encodeURIComponent(purl);
		url += '&p[images][0]=' + encodeURIComponent(pimg);
		Share.popup(url);
	},
	twitter: function(purl, ptitle) {
		url  = 'http://twitter.com/share?';
		url += 'text='      + encodeURIComponent(ptitle);
		url += '&url='      + encodeURIComponent(purl);
		url += '&counturl=' + encodeURIComponent(purl);
		Share.popup(url);
	},
	mailru: function(purl, ptitle, pimg, text) {
		url  = 'http://connect.mail.ru/share?';
		url += 'url='          + encodeURIComponent(purl);
		url += '&title='       + encodeURIComponent(ptitle);
		url += '&description=' + encodeURIComponent(text);
		url += '&imageurl='    + encodeURIComponent(pimg);
		Share.popup(url)
	},

	popup: function(url) {
		window.open(url,'','toolbar=0,status=0,width=626,height=436');
	}
};
</script>
<?endif;?>