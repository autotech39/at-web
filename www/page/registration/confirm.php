<?
if(empty($_GET['st'])) {
    header('Location: /registration/');
}
require_once(ROOT_PATH.'/api/classes/sign.php');
$error = ERROR;
$auth = new RegAuth;
$result = $auth->AccountActivate($_GET['st']);
if($result==-2) {
    $error_text = ACCOUNT_NOT_FOUND;
}
elseif ($result==-1) {
    $error_text = DB_QUERY_ERROR;
}
elseif ($result==0) {
    $error_text = ACC_ALREADY_ACTIVE;
}
elseif ($result==1) {
    $error = REGISTRATION_FINAL;
    $error_text = ACC_IS_ACTIVATE;
}
else {
    $error_text = UNKNOWN_ERROR;
}
//echo $auth->answer;
$butlink = '/login/';
$buttext =  SIGN_IN;
if (!empty($_SESSION['USER_ID'])) {
    $butlink = '/account/';
    $buttext =  GOTO_PAREA;
}

$page_core->set_data('[ACTIVATION_RESULT]',$error);
$page_core->set_data('[ACT_RESULT_TEXT]',$error_text);
$page_core->set_data('[BUT_LNK]',$butlink);
$page_core->set_data('[SIGN_IN]',$buttext);
?>