<?
if(empty($_POST)) {
    header('Location: /registration/');
}
require_once(ROOT_PATH.'/api/classes/sign.php');
$error = ERROR;
$lnk='/login/';
//$lnk='/registration/';
//$button = SIGN_UP;
$button = SIGN_IN;
$buttonClass = 'btn-default';
if(empty($_POST['login'])) {
    $error_text = EMPTY_LOGIN;
}
elseif(empty($_POST['password'])) {
    $error_text = EMPTY_PASS;
}
elseif($_POST['password']!=$_POST['re-password']) {
    $error_text = PASS_NOT_MATCH;
}
elseif(empty($_POST['email'])) {
    $error_text = EMPTY_EMAIL;
}
else {
    $postdata = http_build_query(
        array(
            'response' => $_POST['g-recaptcha-response'],
            'secret' => RECAPTCHA2
        )
    );
    
    $opts = array('http' =>
        array(
            'method'  => 'POST',
            'header'  => 'Content-type: application/x-www-form-urlencoded',
            'content' => $postdata
        )
    );
    
    $context  = stream_context_create($opts);
    
    $result = file_get_contents('https://www.google.com/recaptcha/api/siteverify', false, $context);
    //echo $result;
    $final = json_decode($result,true);
    if ($final['success']) {
        $auth = new RegAuth;
        $isLogin = $auth->CheckUserData($_POST['login']);
        $isEmail = $auth->CheckUserData($_POST['email'],'email');
        if ($isLogin) {
            $error_text = LOGIN_IS_BUSY;
            $button = TO_BACK;
            $lnk = '/registration/';
        }
        elseif ($isEmail) {
            $error_text = EMAIL_IS_BUSY;
            $button = TO_BACK;
            $lnk = '/registration/';
        }
        else {
            $register = $auth->Registration($_POST['login'],$_POST['email'],$_POST['password']);
            //echo $auth->answer;
            if ($register==-2) {
                $error_text = LOGIN_IS_BUSY;
                $button = TO_BACK;
                $lnk = '/registration/';
            }
            elseif ($register<0) {
                $error_text = REG_ERROR;
                $button = TO_BACK;
                $lnk = '/registration/';
            }
            else {
                $error = REGISTER_COMPLET;
                $error_text = REG_COMPLET_TEXT;
                $lnk = '/login/';
                $button = SIGN_IN;
                //$buttonClass = 'btn-primary';
            }
        }
    }
    else {
        $error_text = SEC_CHECK_FAILED;
        $button = TO_BACK;
        $lnk = '/registration/';
    }
}
$page_core->set_data('[REGISTRATION_RESULT]',$error);
$page_core->set_data('[REG_RESULT_TEXT]',$error_text);
$page_core->set_data('[BUTTON_LNK]',$lnk);
$page_core->set_data('[BUTTON_TEXT]',$button);
$page_core->set_data('[BUTTON_CLASS]',$buttonClass);
?>