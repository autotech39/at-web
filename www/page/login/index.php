<?
if (strripos($_SERVER['HTTP_REFERER'],'auto.klassen-art.com/login')) {
    $referer = 'http://auto.klassen-art.com';
}
else {
    $referer = $_SERVER['HTTP_REFERER'];
}
$errortext = '';
if (isset($_GET['sec'])&&$_GET['sec']==0) {
    $errortext = '<span class="red-text">'.SEC_CHECK_FAILED.'</span>';
}
if (isset($_GET['up'])&&$_GET['up']==0) {
    $errortext = '<span class="red-text">'.LOGINPASS_FAILED.'</span>';
}
$page_core->set_data('[LOGIN]',LOGIN);
$page_core->set_data('[PASSWORD]',PASSWORD);
$page_core->set_data('[SIGN_IN]',SIGN_IN);
$page_core->set_data('[LOG_IN_PA]',LOG_IN_PA);
$page_core->set_data('[SIGN_UP]',SIGN_UP);
$page_core->set_data('[PASS_FORGET]',PASS_FORGET);
$page_core->set_data('[UNAME]',!empty($_GET['name'])?$_GET['name']:'');
$page_core->set_data('[SEC_ERROR_CLASS]',isset($_GET['sec'])&&$_GET['sec']==0?' red-light':'');
//$page_core->set_data('[SEC_ERROR_TEXT]',isset($_GET['sec'])&&$_GET['sec']==0?SEC_CHECK_FAILED:'');
$page_core->set_data('[UP_ERROR_CLASS]',isset($_GET['up'])&&$_GET['up']==0?' red-light':'');
$page_core->set_data('[ERROR_TEXT]',$errortext);
$page_core->set_data('[ACTION]','http://'.SITE_URL.'/login/finish');
$page_core->set_data('[AJAX_ACTION]','http://'.SITE_URL.'/ajax/checkcpt.php');
$page_core->set_data('[REFERER]',$referer);
$page_core->set_data('[RECAPTCHA]',RECAPTCHA1);
?>