<?
if(empty($_SESSION)) {
    session_start();
}
//print_r($_POST);
$username = empty($_POST['login']) ? '' : $_POST['login'];
$postdata = http_build_query(
    array(
        'response' => $_POST['g-recaptcha-response'],
        'secret' => RECAPTCHA2
    )
);

$opts = array('http' =>
    array(
        'method'  => 'POST',
        'header'  => 'Content-type: application/x-www-form-urlencoded',
        'content' => $postdata
    )
);

$context  = stream_context_create($opts);

$result = file_get_contents('https://www.google.com/recaptcha/api/siteverify', false, $context);

$final = json_decode($result,true);
//print_r( $final);
if ($final['success']) {
    require_once(ROOT_PATH.'/api/classes/sign.php');
    $auth = new RegAuth;
    $auth->login = empty($_POST['login']) ? '' : $_POST['login'];
    $auth->pass = empty($_POST['password']) ? '' : $_POST['password'];
    $auth->SignIn();
    $result = json_decode($auth->answer,true);
    if($result['answer']==1) {
        //header('Location: '.$_POST['referer']);
        header('Location: /account/');
    }
    else {
        //echo 111;
        //$page_core->set_data('[AUTH_ERROR]',AUTH_ERROR);
        //$page_core->set_data('[GO_TO_LOGIN]',GO_TO_LOGIN);
        header('Location: /login/?up=0&name='.$username);
    }
}
else {
    //echo 222;
    /*$page_core->set_data('[AUTH_ERROR]',AUTH_ERROR);
    $page_core->set_data('[GO_TO_LOGIN]',GO_TO_LOGIN);*/
    header('Location: /login/?sec=0&name='.$username);
}
?>