<?
//session_start();
include_once($_SERVER['DOCUMENT_ROOT'].'/core/config.php');
?>
<?if(!empty($_REQUEST['device'])&&$_REQUEST['device']!='NULL'):?>
<?
if (empty($MyDB)) {
    $MyDB = new dbconnect;
}
$MyDB->Connect();
$MyDB->Text = "SELECT `id`, `device`, `name`, `serial`
FROM `userdevices`
WHERE `user`='".$_SESSION['USER_ID']."'
AND `device`='".$_REQUEST['device']."'
LIMIT 1";
$MyDB->Query();
$MyDB->Assoc();
?>
<?if(empty($MyDB->Data)):?>
    <?=NOTDATAFORDEVICE;?>
<?else:?>
    <strong><?=DEVICE;?>:</strong> <?=$MyDB->Data[0]['name'];?>,&nbsp;<strong><?=SN;?>:</strong> <?=$MyDB->Data[0]['serial'];?>
    <?
    $MyDB->Data = array();
    $MyDB->Text = "SELECT *
    FROM `diagnostic_result`
    WHERE `user`='".$_SESSION['USER_ID']."'
    AND `device`='".$_REQUEST['device']."'
    ORDER BY `timestamp` DESC
    LIMIT 1";
    $MyDB->Query();
    $MyDB->Assoc();
    ?>
    <?if(empty($MyDB->Data)):?>
        <?=NOTDATAFORDEVICE;?>
    <?else:?>
        <?if(!empty($MyDB->Data[0]['timestamp'])):?>
    <br />
    <strong><?=DATARECIEVED;?>:</strong>&nbsp;<?=date('d.m.Y H:i:s',strtotime($MyDB->Data[0]['timestamp']));?>
        <?endif;?>
    <div class="devicelist table-block">
        <div class="deviceboxhead table-row">
            <div class="table-cell"><?=DEVSTATUSPARAM;?></div>
            <div class="table-cell"><?=DEVSTATUSVALUE;?></div>
        </div>
        <?foreach($MyDB->Data[0] as $key => $param):?>
            <?if($key!='id'&&$key!='user'&&$key!='device'&&$key!='timestamp'&&$key!='status'):?>
            <?
            $paramName = strtoupper($key);
            $paramValue = '-';
            $paramClass = '';
            if($param==0) {
                $paramValue = NO;
            }
            elseif($param==1) {
                $paramValue = YES;
                $paramClass = ' red-text';
            }
            elseif(!empty($param)) {
                $paramValue = $param;
            }
            ?>
                <div class="table-row">
                    <div class="table-cell dark-text"><?=constant($paramName);?></div>
                    <div class="table-cell<?=$paramClass;?>"><?=$paramValue;?></div>
                </div>
            <?endif;?>
        <?endforeach;?>
    </div>
    <?endif;?>
<?endif;?>
<?endif;?>