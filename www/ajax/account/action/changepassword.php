<?
//session_start();
include_once($_SERVER['DOCUMENT_ROOT'].'/core/config.php');
?>
<?if(empty($_SESSION['USER_ID'])):?>
    <span class="red-text">Доступ запрещен</span>
<?else:?>
    <?if(empty($_POST['old'])||empty($_POST['new'])||empty($_POST['renew'])):?>
        <span class="red-text">Заполните все поля</span>
    <?elseif($_POST['new']!=$_POST['renew']):?>
        <span class="red-text">Пароли не совпадают</span>
    <?else:?>
        <?
        if (empty($MyDB)) {
            $MyDB = new dbconnect;
        }
        $MyDB->Connect();
        $MyDB->Text = "SELECT `password`
        FROM `sys_user`
        WHERE `id`='".$_SESSION['USER_ID']."'";
        $MyDB->Query();
        $MyDB->Assoc();
        $oldcryptpass = crypt($_POST['old'],$MyDB->Data[0]['password']);
        ?>
        <?if($oldcryptpass==$MyDB->Data[0]['password']):?>
        <?
        $cryptpass = crypt($_POST['new'],$MyDB->Data[0]['password']);
        $MyDB->Text = "UPDATE `sys_user` SET `password`='".$cryptpass."' WHERE `id`='".$_SESSION['USER_ID']."'";
        $MyDB->Query();
        $MyDB->ErrorNum();
        ?>
        <?if(empty($MyDB->Error)):?>
        <span><?=PASSISCHANGED;?></span>
        <?
        writeLog ('useraction','Password is changed. User ID: '.$_SESSION['USER_ID'].'\n','account',true);
        ?>
        <?else:?>
        <span class="red-text"><?=ERRORCHNGPASS;?></span>
        <?
        writeLog ('useraction','Error of change password, DB error: '.$MyDB->Error.'. User ID: '.$_SESSION['USER_ID'].'\n','account',true);
        ?>
        <?endif;?>
        <?else:?>
        <span class="red-text"><?=WRONGOLDPASS;?></span>
        <?
        writeLog ('useraction','Error of change password. Wrong old password. User ID: '.$_SESSION['USER_ID'].'\n','account',true);
        ?>
        <?endif;?>
    <?endif;?>
<?endif;?>