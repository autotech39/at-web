<?
//session_start();
include_once($_SERVER['DOCUMENT_ROOT'].'/core/config.php');
?>
<?//if(!empty($_REQUEST['device'])&&$_REQUEST['device']!='NULL'):?>
<script src="/admin/js/lng/<?=SITE_LANG;?>.js"></script>
<?
if(empty($aa_DevId)) {
    $aa_DevId = $_REQUEST['device'];
}
if(empty($dateStart)) {
    $dateStart = $_REQUEST['start'];
}
if(empty($dateEnd)) {
    $dateEnd = $_REQUEST['end'];
}

if (empty($MyDB)) {
    $MyDB = new dbconnect;
}
$MyDB->Connect();
$MyDB->Text = "SELECT `id`, `device`, `name`, `serial`
FROM `userdevices`
WHERE `user`='".$_SESSION['USER_ID']."'
AND `device`='".$aa_DevId."'
LIMIT 1";
$MyDB->Query();
$MyDB->Assoc();
?>
    
<?if(empty($MyDB->Data)):?>
    <?=NOTDATAFORDEVICE;?>
<?else:?>
    <?if (!empty($aa_DevId)): ?>
        <?
        if (empty($MyDB)) {
            $MyDB = new dbconnect;
        }
        $MyDB->Connect();
        $MyDB->Data = array();
        $MyDB->Text = "SELECT *
        FROM `diagnostic_result`
        WHERE `user`='".$_SESSION['USER_ID']."'
        AND `device`='".$aa_DevId."'
        AND `timestamp`>='".date('Y-m-d 00:00:00',strtotime($dateStart))."'
        AND `timestamp`<='".date('Y-m-d 23:59:59',strtotime($dateEnd))."'
        ORDER BY `timestamp` DESC";
        $MyDB->Query();
        $MyDB->Assoc();
        ?>
        <?if(empty($MyDB->Data)):?>
            <?=NOTDATAFORDEVICE;?>
        <?else:?>
            <?
            $dataArr = array();
            foreach ($MyDB->Data as $ind => $data) {
                foreach ($data as $dpk => $dataParam) {
                    $dataArr[$dpk][$ind] = $dataParam;
                }
            }
            //print_r ($dataArr);
            ?>
        <table class="table table-bordered" style="margin-top: 10px;">
            <thead>
            <tr>
                <th rowspan="2"><?=DEVSTATUSPARAM;?></th>
                <th colspan="<?=count($dataArr['timestamp']);?>"><?=DEVSTATUSVALUE;?></th>
            </tr>
            <tr>
                <?foreach($dataArr['timestamp'] as $tk => $timestamp):?>
                <th class="small-header-text">
                    <?=date('d.m.Y H:i',strtotime($timestamp));?>
                </th>
                <?endforeach;?>
            </tr>
            </thead>
                <tbody>
            <?foreach($dataArr as $key => $param):?>
                <?if($key!='id'&&$key!='user'&&$key!='device'&&$key!='timestamp'&&$key!='status'):?>
                <?
                $paramName = strtoupper($key);
                ?>
                    <tr>
                        <td><?=constant($paramName);?></td>
                        <?foreach($param as $pk => $paramData):?>
                        <?
                        $paramValue = '-';
                        $paramClass = '';
                        if($paramData==0) {
                            $paramValue = NO;
                        }
                        elseif($paramData==1) {
                            $paramValue = YES;
                            $paramClass = ' red-text';
                        }
                        elseif(!empty($paramData)) {
                            $paramValue = $paramData;
                        }
                        ?>
                        <td class="<?=$paramClass;?>"><?=$paramValue;?></td>
                        <?endforeach;?>
                    </tr>
                <?endif;?>
            <?endforeach;?>
                </tbody>
        </table>
        <?endif;?>
    <?endif;?>
<?endif;?>