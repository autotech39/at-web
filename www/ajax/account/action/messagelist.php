<?
//session_start();
include_once($_SERVER['DOCUMENT_ROOT'].'/core/config.php');
?>
<?//if(!empty($_REQUEST['device'])&&$_REQUEST['device']!='NULL'):?>
<script src="/admin/js/lng/<?=SITE_LANG;?>.js"></script>
<?
if(empty($aa_DevId)) {
    $aa_DevId = $_REQUEST['device'];
}
if(empty($dateStart)) {
    $dateStart = $_REQUEST['start'];
}
if(empty($dateEnd)) {
    $dateEnd = $_REQUEST['end'];
}

if (empty($MyDB)) {
    $MyDB = new dbconnect;
}
$MyDB->Connect();
$MyDB->Text = "SELECT `id`, `device`, `name`, `serial`
FROM `userdevices`
WHERE `user`='".$_SESSION['USER_ID']."'
AND `device`='".$aa_DevId."'
LIMIT 1";
$MyDB->Query();
$MyDB->Assoc();
?>
    
<?if(empty($MyDB->Data)):?>
    <?=NOTDATAFORDEVICE;?>
<?else:?>
    <?
    function setStrLength($str,$minLn,$add="&nbsp;") {
        $ln = mb_strlen($str);
        $rep = 0;
        if ($minLn>$ln) {
            $rep = $minLn - $ln;
        }
        $result = $str.str_repeat($add,$rep);
        return $result;
    }
    $MyDB->Data = array();
    $start = date('Y-m-d H:i:s',strtotime($dateStart));
    $ms = date('n',strtotime($start));
    $ds = date('j',strtotime($start));
    $ys = date('Y',strtotime($start));
    $end = date('Y-m-d H:i:s',strtotime($dateEnd));
    $me = date('n',strtotime($end));
    $de = date('j',strtotime($end));
    $ye = date('Y',strtotime($end));
    if(empty($start)||!checkdate($ms,$ds,$ys)) {
        $start = date('Y-m-d', strtotime('-1 day'));
    }
    if(empty($end)||!checkdate($me,$de,$ye)) {
        $end = date('Y-m-d');
    }
    //628WBMpniFMD
    $whereDate = "AND `datetime`>='".$start." 00:00:00' AND `datetime`<='".$end." 23:59:59' ";
    $MyDB->Text = "SELECT `user`, `device`, `error_code`, `datetime`, `gps`, `text`
    FROM `devicemessage`
    WHERE `user`='".$_SESSION['USER_ID']."' 
    AND `device`='".$aa_DevId."' 
    ".$whereDate."
    AND `status`='1'
    ORDER BY `datetime` DESC";
    $MyDB->Query();
    $MyDB->Assoc();
    ?>
    <?if(empty($MyDB->Data)):?>
        <?=NOTDATAFORDEVICE;?>
    <?else:?>
    <?
    $toFileData = '';
    $toXlsData = '';
    $toXmlData = '';
    //$toFileData .= ' '.setStrLength(ERROR_CODE,10).' | '.setStrLength(DATETIMEH,20).' | '.setStrLength(GPSDATA,28).' | '.MESSAGETEXT.' \r\n';
    ?>
    
    <table class="table" id="messagelisttable">
        <thead>
            <tr>
                <th class="text-center">
                    <?=ERROR_CODE;?>
                </th>
                <th class="text-center">
                    <?=DATETIMEH;?>
                </th>
                <th class="text-center">
                    <?=GPSDATA;?>
                </th>
                <th class="text-center">
                    <?=MESSAGETEXT;?>
                </th>
            </tr>
        </thead>
        <tbody>
        <?
        $toXlsData .= '<table><thead><tr><th><b>'.ERROR_CODE.'</b></th><th><b>'.DATETIMEH.'</b></th><th><b>'.GPSDATA.'</b></th><th><b>'.MESSAGETEXT.'</b></th></tr></thead><tbody>';
        $toXmlData .= '<messages>';
        ?>
        <?foreach ($MyDB->Data as $km => $message) :?>
            <tr>
                <td>
                    <?=$message['error_code'];?>
                </td>
                <td>
                    <?
                    $dateTime = date('d.m.Y H:i:s',strtotime($message['datetime']));
                    ?>
                    <?=$dateTime;?>
                </td>
                <td>
                    <?
                    $paramArr = explode(':',$message['gps']);
                    $paramValue = $paramArr[0].'&deg;'.$paramArr[1].', '.$paramArr[2].'&deg;'.$paramArr[3];
                    $paramStr = $paramArr[0].''.$paramArr[1].', '.$paramArr[2].''.$paramArr[3];
                    //echo mb_strlen($paramValue).' || ';
                    ?>
                    <?=$paramValue;?>
                </td>
                <td>
                    <?=$message['text'];?>
                </td>
            </tr>
            <?
            $toXlsData .= '<tr><td>'.$message['error_code'].'</td><td>'.$dateTime.'</td><td>'.$paramValue.'</td><td>'.$message['text'].'</td></tr>';
            $toXmlData .= '<message><code>'.$message['error_code'].'</code><datetime>'.$dateTime.'</datetime><location>'.$paramValue.'</location><text>'.$message['text'].'</text></message>';
            $toFileData .= $dateTime.'\r\n'.ERROR_CODE.': '.$message['error_code'].'\r\nСообщение: '.$message['text'].'\r\nЛокация: '.$paramValue.'\r\n \r\n';
            //$toFileData .= ' &nbsp; '.setStrLength($message['error_code'],8).' | '.setStrLength($dateTime,20).' | '.setStrLength($paramStr,28).' | '.$message['text'].' \r\n';
            $headFile = 'Device messages '.date('d.m.Y',strtotime($start)).'-'.date('d.m.Y',strtotime($end));
            ?>
        <?endforeach;?>
        <?
        $toXlsData .= '</tbody></table>';
        $toXmlData .= '</messages>';
        ?>
        </tbody>
    </table>
    <div class="row">
        <div class="col-md-12 text-left" style="padding: 0 30px;">
            <a class="btn btn-default" href="/getfile?data=<?=$toFileData;?>&file=<?=$headFile;?>"><?=MESSAGE_TO_FILE;?>.TXT</a>
            <a class="btn btn-default" id="toxlsbutton" href="/getfile?data=<?=$toXlsData;?>&file=<?=$headFile;?>&type=xls"><?=MESSAGE_TO_FILE;?>.XLS</a>
            <a class="btn btn-default" id="toxmlbutton" href="/getfile?data=<?=$toXmlData;?>&file=<?=$headFile;?>&type=xml"><?=MESSAGE_TO_FILE;?>.XML</a>
            <script>
            //var tableData = '<table>'+$('#messagelisttable').html()+'</table>';
            //$('#toxlsbutton').attr('href','/getfile?data='+tableData+'&file=<?=$headFile;?>&type=xls');
            //$('#toxmlbutton').attr('href','/getfile?data='+tableData+'&file=<?=$headFile;?>&type=xml');
            </script>
        </div>
    </div>
    
    <?endif;?>
<?endif;?>
<?//endif;?>