<?
include_once($_SERVER['DOCUMENT_ROOT'].'/core/config.php');
?>
<?if(empty($_SESSION['USER_ID'])):?>
    <span class="red-text">Доступ запрещен</span>
<?else:?>
    <?
    if (empty($MyDB)) {
        $MyDB = new dbconnect;
    }
    $MyDB->Connect();
    $MyDB->Text = "SELECT `id`, `serial`, `name`
    FROM `userdevices`
    WHERE `user`='".$_SESSION['USER_ID']."'
    AND `serial`='".$_GET['sn']."'";
    $MyDB->Query();
    $MyDB->Assoc();
    ?>
    <?if(empty($MyDB->Data)):?>
    <span class="red-text"><?=DEVICENOTFOUND;?></span>
    <?else:?>
    <span class="devrenametext"><?=SN;?>: <b><?=$_GET['sn'];?></b></span>
    &nbsp;&nbsp;&nbsp;
    <span class="devrenametext"><?=DEVNAME;?>: <b><?=$MyDB->Data[0]['name'];?></b></span>
    <br />
    <input type="hidden" name="devicenumber" id="devicenumber" value="<?=$_GET['sn'];?>" />
    <input type="hidden" name="deviceoldname" id="deviceoldname" value="<?=$MyDB->Data[0]['name'];?>" />
    <div class="row">
        <div class="col-md-8">
            <input type="text" class="form-control" name="devicenewname" id="devicenewname" placeholder="<?=NEWDEVNAME;?>" />
        </div>
        <div class="col-md-4">
            <input type="button" class="btn btn-default" name="renamedevice" id="renamedevice" value="<?=RENAME;?>" />
        </div>
    </div>
    
    
    <div id="rendeviceresult" class="red-text"></div>
    <script>
    $('#renamedevice').click(function(){
        //$('#rendevicewin .windowcontent').html('12');
        var newname = $('#devicenewname').val();
        if (isEmpty(newname)) {
            $('#devicenewname').addClass('red-light');
            return false;
        }
        else {
            var number = $('#devicenumber').val();
            var oldname =$('#deviceoldname').val();
            var param = 'number='+number+'&oldname='+oldname+'&newname='+newname;
            var result = syncAjax('/ajax/account/action/newdevname.php',param,'post');
            var answer = JSON.parse(result);
            if(answer.answer!=1) {
                $('#rendeviceresult').html(ln.error[answer.error]);
            }
            else {
                $('#rendeviceresult').html(ln.dviceisrenamed);
                $('#devicebox').html(syncAjax('/ajax/account/action/userdevices.php'));
                $('.fullwindowback').hide();
            }
            //$('#rendeviceresult').
        }
    });
    $('#devicenewname').on('input',function(){
        $(this).removeClass('red-light');
    });
    </script>
    <?endif;?>
<?endif;?>