<?
include_once($_SERVER['DOCUMENT_ROOT'].'/core/config.php');
?>
<?if (empty($_SESSION['USER_ID'])):?>
    <?=USERDATANOTFOUND;?>
<?else:?>
    <?
    if (empty($MyDB)) {
        $MyDB = new dbconnect;
    }
    $MyDB->Connect();
    $MyDB->Text = "SELECT `id`, `name`, `serial`
    FROM `userdevices`
    WHERE `user`='".$_SESSION['USER_ID']."'";
    $MyDB->Query();
    $MyDB->Assoc();
    ?>
    <?if (empty($MyDB->Data)):?>
        <span class="no-data"><?=REGDEVICENOTFOUND;?></span>
    <?else:?>
        <div class="devicelist table-block">
            <div class="deviceboxhead table-row">
                <div class="table-cell">#</div>
                <div class="table-cell"><?=DEVNAME;?></div>
                <div class="table-cell"><?=SERIALNUMBER;?></div>
                <div class="table-cell"></div>
                <div class="table-cell"></div>
            </div>
        <?$n=1;?>
        <?foreach($MyDB->Data as $k => $device):?>
            <div class="table-row">
                <div class="table-cell"><?=$n;?></div>
                <div class="table-cell"><?=$device['name'];?></div>
                <div class="table-cell"><?=$device['serial'];?></div>
                <div class="table-cell"><span class="devicerename" onclick="deviceRename('<?=$device['serial'];?>')" title="<?=RENAMEDEVICE;?>"></span></div>
                <div class="table-cell"><span class="deviceremove" onclick="deviceRemove('<?=$device['serial'];?>')" title="<?=REMOVEDEVICE;?>"></span></div>
            </div>
            <?$n++;?>
        <?endforeach;?>
        </div>
    <?endif;?>
<?endif;?>