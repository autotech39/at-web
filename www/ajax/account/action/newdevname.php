<?
//session_start();
include_once($_SERVER['DOCUMENT_ROOT'].'/core/config.php');
?>
<?if(empty($_SESSION['USER_ID'])):?>
<?
writeLog ('useraction','Device rename error. Access denied. User ID: '.$_SESSION['USER_ID'],'account',true);
?>
{
    "answer": 0,
    "error": "9",
    "description": "Access denied"
}
<?else:?>
    <?if(empty($_POST['newname'])):?>
    <?
    writeLog ('useraction','Device rename error. Not all fields are filled in. User ID: '.$_SESSION['USER_ID'],'account',true);
    ?>
{
    "answer": 0,
    "error": "13",
    "description": "Not all fields are filled in"
}
    <?else:?>
        <?
        if (empty($MyDB)) {
            $MyDB = new dbconnect;
        }
        $MyDB->Connect();
        $MyDB->Text = "SELECT `id`
        FROM `userdevices`
        WHERE `user`='".$_SESSION['USER_ID']."'
        AND `serial`='".$_POST['number']."'
        AND `name`='".$_POST['oldname']."'";
        $MyDB->Query();
        $MyDB->Assoc();
        ?>
        <?if(empty($MyDB->Data)):?>
        <?
        writeLog ('useraction','Device rename error. Device not found. User ID: '.$_SESSION['USER_ID'],'account',true);
        ?>
{
    "answer": 0,
    "error": "15",
    "description": "Device not found"
}
        <?else:?>
            <?
            $id = $MyDB->Data[0]['id'];
            $MyDB->Text = "UPDATE `userdevices` SET `name`='".$_POST['newname']."' WHERE `user`='".$_SESSION['USER_ID']."' AND `id`='".$id."' AND `serial`='".$_POST['number']."' AND `name`='".$_POST['oldname']."'";
            $MyDB->Query();
            $MyDB->ErrorNum();
            ?>
            <?if(empty($MyDB->Error)):?>
            <?
            writeLog ('useraction','Device is renamed. Device SN: '.$_POST['number'].'. User ID: '.$_SESSION['USER_ID'],'account',true);
            ?>
{
    "answer": 1,
    "number": "<?=$_POST['number'];?>",
    "description": "Device successfully renamed"
}
            <?else:?>
            <?
            writeLog ('useraction','Device rename error. Error of saving data. User ID: '.$_SESSION['USER_ID'],'account',true);
            ?>
{
    "answer": 0,
    "error": "16",
    "description": "Error of saving data"
}
            <?endif;?>
        <?endif;?>
    <?endif;?>
<?endif;?>