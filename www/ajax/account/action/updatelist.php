<?
//session_start();
include_once($_SERVER['DOCUMENT_ROOT'].'/core/config.php');
?>
<?
if (empty($MyDB)) {
    $MyDB = new dbconnect;
}
$MyDB->Connect();
$MyDB->Text = "SELECT `id`, 
	`user`, 
    `device`, 
    `serial`, 
    `name`, 
    (SELECT `version` FROM `device_state` 
     WHERE `user`=ud.`user` AND `device`=ud.`device`
     ORDER BY `datetime` DESC LIMIT 1) AS dev_version, 
    (SELECT MAX(`version`) FROM `software`) AS db_version, 
    (SELECT `need_update` FROM `devices`
     WHERE `id`=ud.`device`) AS need_version
FROM `userdevices` ud
WHERE `user`='".$_SESSION['USER_ID']."'
ORDER BY `name`";
$MyDB->Query();
$MyDB->Assoc();
$deviceDataArr = $MyDB->Data;
?>

<div class="selectdevice">
    <table class="table">
        <thead>
            <tr class="dark-text">
                <th><?=DEVNAME;?></th>
                <th><?=SERIALNUMBER;?></th>
                <th><?=DEVVERSION;?></th>
                <th><?=AVVERSION;?></th>
                <th></th>
            </tr>
        </thead>
        <tbody>
    <?if (empty($deviceDataArr)):?>
            <tr>
                <td colspan="5">
                    <?=REGDEVICENOTFOUND;?>
                </td>
            </tr>
    <?else:?>
        <?foreach ($deviceDataArr as $ddk => $deviceData):?>
            <?
            $needUpdClass = '';
            $needUpd = 0;
            if ($deviceData['db_version']>$deviceData['dev_version'] && $deviceData['db_version']>$deviceData['need_version']) {
                $needUpdClass = ' class="red-text"';
                $needUpd = 1;
            }
            elseif ($deviceData['db_version']>$deviceData['dev_version'] && $deviceData['db_version']==$deviceData['need_version'] && $deviceData['dev_version']<$deviceData['need_version']) {
                $needUpd = 2;
            }
            ?>
            <tr<?=$needUpdClass;?>>
                <td>
                    <?=$deviceData['name'];?>
                </td>
                <td>
                    <?=$deviceData['serial'];?>
                </td>
                <td>
                    <?=$deviceData['dev_version'];?>
                </td>
                <td>
                    <?=$deviceData['db_version'];?>
                </td>
                <td style="width: 20%;">
            <?if ($needUpd==1):?>
                    <button class="btn btn-primary" onclick="setUpdate(<?=$deviceData['device'];?>,<?=$deviceData['db_version'];?>);"><?=UPDATESW;?></button>
            <?elseif ($needUpd==2):?>
                    <span class="red-text"><?=UPDATEREQUESTED;?></span>
            <?endif;?>
                </td>
            </tr>
        <?endforeach;?>
    <?endif;?>
        </tbody>
    </table>
</div>

<script>
var setUpdate = function(dev,ver){
    var setUpd = syncAjax('/ajax/account/action/setupdate.php?device='+dev+'&version='+ver);
    if (setUpd=='1') {
        $('#text-result').html(ln.error[15]);
    }
    else if (setUpd=='2') {
        $('#text-result').html(ln.error[16]);
    }
    else {
        $('#udatecontent').html('');
        $('#udatecontent').html(ln.load);
        var content = syncAjax('/ajax/account/action/updatelist.php');
        $('#udatecontent').html(content);
    }
};
</script>