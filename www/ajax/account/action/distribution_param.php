<?
//session_start();
include_once($_SERVER['DOCUMENT_ROOT'].'/core/config.php');
?>
<?if(!empty($_REQUEST['device'])&&$_REQUEST['device']!='NULL'):?>
<?
if (empty($MyDB)) {
    $MyDB = new dbconnect;
}
$MyDB->Connect();
$MyDB->Text = "SELECT `id`, `name`, `serial`
FROM `userdevices`
WHERE `user`='".$_SESSION['USER_ID']."'";
$MyDB->Query();
$MyDB->Assoc();
$deviceArr = $MyDB->Data[0];
?>
<?if(empty($MyDB->Data)):?>
    <?=NOTDATAFORDEVICE;?>
<?else:?>
    <?
    $MyDB->Text = "SELECT `id`, `value`
    FROM `distribution_param`
    WHERE `user`='".$_SESSION['USER_ID']."'
    AND `device` = '".$_REQUEST['device']."'
    AND (`is_email` IS NULL OR `is_email`='0')
    ORDER BY `timestamp` DESC
    LIMIT 3";
    $MyDB->Query();
    $MyDB->Assoc();
    $phoneArr = $MyDB->Data;
    $MyDB->Text = "SELECT `id`, `value`
    FROM `distribution_param`
    WHERE `user`='".$_SESSION['USER_ID']."'
    AND `device` = '".$_REQUEST['device']."'
    AND `is_email`='1'
    ORDER BY `timestamp` DESC
    LIMIT 3";
    $MyDB->Query();
    $MyDB->Assoc();
    $emailArr = $MyDB->Data;
    
    $phonesIds = array(0,0,0);
    $emailsIds = array(0,0,0);
    $phonesVals = array('','','');
    $emailsVals = array('','','');
    $pi = 0;
    foreach($phoneArr as $p => $phone) {
        $phonesIds[$pi] = $phone['id'];
        $phonesVals[$pi] = $phone['value'];
        $pi++;
    }
    $ei = 0;
    foreach($emailArr as $e => $email) {
        $emailsIds[$ei] = $email['id'];
        $emailsVals[$ei] = $email['value'];
        $ei++;
    }
    for($i=0;$i<=2;$i++) {
        $phoneData[$i]['id'] = $phonesIds[$i];
        $phoneData[$i]['value'] = $phonesVals[$i];
        $emailData[$i]['id'] = $emailsIds[$i];
        $emailData[$i]['value'] = $emailsVals[$i];
    }
    //print_r($phoneData);
    //print_r($emailData);
    //print_r($_REQUEST);
    ?>
    <span class="dark-text"><strong><?=DEVICE;?>:</strong> <?=$deviceArr['name'];?>,&nbsp;<strong><?=SN;?>:</strong> <?=$deviceArr['serial'];?></span>
    <div class="distributionparam_content">
    <h4><?=DISTRIB_PHONE_NUMBERS;?></h4>
    
    <form id="phoneform" name="phoneform" onsubmit="retun false;" role="form">
        <input type="text" class="numberofphone form-control short-input" name="number[0]" id="number[0]" value="<?=$phoneData[0]['value'];?>" placeholder="<?=PHONE_NUMBER;?>" />
        <input type="hidden" name="pid[0]" id="pid[0]" value="<?=$phoneData[0]['id'];?>" />
        <br />
        <input type="text" class="numberofphone form-control short-input" name="number[1]" id="number[1]" value="<?=$phoneData[1]['value'];?>" placeholder="<?=PHONE_NUMBER;?>" />
        <input type="hidden" name="pid[1]" id="pid[1]" value="<?=$phoneData[1]['id'];?>" />
        <br />
        <input type="text" class="numberofphone form-control short-input" name="number[2]" id="number[2]" value="<?=$phoneData[2]['value'];?>" placeholder="<?=PHONE_NUMBER;?>" />
        <input type="hidden" name="pid[2]" id="pid[2]" value="<?=$phoneData[2]['id'];?>" />
        <input type="hidden" name="device" id="devicephone" value="<?=$_REQUEST['device'];?>" />
    </form>
    <br />
    <input type="button" class="btn btn-default" id="savedistribnumbers" value="<?=SAVE;?>" /><div id="savedistribnumbers_result"><?=!empty($_REQUEST['presult'])?$_REQUEST['presult']:'';?></div>
    </div>
    
    <div class="distributionparam_content">
    <h4><?=DISTRIB_EMAILS;?></h4>
    <form id="emailform" name="emailform" onsubmit="retun false;">
        <input type="text" class="emailaddress form-control short-input" name="email[0]" id="email[0]" value="<?=$emailData[0]['value'];?>" placeholder="<?=EMAIL;?>" />
        <input type="hidden" name="eid[0]" id="eid[0]" value="<?=$emailData[0]['id'];?>" />
        <br />
        <input type="text" class="emailaddress form-control short-input" name="email[1]" id="email[1]" value="<?=$emailData[1]['value'];?>" placeholder="<?=EMAIL;?>" />
        <input type="hidden" name="eid[1]" id="eid[1]" value="<?=$emailData[1]['id'];?>" />
        <br />
        <input type="text" class="emailaddress form-control short-input" name="email[2]" id="email[2]" value="<?=$emailData[2]['value'];?>" placeholder="<?=EMAIL;?>" />
        <input type="hidden" name="eid[2]" id="eid[2]" value="<?=$emailData[2]['id'];?>" />
        <input type="hidden" name="device" id="deviceemail" value="<?=$_REQUEST['device'];?>" />
    </form>
    <br />
    <input type="button" class="btn btn-default" id="savedistribemails" value="<?=SAVE;?>" /><div id="savedistribemails_result"><?=!empty($_REQUEST['eresult'])?$_REQUEST['eresult']:'';?></div>
    </div>
<script>
$('#numberofphone').on('input',function(){
    $('#savedistribnumbers_result').html('');
});
$('#emailaddress').on('input',function(){
    $('#savedistribemails_result').html('');
});
$('#savedistribnumbers').click(function(){
    var formdata = $('#phoneform').serialize();
    //console.log(formdata);
    var result = syncAjax('/ajax/account/action/phonessave.php',formdata,'post');
    $('#savedistribnumbers_result').html(result);
    var dev = $('#devicephone').val();
    var content = syncAjax('/ajax/account/action/distribution_param.php?device='+dev+'&presult='+$('#savedistribnumbers_result').html());
    $('#devicedistribcontent').html(content);
});
$('#savedistribemails').click(function(){
    var formdata = $('#emailform').serialize();
    //console.log(formdata);
    var result = syncAjax('/ajax/account/action/emailssave.php',formdata,'post');
    $('#savedistribemails_result').html(result);
    var dev = $('#deviceemail').val();
    var content = syncAjax('/ajax/account/action/distribution_param.php?device='+dev+'&eresult='+$('#savedistribemails_result').html());
    $('#devicedistribcontent').html(content);
});
</script>
<?endif;?>
<?endif;?>