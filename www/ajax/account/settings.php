<?
include_once($_SERVER['DOCUMENT_ROOT'].'/core/config.php');
?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
<script src="/admin/js/jquery-ui/jquery-ui.min.js"></script>
<script src="/admin/js/lng/<?=SITE_LANG;?>.js"></script>
<link rel="stylesheet" type="text/css" href="/admin/js/jquery-ui/jquery-ui.min.css">
<h3 class="accountheader"><?=SETTINGS;?></h3>

<div id="settingbox">
    <h3><?=DEVICES;?></h3>
    <div class="settingblock">
        <div id="devicebox">
            <?include_once('action/userdevices.php');?>
        </div>
        <input type="button" class="btn btn-default" value="<?=REGISTERDEVICE;?>" id="newdevice" />
    </div>
    <h3><?=CHANGEPASSWORD;?></h3>
    <div class="settingblock">
        <div class="row">
            <div class="col-md-3">
                <input type="password" name="oldpass" id="oldpass" class="form-control" placeholder="<?=OLDPASS;?>" required />
            </div>
            <div class="col-md-3">
                <input type="password" name="newpass" id="newpass" class="form-control" placeholder="<?=NEWPASS;?>" required />
            </div>
            <div class="col-md-3">
                <input type="password" name="renewpass" id="renewpass" class="form-control" placeholder="<?=RE_PASSWORD;?>" required />
            </div>
            <div class="col-md-3">
                <input type="button" name="changepass" id="changepass" class="btn btn-default wide-button" value="<?=CHANGEPASSWORD;?>" />
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div id="changepassresult"></div>
            </div>
        </div>
        
    </div>
</div>
<div class="fullwindowback" id="newdevicewin">
    <!--<div class="windowcontainer">-->
        <div class="windowbox">
            <h4 class="windowhead"><?=REGISTERDEVICE;?><span class="windowclose">&#215;</span></h4>
            <div class="windowcontent">
                <div class="row">
                    <div class="col-md-4">
                        <input type="text" class="form-control" name="devicename" id="devicename" placeholder="<?=DEVNAME;?>" />
                    </div>
                    <div class="col-md-4">
                        <input type="text" class="form-control" name="devicenum" id="devicenum" placeholder="<?=SERIALNUMBER;?>" />
                    </div>
                    <div class="col-md-4">
                        <input type="button" class="form-control" name="adddevice" id="adddevice" value="<?=REGDEV;?>" />
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div id="regdeviceresult" class="red-text"></div>
                    </div>
                </div>
            </div>
        </div>
    <!--</div>-->
</div>
<div class="fullwindowback" id="rendevicewin">
        <div class="windowbox">
            <h4 class="windowhead"><?=RENAMEDEVICE;?><span class="windowclose">&#215;</span></h4>
            <div class="windowcontent">
            </div>
        </div>
</div>
<script>
$('#settingbox').accordion({
    collapsible: true,
    heightStyle: 'content',
    active: 0
});
$('#newdevice').click(function(){
    $('#newdevicewin').show();
});
$('.windowclose').click(function(){
    $('.fullwindowback').hide();
    $('.fullwindowback input[type="text"]').val('');
});
$('.chngpassinput').on('input',function(){
    $('.chngpassinput').removeClass('red-light');
    $('#changepassresult').html('');
});
$('#changepass').click(function(){
    var old = $('#oldpass').val();
    var newp = $('#newpass').val();
    var renp = $('#renewpass').val();
    if(isEmpty(old)) {
        $('#oldpass').addClass('red-light');
        return false;
    }
    else if(isEmpty(newp)) {
        $('#newpass').addClass('red-light');
        return false;
    }
    if(newp!=renp) {
        $('#newpass').addClass('red-light');
        $('#renewpass').addClass('red-light');
        return false;
    }
    var param = 'old='+old+'&new='+newp+'&renew='+renp;
    var chngpass = syncAjax('/ajax/account/action/changepassword.php',param,'post');
    $('#changepassresult').html(chngpass);
    //console.log(chngpass);
});
$('#adddevice').click(function(){
    $('#regdeviceresult').html('');
    var number = $('#devicenum').val();
    var name = $('#devicename').val();
    var param = 'name='+name+'&number='+number;
    var regdev = syncAjax('/ajax/account/action/regdevice.php',param,'post');
    var answer = JSON.parse(regdev);
    if (answer.answer!=1) {
        //console.log(answer);
        //console.log(ln.error);
        $('#regdeviceresult').html(ln.error[answer.error]);
    }
    else {
        $('#devicebox').html(syncAjax('/ajax/account/action/userdevices.php'));
        $('.fullwindowback').hide();
    }
});
function deviceRename(sn) {
    if(isEmpty(sn)) {
        return false;
    }
    else {
        $('#rendevicewin').show();
        $('#rendevicewin .windowcontent').html(syncAjax('/ajax/account/action/renamedevice.php?sn='+sn));
    }
}
function deviceRemove(sn) {
    if(confirm(ln.suredevremove+' '+sn+'?')) {
        var result = syncAjax('/ajax/account/action/removedevice.php?sn='+sn);
        var answer = JSON.parse(result);
        if(answer.answer!=1) {
            alert(ln.error[answer.error]);
        }
        else {
            $('#devicebox').html(syncAjax('/ajax/account/action/userdevices.php'));
        }
    }
}
</script>
<script src="/ajax/account/js/settings.js"></script>