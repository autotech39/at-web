<?
include_once($_SERVER['DOCUMENT_ROOT'].'/core/config.php');
?>
<script src="/admin/js/lng/<?=SITE_LANG;?>.js"></script>

<h3 class="accountheader"><?=MESSAGESOFASO;?></h3>
<script src="/ajax/account/js/message.js"></script>
    <!--<script src="/admin/js/jquery-ui/jquery-ui.min.js"></script>-->
    <!--link rel="stylesheet" type="text/css" href="/admin/js/jquery-ui/jquery-ui.min.css">-->
    
    <link href="/ajax/js/datepickerair/css/datepicker.min.css" rel="stylesheet" type="text/css">
	<script src="/ajax/js/datepickerair/js/datepicker.min.js"></script>
<?
if (empty($MyDB)) {
    $MyDB = new dbconnect;
}
$MyDB->Connect();
$MyDB->Text = "SELECT `id`, `device`, `name`, `serial`
FROM `userdevices`
WHERE `user`='".$_SESSION['USER_ID']."'
ORDER BY `name`";
$MyDB->Query();
$MyDB->Assoc();
?>
<div class="selectdevice">
<?if (empty($MyDB->Data)):?>
    <span class="no-data"><?=REGDEVICENOTFOUND;?></span>
<?else:?>
    <div class="row">
        <div class="col-md-3">
    <select id="deviceselect" class="form-control">
    <?if(count($MyDB->Data)>1):?>
        <option value="NULL"><?=SELECTDEVICE;?></option>
    <?elseif(count($MyDB->Data)==1):?>
    <script>
        var dev = <?=$MyDB->Data[0]['device'];?>;
        $('#devicestatuscontent').html(ln.load);
        var start = $('#datestart').val();
        var end = $('#dateend').val();
        var content = syncAjax('/ajax/account/action/messagelist.php?device='+dev+'&start='+start+'&end='+end);
        $('#devicestatuscontent').html(content);
    </script>
    <?endif;?>
    <?foreach($MyDB->Data as $n => $device):?>
        <option value="<?=$device['device'];?>"><?=$device['name'];?></option>
    <?endforeach;?>
    </select>
        </div>
    <?
    $dateStart = date('d.m.Y', strtotime('-1 day'));
    $dateEnd = date('d.m.Y');
    ?>
    <div class="col-md-3">
    <input class="datepickerair form-control" type="text" name="datestart" id="datestart" placeholder="<?=DD_MM_YYYY;?>" value="<?=$dateStart;?>" />
    </div>
    <div class="col-md-3">
    <input class="datepickerair form-control" type="text" name="dateend" id="dateend" placeholder="<?=DD_MM_YYYY;?>" value="<?=$dateEnd;?>" />
    </div>
    <div class="col-md-3"></div>
    </div>
<?endif;?>
    <div id="devicestatuscontent"></div>
</div>
<script>
$(document).ready(function() {
    /*$( ".datepicker" ).datepicker({ dateFormat: "dd.mm.yy"});
    if (ln.code=='ru') {
        $.datepicker.regional['ru'] = {
            monthNames: ['Январь', 'Февраль', 'Март', 'Апрель',
            'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь',
            'Октябрь', 'Ноябрь', 'Декабрь'],
            dayNamesMin: ['Вс','Пн','Вт','Ср','Чт','Пт','Сб'],
            firstDay: 1,
            };
        $.datepicker.setDefaults($.datepicker.regional['ru']);
    }*/
    var dpLang = {};
    dpLang['ru'] =  {
        days: ['Воскресенье','Понедельник','Вторник','Среда','Четверг','Пятница','Суббота'],
        daysShort: ['Вос','Пон','Вто','Сре','Чет','Пят','Суб'],
        daysMin: ['Вс','Пн','Вт','Ср','Чт','Пт','Сб'],
        months: ['Январь','Февраль','Март','Апрель','Май','Июнь','Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],
        monthsShort: ['Янв','Фев','Мар','Апр','Май','Июн','Июл','Авг','Сен','Окт','Ноя','Дек'],
        today: 'Сегодня',
        clear: 'Очистить',
        dateFormat: 'dd.mm.yyyy',
        firstDay: 1
    };
    dpLang['en'] = {
        days: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
        daysShort: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
        daysMin: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
        months: ['January','February','March','April','May','June', 'July','August','September','October','November','December'],
        monthsShort: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
        today: 'Today',
        clear: 'Clear',
        dateFormat: 'mm/dd/yy',
        firstDay: 0
    };
    $('.datepickerair').datepicker({
        language: dpLang[ln.code],
        autoClose: true
    });
});
$('#deviceselect').change(function(){
    $('#devicestatuscontent').html('');
    var dev = $(this).val();
    $('#devicestatuscontent').html(ln.load);
    var start = $('#datestart').val();
    var end = $('#dateend').val();
    var content = syncAjax('/ajax/account/action/messagelist.php?device='+dev+'&start='+start+'&end='+end);
    $('#devicestatuscontent').html(content);
});
</script>