<?
//session_start();
include_once($_SERVER['DOCUMENT_ROOT'].'/core/config.php');
?>
<?
if(!empty($_REQUEST['devid'])) {
        $aa_DevId = $_REQUEST['devid'];
}
if(!empty($_REQUEST['devnum'])) {
        $aa_DevNum = $_REQUEST['devnum'];
}
?>
<div class="row">
    </div>
    <div class="col-md-3 text-center">
        <button class="btn btn-primary" style="width:95%;min-width:150px;margin-bottom:5px;" onclick="showSubPage('message','<?=$aa_DevId;?>','<?=$aa_DevNum;?>');">Сообщения АСО</button>
    </div>
    <div class="col-md-3 text-center">
        <button class="btn btn-primary" style="width:95%;min-width:150px;margin-bottom:5px;" onclick="showSubPage('carstate','<?=$aa_DevId;?>','<?=$aa_DevNum;?>');">Состояния Авто</button>
    </div>
    <div class="col-md-3 text-center">
        <button class="btn btn-default" style="width:95%;min-width:150px;margin-bottom:5px;">Состояния АСО</button>
    </div>
    <div class="col-md-3 text-center">
        <button class="btn btn-primary glyphicon glyphicon-home" style="width:95%;min-width:150px;margin-bottom:5px;" onclick="showSubPage('basic','<?=$aa_DevId;?>','<?=$aa_DevNum;?>');"></button>
    </div>
</div>
<div class="row">
    <div class="col-md-12 text-center">
        <div class="accountblock-content">
            <div class="accountblock-header">
                Состояния АСО - Устройство <?=$aa_DevName?>. SN <?=$aa_DevNum?>
            </div>
            <div id="messagesbox">
            <?if (!empty($aa_DevId)): ?>
                <?
                if (empty($MyDB)) {
                    $MyDB = new dbconnect;
                }
                $MyDB->Connect();
                $MyDB->Data = array();
                $MyDB->Text = "SELECT *
                FROM `diagnostic_result`
                WHERE `user`='".$_SESSION['USER_ID']."'
                AND `device`='".$aa_DevId."'
                ORDER BY `timestamp` DESC
                LIMIT 1";
                $MyDB->Query();
                $MyDB->Assoc();
                ?>
                <?if(empty($MyDB->Data)):?>
                    <?=NOTDATAFORDEVICE;?>
                <?else:?>
                    <?if(!empty($MyDB->Data[0]['timestamp'])):?>
                <strong><?=DATARECIEVED;?>:</strong>&nbsp;<?=date('d.m.Y H:i:s',strtotime($MyDB->Data[0]['timestamp']));?>
                    <?endif;?>
                <table class="table">
                    <thead>
                    <tr>
                        <th><?=DEVSTATUSPARAM;?></th>
                        <th><?=DEVSTATUSVALUE;?></th>
                    </tr>
                    </thead>
                    <?foreach($MyDB->Data[0] as $key => $param):?>
                        <?if($key!='id'&&$key!='user'&&$key!='device'&&$key!='timestamp'&&$key!='status'):?>
                        <?
                        $paramName = strtoupper($key);
                        $paramValue = '-';
                        $paramClass = '';
                        if($param==0) {
                            $paramValue = NO;
                        }
                        elseif($param==1) {
                            $paramValue = YES;
                            $paramClass = ' red-text';
                        }
                        elseif(!empty($param)) {
                            $paramValue = $param;
                        }
                        ?>
                        <tbody>
                            <tr>
                                <td><?=constant($paramName);?></td>
                                <td class="<?=$paramClass;?>"><?=$paramValue;?></td>
                            </tr>
                        </tbody>
                        <?endif;?>
                    <?endforeach;?>
                </table>
                <?endif;?>
            <?endif;?>
            </div>
        </div>
    </div>
</div>
<script>
/*function showSubPage(sub) {
    $('#datepickers-container').detach();
    var devid = <?=$_REQUEST['devid']?>;
    var devnum = '<?=$_REQUEST['devnum']?>';
    $('#account-content-app-box').html(ln.load);
    var content = syncAjax('/ajax/account/app/app-content-'+sub+'.php?devid='+devid+'&devnum='+devnum);
    $('#account-content-app-box').html(content);
}*/
</script>