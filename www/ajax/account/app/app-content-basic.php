<?
//session_start();
include_once($_SERVER['DOCUMENT_ROOT'].'/core/config.php');
?>
<?
if (!empty($_REQUEST['devnum'])) {
    $aa_DevNum = $_REQUEST['devnum'];
}
if (!empty($_REQUEST['devnum'])) {
    $aa_DevName = $_REQUEST['devname'];
}
if (!empty($_REQUEST['devnum'])) {
    $aa_DevId = $_REQUEST['devid'];
}
?>
<?if (!empty($aa_DevNum)&&!empty($aa_DevId)): ?>
<?
if (empty($MyDB)) {
    $MyDB = new dbconnect;
}
$MyDB->Connect();
$MyDB->Text = "SELECT `user`, `device`, `error_code`, `datetime`, `gps`, `text`
FROM `devicemessage`
WHERE `user`='".$_SESSION['USER_ID']."' 
AND `device`='".$aa_DevId."' 
AND `status`='1'
ORDER BY `datetime` DESC
LIMIT 3";
$MyDB->Query();
$MyDB->Assoc();
$messagesArr = $MyDB->Data;
?>
<div class="row">
    <div class="col-md-6 text-center">
        <div class="accountblock-top">
            <div class="accountblock-header">
                Сообщения от АСО - Устройство <?=$aa_DevName?>. SN <?=$aa_DevNum?>
            </div>
            <div id="messagesbox">
                <div class="messagelistblock">
                    <div class="row">
                        <div class="col-md-9 text-left">
                        <?if (!empty($messagesArr[0])): ?>
                            <span class="message-head">
                                Код сообщения <?=$messagesArr[0]['error_code'];?>
                            </span>
                            <span class="message-string">
                                <?=$messagesArr[0]['text'];?>
                            </span>
                        </div>
                        <div class="col-md-3 text-right">
                            <span class="message-head">
                                <?=date('d.m.Y',strtotime($messagesArr[0]['datetime']));?>
                            </span>
                            <span class="message-string">
                                <?=date('H:i',strtotime($messagesArr[0]['datetime']));?>
                            </span>
                        <?endif;?>
                        </div>
                    </div>
                </div>
                <div class="messagelistblock">
                    <div class="row">
                        <div class="col-md-9 text-left">
                        <?if (!empty($messagesArr[1])): ?>
                            <span class="message-head">
                                Код сообщения: <?=$messagesArr[1]['error_code'];?>
                            </span>
                            <span class="message-string">
                                <?=$messagesArr[1]['text'];?>
                            </span>
                        </div>
                        <div class="col-md-3 text-right">
                            <span class="message-head">
                                <?=date('d.m.Y',strtotime($messagesArr[1]['datetime']));?>
                            </span>
                            <span class="message-string">
                                <?=date('H:i',strtotime($messagesArr[1]['datetime']));?>
                            </span>
                        <?endif;?>
                        </div>
                    </div>
                </div>
                <div class="messagelistblock">
                    <div class="row">
                        <div class="col-md-9 text-left">
                        <?if (!empty($messagesArr[2])): ?>
                            <span class="message-head">
                                Код сообщения: <?=$messagesArr[2]['error_code'];?>
                            </span>
                            <span class="message-string">
                                <?=$messagesArr[2]['text'];?>
                            </span>
                        </div>
                        <div class="col-md-3 text-right">
                            <span class="message-head">
                                <?=date('d.m.Y',strtotime($messagesArr[2]['datetime']));?>
                            </span>
                            <span class="message-string">
                                <?=date('H:i',strtotime($messagesArr[2]['datetime']));?>
                            </span>
                        <?endif;?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="message-button-box">
                <button class="btn btn-default button-fix160" onclick="showAllMessage();">Показать все</button>
            </div>
        </div>
    </div>
    <div class="col-md-6 text-center">
        <div class="accountblock-top">
            <div class="accountblock-header">
                Сообщения от АСО - Устройство <?=$aa_DevName?>. SN <?=$aa_DevNum?>
            </div>
            <div id="account-carstate">
            <?include_once(ROOT_PATH.'/ajax/account/app/app-carstate-content.php');?>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="accountblock-header"></div>
    </div>
</div>
<div class="row">
    <div class="col-md-6 text-left">
        <div class="accountblock-bottom" id="accbotboxleft">
            <div class="accountblock-header">
                Состояния АСО - Устройство <?=$aa_DevName?>. SN <?=$aa_DevNum?>
            </div>
            <div id="statecontent" style="margin-bottom: 60px;">
            <?
            include_once(ROOT_PATH.'/ajax/account/app/app-state-content.php');
            ?>
            </div>
            <div class="bottom-button-box text-center">
                <button class="btn btn-primary button-fix160" onclick="refreshErrors();">Обновить</button>
                &nbsp;
                <button class="btn btn-default button-fix160" onclick="showAllErrors();">Показать все</button>
            </div>
        </div>
    </div>
    <div class="col-md-6 text-left">
        <div class="accountblock-bottom" id="accbotboxright">
            <div class="accountblock-header">
                Обновление ПО
            </div>
            <div id="account-update-content">
            <?
            include_once(ROOT_PATH.'/ajax/account/app/app-update-content.php');
            ?>
            </div>
        </div>
    </div>
</div>

<script>
var botboxheight = $('#accbotboxleft').height();
console.log(botboxheight);
$('#accbotboxright').css('height',botboxheight+'px');
function showAllMessage() {
    var devid = <?=$aa_DevId?>;
    var devnum = '<?=$aa_DevNum?>';
    $('#account-content-app-box').html('load...');
    var content = syncAjax('/ajax/account/app/app-content-message.php?devid='+devid+'&devnum='+devnum);
    $('#account-content-app-box').html(content);
}
function showAllErrors() {
    var devid = <?=$aa_DevId?>;
    var devnum = '<?=$aa_DevNum?>';
    $('#account-content-app-box').html('load...');
    var content = syncAjax('/ajax/account/app/app-content-state.php?devid='+devid+'&devnum='+devnum);
    $('#account-content-app-box').html(content);
}
function refreshErrors() {
    var devid = <?=$aa_DevId?>;
    var devnum = '<?=$aa_DevNum?>';
    $('#statecontent').html('load...');
    var content = syncAjax('/ajax/account/app/app-state-content.php?devid='+devid+'&devnum='+devnum);
    $('#statecontent').html(content);
}
</script>
<?endif;?>