<?
//session_start();
include_once($_SERVER['DOCUMENT_ROOT'].'/core/config.php');
?>
<?
if(!empty($_REQUEST['devid'])) {
        $aa_DevId = $_REQUEST['devid'];
}
if(!empty($_REQUEST['devnum'])) {
        $aa_DevNum = $_REQUEST['devnum'];
}
$dateStart = date('d.m.Y', strtotime('-1 day'));
$dateEnd = date('d.m.Y');
?>
<div class="row">
    </div>
    <div class="col-md-3 text-center">
        <button class="btn btn-primary" style="width:95%;min-width:150px;margin-bottom:5px;" onclick="showSubPage('message','<?=$aa_DevId;?>','<?=$aa_DevNum;?>');">Сообщения АСО</button>
    </div>
    <div class="col-md-3 text-center">
        <button class="btn btn-primary" style="width:95%;min-width:150px;margin-bottom:5px;" onclick="showSubPage('carstate','<?=$aa_DevId;?>','<?=$aa_DevNum;?>');">Состояния Авто</button>
    </div>
    <div class="col-md-3 text-center">
        <button class="btn btn-default" style="width:95%;min-width:150px;margin-bottom:5px;">Состояния АСО</button>
    </div>
    <div class="col-md-3 text-center">
        <button class="btn btn-primary glyphicon glyphicon-home" style="width:95%;min-width:150px;margin-bottom:5px;" onclick="showSubPage('basic','<?=$aa_DevId;?>','<?=$aa_DevNum;?>');"></button>
    </div>
</div>
<div class="row">
    <div class="col-md-12 text-center">
        <div class="accountblock-content">
            <div class="accountblock-header">
                Состояния АСО - Устройство <?=$aa_DevName?>. SN <?=$aa_DevNum?>
            </div>
            <div id="messagesbox">
            
                    <link href="/ajax/js/datepickerair/css/datepicker.min.css" rel="stylesheet" type="text/css">
	<script src="/ajax/js/datepickerair/js/datepicker.min.js"></script>

                <div class="col-md-3">
    <input class="datepickerair form-control" type="text" name="datestart" id="datestart" placeholder="<?=DD_MM_YYYY;?>" value="<?=$dateStart;?>" />
    </div>
    <div class="col-md-3">
    <input class="datepickerair form-control" type="text" name="dateend" id="dateend" placeholder="<?=DD_MM_YYYY;?>" value="<?=$dateEnd;?>" />
    </div><div class="col-md-3">
        <input type="button" id="refreshmessages" class="refreshdata btn btn-primary" value="<?=REFRESH_DATA;?>" />
        <script>
        $('#refreshmessages').on('click',function(){
            $('#devicestatuscontent').html(ln.load);
            //var dev = $('#deviceselect').val();
            var start = $('#datestart').val();
            var end = $('#dateend').val();
			localStorage.setItem('start', start);
			localStorage.setItem('end', end);
			/*var hash = window.location.hash;
			var hashArr = hash.split('#');
			var preNameArr = hashArr[1].split(';');
			var newHash = '';
			var tmpArr = [];
			for (var i=0; i<preNameArr.lemgth; i++) {
				if (i>0) {
					newHash = newHash+';';
				}
				tmpArr = preNameArr[i].split(':');
				if (tmpArr[0]=='period') {
					newHash = newHash+'period:'+start+':'+end;
				}
				else {
					newHash = newHash+preNameArr[i];
				}
			}*/
            var content = syncAjax('/ajax/account/action/statelist.php?device=<?=$aa_DevId;?>&start='+start+'&end='+end);
            $('#statecontenbox').html(content);
        });
        </script>
    </div>
            <div id="statecontenbox" style="overflow-x: auto;min-width: 100%;">
                
            </div>
            </div>
        </div>
    </div>
</div>
<script>
$(document).ready(function() {
    /*$( ".datepicker" ).datepicker({ dateFormat: "dd.mm.yy"});
    if (ln.code=='ru') {
        $.datepicker.regional['ru'] = {
            monthNames: ['Январь', 'Февраль', 'Март', 'Апрель',
            'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь',
            'Октябрь', 'Ноябрь', 'Декабрь'],
            dayNamesMin: ['Вс','Пн','Вт','Ср','Чт','Пт','Сб'],
            firstDay: 1,
            };
        $.datepicker.setDefaults($.datepicker.regional['ru']);
    }*/
    var dpLang = {};
    dpLang['ru'] =  {
        days: ['Воскресенье','Понедельник','Вторник','Среда','Четверг','Пятница','Суббота'],
        daysShort: ['Вос','Пон','Вто','Сре','Чет','Пят','Суб'],
        daysMin: ['Вс','Пн','Вт','Ср','Чт','Пт','Сб'],
        months: ['Январь','Февраль','Март','Апрель','Май','Июнь','Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],
        monthsShort: ['Янв','Фев','Мар','Апр','Май','Июн','Июл','Авг','Сен','Окт','Ноя','Дек'],
        today: 'Сегодня',
        clear: 'Очистить',
        dateFormat: 'dd.mm.yyyy',
        firstDay: 1
    };
    dpLang['en'] = {
        days: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
        daysShort: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
        daysMin: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
        months: ['January','February','March','April','May','June', 'July','August','September','October','November','December'],
        monthsShort: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
        today: 'Today',
        clear: 'Clear',
        dateFormat: 'mm/dd/yy',
        firstDay: 0
    };
    $('.datepickerair').datepicker({
        language: dpLang[ln.code],
        autoClose: true
    });
});
var startStr = '';
var endStr = '';
var periodStr = '';
if (localStorage.getItem('start')&&localStorage.getItem('end')) {
	endStr=localStorage.getItem('end');
	startStr = localStorage.getItem('start');
	$('#datestart').val(startStr);
    $('#dateend').val(endStr);
	periodStr = '&start='+startStr+'&end='+endStr;
	//console.log(periodStr);
}
$('#statecontenbox').load('/ajax/account/action/statelist.php?device=<?=$aa_DevId;?>'+periodStr);
/*function showSubPage(sub) {
    $('#datepickers-container').detach();
    var devid = <?=$_REQUEST['devid']?>;
    var devnum = '<?=$_REQUEST['devnum']?>';
    $('#account-content-app-box').html(ln.load);
    var content = syncAjax('/ajax/account/app/app-content-'+sub+'.php?devid='+devid+'&devnum='+devnum);
    $('#account-content-app-box').html(content);
}*/
</script>