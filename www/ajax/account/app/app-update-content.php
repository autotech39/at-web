<?
//session_start();
include_once($_SERVER['DOCUMENT_ROOT'].'/core/config.php');
?>
<?
if (empty($MyDB)) {
    $MyDB = new dbconnect;
}
if (!empty($_REQUEST['devid'])) {
    $aa_DevId = $_REQUEST['devid'];
}
?>
<?if (!empty($aa_DevId)): ?>
<?
$MyDB->Connect();
$MyDB->Text = "SELECT `id`, 
	`user`, 
    `device`, 
    `serial`, 
    `name`, 
    (SELECT `version` FROM `device_state` 
     WHERE `user`=ud.`user` AND `device`=ud.`device`
     ORDER BY `datetime` DESC LIMIT 1) AS dev_version, 
    (SELECT MAX(`version`) FROM `software`) AS db_version, 
    (SELECT `need_update` FROM `devices`
     WHERE `id`=ud.`device`) AS need_version
FROM `userdevices` ud
WHERE `user`='".$_SESSION['USER_ID']."'
AND `device`='".$aa_DevId."'
ORDER BY `name`";
$MyDB->Query();
$MyDB->Assoc();
$deviceData = $MyDB->Data[0];
$needUpd = 0;
if ($deviceData['db_version']>$deviceData['dev_version'] && $deviceData['db_version']>$deviceData['need_version']) {
    //$needUpdClass = ' class="red-text"';
    $needUpd = 1;
}
?>
    <?if($needUpd==1):?>
<div class="device-status-head red-text">
    Доступно новое обновление
</div>
<span class="device-status-text">Доступна новая версия ПО АСО вер. <?=$deviceData['db_version'];?></span>
<div class="bottom-button-box text-center">
    <button class="btn btn-primary button-fix160" onclick="setUpdate('<?=$aa_DevId;?>','<?=$deviceData['db_version'];?>')">Обновить</button>
</div>
    <?else:?>
<div class="device-status-head green-text">
    Обновление не требуется
</div>
<span class="device-status-text">На текущий момент все программное обеспечение имеет последние версии</span>
<div class="bottom-button-box text-center">
    <button class="btn btn-default button-fix160" onclick="checkUpdate('<?=$aa_DevId;?>')">Проверить</button>
</div>

    <?endif;?>
<?endif;?>
<script>
var setUpdate = function(dev,ver) {
    var setUpd = syncAjax('/ajax/account/action/setupdate.php?device='+dev+'&version='+ver);
    if (setUpd=='1') {
        $('#text-result').html(ln.error[15]);
    }
    else if (setUpd=='2') {
        $('#text-result').html(ln.error[16]);
    }
    else {
        $('#account-update-content').html('');
        $('#account-update-content').html(ln.load);
        var cont = syncAjax('/ajax/account/app/app-update-content.php?devid='+dev);
		$('#account-update-content').html(cont);
    }
};
var checkUpdate = function(dev) {
	var cont = syncAjax('/ajax/account/app/app-update-content.php?devid='+dev);
	$('#account-update-content').html(cont);
}
</script>