<?
//session_start();
include_once($_SERVER['DOCUMENT_ROOT'].'/core/config.php');
?>
<?
if(!empty($_REQUEST['devid'])) {
        $aa_DevId = $_REQUEST['devid'];
}
if(!empty($_REQUEST['devnum'])) {
        $aa_DevNum = $_REQUEST['devnum'];
}
?>
<?if (!empty($aa_DevId)): ?>
<?
if (empty($MyDB)) {
    $MyDB = new dbconnect;
}
$MyDB->Connect();
$MyDB->Text = "SELECT `id`, `device`, `name`, `serial`
FROM `userdevices`
WHERE `user`='".$_SESSION['USER_ID']."'
AND `device`='".$aa_DevId."'
LIMIT 1";
$MyDB->Query();
$MyDB->Assoc();
?>
<?if(empty($MyDB->Data)):?>
    <?=NOTDATAFORDEVICE;?>
<?else:?>
    <?
    $MyDB->Data = array();
    $MyDB->Text = "SELECT *
    FROM `diagnostic_result`
    WHERE `user`='".$_SESSION['USER_ID']."'
    AND `device`='".$aa_DevId."'
    ORDER BY `timestamp` DESC
    LIMIT 1";
    $MyDB->Query();
    $MyDB->Assoc();
    ?>
    <?if(empty($MyDB->Data)):?>
    <?else:?>
        <?
        $isError = 0;
        $errorsText = '';
        ?>
        <?foreach($MyDB->Data[0] as $key => $param):?>
            <?if($key!='id'&&$key!='user'&&$key!='device'&&$key!='timestamp'&&$key!='status'):?>
            <?
            $paramName = strtoupper($key);
            $paramValue = '-';
            $paramClass = '';
            if($param==0) {
                $paramValue = NO;
            }
            elseif($param==1) {
                $paramValue = YES;
                $paramClass = ' red-text';
                if($isError>0) {
                    $errorsText .= '<br>';
                }
                $errorsText .= '<span class="device-status-text">'.constant($paramName).'</span>';
                $isError++;
            }
            elseif(!empty($param)) {
                $paramValue = $param;
            }
            ?>
            <?endif;?>
        <?endforeach;?>
        <?if($isError>0):?>
            <div class="device-status-head red-text">
                Обнаружена неисправность
            </div>
            <?=$errorsText;?>
        <?else:?>
            <div class="device-status-head green-text">
                Все работает исправно
            </div>
            <span class="device-status-text">На текущий момент все датчики работают исправно</span>
        <?endif;?>
    <?endif;?>
<?endif;?>
<?endif;?>