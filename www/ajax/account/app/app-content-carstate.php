<?
//session_start();
include_once($_SERVER['DOCUMENT_ROOT'].'/core/config.php');
?>
<?
if(!empty($_REQUEST['devid'])) {
        $aa_DevId = $_REQUEST['devid'];
}
if(!empty($_REQUEST['devnum'])) {
        $aa_DevNum = $_REQUEST['devnum'];
}
?>
<div class="row">
    </div>
    <div class="col-md-3 text-center">
        <button class="btn btn-primary" style="width:95%;min-width:150px;margin-bottom:5px;" onclick="showSubPage('message','<?=$aa_DevId;?>','<?=$aa_DevNum;?>');">Сообщения АСО</button>
    </div>
    <div class="col-md-3 text-center">
        <button class="btn btn-default" style="width:95%;min-width:150px;margin-bottom:5px;">Состояния Авто</button>
    </div>
    <div class="col-md-3 text-center">
        <button class="btn btn-primary" style="width:95%;min-width:150px;margin-bottom:5px;" onclick="showSubPage('state','<?=$aa_DevId;?>','<?=$aa_DevNum;?>');">Состояния АСО</button>
    </div>
    <div class="col-md-3 text-center">
        <button class="btn btn-primary glyphicon glyphicon-home" style="width:95%;min-width:150px;margin-bottom:5px;" onclick="showSubPage('basic','<?=$aa_DevId;?>','<?=$aa_DevNum;?>');"></button>
    </div>
</div>
<div class="row">
    <div class="col-md-12 text-center">
        <div class="accountblock-content">
            <div class="accountblock-header">
                Состояния Авто - Устройство <?=$aa_DevName?>. SN <?=$aa_DevNum?>
            </div>
            <div id="messagesbox" style="margin-bottom: 50px;">

<?if (!empty($aa_DevId)): ?>
<?
if (empty($MyDB)) {
    $MyDB = new dbconnect;
}
$MyDB->Connect();
/*$MyDB->Text = "SELECT `id`, `T_UST`, `N_UST`, `GYR_X_UST`, `GYR_Y_UST`, `ACC_X_UST`, `ACC_Y_UST`, `Velocity_UST`, `U_HIGH_UST`, `U_MED_UST`, `U_LOW_UST`, `U_CRIT_UST`, `timestamp` FROM `critical_param` ORDER BY `timestamp` DESC LIMIT 1";
$MyDB->Query();
$MyDB->Assoc();
$criticalParams = $MyDB->Data[0];*/
if (empty($criticalParams)) {
    $criticalParams['T_UST'] = '50';
    $criticalParams['N_UST'] = '10';
    $criticalParams['GYR_X_UST'] = '1000';
    $criticalParams['GYR_Y_UST'] = '1000';
    $criticalParams['GYR_Z_UST'] = '1000';
    $criticalParams['ACC_X_UST'] = '2';
    $criticalParams['ACC_Y_UST'] = '2';
    $criticalParams['ACC_Z_UST'] = '2';
    $criticalParams['Velocity_UST'] = '0';
    $criticalParams['U_HIGH_UST'] = '12.5'; //100
    $criticalParams['U_MED_UST'] = '12'; //75
    $criticalParams['U_LOW_UST'] = '11.5'; //50
    $criticalParams['U_CRIT_UST'] = '11'; //25
}

$MyDB->Text = "SELECT `id`, `device`, `name`, `serial`
FROM `userdevices`
WHERE `user`='".$_SESSION['USER_ID']."'
AND `device`='".$aa_DevId."'
LIMIT 1";
$MyDB->Query();
$MyDB->Assoc();
?>
<?if(empty($MyDB->Data)):?>
    <?=NOTDATAFORDEVICE;?>
<?else:?>
    <?
    $MyDB->Data = array();
    $MyDB->Text = "SELECT *
    FROM `device_state`
    WHERE `user`='".$_SESSION['USER_ID']."'
    AND `device`='".$aa_DevId."'
    ORDER BY `timestamp` DESC
    LIMIT 1";
    $MyDB->Query();
    $MyDB->Assoc();
    ?>
    <?if(empty($MyDB->Data)):?>
        <?=NOTDATAFORDEVICE;?>
    <?else:?>
        <?if(!empty($MyDB->Data[0]['timestamp'])):?>
    <span class="update-info">
        Последнее обновление <?=date('d.m.Y H:i',strtotime($MyDB->Data[0]['timestamp']));?>
    </span>
        <?endif;?>
        <?foreach($MyDB->Data[0] as $key => $param):?>
            <?if($key!='id'&&$key!='user'&&$key!='device'&&$key!='timestamp'&&$key!='status'&&$key!='device_charge'):?>
            <?
            $paramName = strtoupper($key);
            $paramValue = '-';
            $paramClass = '';
            $showParam = 0;
            $level = 0;
            $iconsClass[0] = 'staus-icon-ok';
            $iconsClass[1] = 'staus-icon-warn';
            $iconsClass[2] = 'staus-icon-alert';
            switch ($key) {
                case 'gps':
                    $paramArr = explode(':',$param);
                    $paramValue = 'GPS: '.$paramArr[0].'&deg;'.$paramArr[1].', '.$paramArr[2].'&deg;'.$paramArr[3];
                    $showParam = 1;
                    break;
                /*case 'device_charge':
                    $paramValue = $param.' '.VOLT;
                    break;*/
                case 'car_charge':
                    if ($param<$criticalParams['U_MED_UST']&&$param>$criticalParams['U_CRIT_UST']) {
                        $level = 1;
                    }
                    elseif ($param<=$criticalParams['U_CRIT_UST']) {
                        $level = 2;
                    }
                    $paramValue = $param.' '.VOLT;
                    $showParam = 1;
                    break;
                case 'current_temp':
                    $paramValue = 'Температура в салоне: '.$param.'&deg;C';
                    $showParam = 1;
                    if ($param>$criticalParams['T_UST']) {
                        $level = 2;
                        $paramValue = '<strong>'.$paramValue.'. Возгорание!<strong>';
                    }
                    break;
                case 'fire_temp':
                    $paramValue = $param.'&deg;C';
                    $showParam = 0;
                    break;
                case 'temp_speed':
                    $paramValue = 'Изменение температуры: '.$param.'&deg;C';
                    $showParam = 1;
                    if ($param>$criticalParams['N_UST']) {
                        $level = 2;
                        $paramValue = '<strong>'.$paramValue.'. Возгорание!<strong>';
                    }
                    break;
                case 'smoke':
                    $paramValue = 'Датчик дыма: ';
                    $showParam = 1;
                    if ($param==1) {
                        $level = 2;
                        $paramValue = '<strong>'.$paramValue.' задымление!<strong>';
                    }
                    else {
                        $paramValue .= 'показатели в норме';
                    }
                    break;
                case 'car_speed':
                    $paramValue = $param.' '.KMH;
                    $showParam = 1;
                    break;
                case 'accel_data':
                    $paramArr = explode(':',$param);
                    $paramValue = ACCELERATION.' ('.MSSQ.'): '.AXES.' X: '.$paramArr[0].', '.AXES.' Y: '.$paramArr[1].', '.AXES.' Z: '.$paramArr[2];
                    $showParam = 1;
                    if ($paramArr[0]>$criticalParams['ACC_X_UST']||$paramArr[1]>$criticalParams['ACC_Y_UST']||$paramArr[2]>$criticalParams['ACC_Z_UST']) {
                        $level = 2;
                        $paramValue = '<strong>'.$paramValue.'<strong>';
                    }
                    break;
                case 'gyro_data':
                    $paramArr = explode(':',$param);
                    $paramValue = POSITION.' ('.RAD.'): '.AXES.' X: '.$paramArr[0].', '.AXES.' Y: '.$paramArr[1].', '.AXES.' Z: '.$paramArr[2];
                    $showParam = 1;
                    if ($paramArr[0]>$criticalParams['GYR_X_UST']||$paramArr[1]>$criticalParams['GYR_Y_UST']||$paramArr[2]>$criticalParams['GYR_Z_UST']) {
                        $level = 2;
                        $paramValue = '<strong>'.$paramValue.'<strong>';
                    }
                    break;
                case 'mag_data':
                    $paramArr = explode(':',$param);
                    $paramValue = MAGNETOMETER.' ('.NTESLA.'): '.AXES.' X: '.$paramArr[0].', '.AXES.' Y: '.$paramArr[1].', '.AXES.' Z: '.$paramArr[2];
                    $showParam = 1;
                    break;
                default:
                    $paramValue = $param;
            }
            
            ?>
            <?if($showParam==1):?>
            <div class="message-content-string">
                <div class="message-status-icon <?=$iconsClass[$level];?>"></div>
                <div class="message-content-text text-left">
                    <?=$paramValue;?>
                </div>
            </div>
            <?endif;?>
            <?endif;?>
        <?endforeach;?>
            <div class="message-button-box">
                <button class="btn btn-primary button-fix160" onclick="refreshCarstate();">Обновить</button>
                &nbsp; 
            </div>
            <script>
                function refreshCarstate () {
                    var devid = <?=$aa_DevId?>;
                    var devnum = '<?=$aa_DevNum?>';
                    $('#account-content-app-box').html('load...');
                    var content = syncAjax('/ajax/account/app/app-content-carstate.php?devid='+devid+'&devnum='+devnum);
                    $('#account-content-app-box').html(content);
                }
            </script>
    <?endif;?>
<?endif;?>
<?endif;?>
 
            </div>
        </div>
    </div>
</div>
<script>
/*function showSubPage(sub) {
    $('#datepickers-container').detach();
    var devid = <?=$_REQUEST['devid']?>;
    var devnum = '<?=$_REQUEST['devnum']?>';
    $('#account-content-app-box').html(ln.load);
    var content = syncAjax('/ajax/account/app/app-content-'+sub+'.php?devid='+devid+'&devnum='+devnum);
    $('#account-content-app-box').html(content);
}*/
</script>