<?
include_once($_SERVER['DOCUMENT_ROOT'].'/core/config.php');
?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
<script src="/admin/js/lng/<?=SITE_LANG;?>.js"></script>
<h3 class="accountheader"><?=STATUSESOFASO;?></h3>

<?
if (empty($MyDB)) {
    $MyDB = new dbconnect;
}
$MyDB->Connect();
$MyDB->Text = "SELECT `id`, `device`, `name`, `serial`
FROM `userdevices`
WHERE `user`='".$_SESSION['USER_ID']."'
ORDER BY `name`";
$MyDB->Query();
$MyDB->Assoc();
?>
<div class="selectdevice">
<?if (empty($MyDB->Data)):?>
    <span class="no-data"><?=REGDEVICENOTFOUND;?></span>
<?else:?>
    <select id="deviceselect" class="form-control short-input">
    <?if(count($MyDB->Data)>1):?>
        <option value="NULL"><?=SELECTDEVICE;?></option>
    <?elseif(count($MyDB->Data)==1):?>
    <script>
        var dev = <?=$MyDB->Data[0]['device'];?>;
        $('#devicestatuscontent').html(ln.load);
        var content = syncAjax('/ajax/account/action/statuslist.php?device='+dev);
        $('#devicestatuscontent').html(content);
    </script>
    <?endif;?>
    <?foreach($MyDB->Data as $n => $device):?>
        <option value="<?=$device['device'];?>"><?=$device['name'];?></option>
    <?endforeach;?>
    </select>
<?endif;?>
    <div id="devicestatuscontent"></div>
</div>
<script>
$('#deviceselect').change(function(){
    $('#devicestatuscontent').html('');
    var dev = $(this).val();
    $('#devicestatuscontent').html(ln.load);
    var content = syncAjax('/ajax/account/action/statuslist.php?device='+dev);
    $('#devicestatuscontent').html(content);
});
</script>