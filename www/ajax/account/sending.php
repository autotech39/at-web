<?
include_once($_SERVER['DOCUMENT_ROOT'].'/core/config.php');
?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
<script src="/admin/js/jquery-ui/jquery-ui.min.js"></script>
<script src="/admin/js/lng/<?=SITE_LANG;?>.js"></script>
<link rel="stylesheet" type="text/css" href="/admin/js/jquery-ui/jquery-ui.min.css">

<h3 class="accountheader"><?=DISTRIBUTION_OPTIONS;?></h3>

<?
if (empty($MyDB)) {
    $MyDB = new dbconnect;
}
$MyDB->Connect();
$MyDB->Text = "SELECT `id`, `device`, `name`, `serial`
FROM `userdevices`
WHERE `user`='".$_SESSION['USER_ID']."'
ORDER BY `name`";
$MyDB->Query();
$MyDB->Assoc();
?>
<div class="selectdevice">
<?if (empty($MyDB->Data)):?>
    <span class="no-data"><?=REGDEVICENOTFOUND;?></span>
<?else:?>
    <select id="deviceselect" class="form-control short-input">
    <?if(count($MyDB->Data)>1):?>
        <option value="NULL"><?=SELECTDEVICE;?></option>
    <?elseif(count($MyDB->Data)==1):?>
    <script>
        var dev = <?=$MyDB->Data[0]['device'];?>;
        $('#devicestatuscontent').html(ln.load);
        $('#devicedistribcontent').html(ln.load);
        var content = syncAjax('/ajax/account/action/distribution_param.php?device='+dev);
        $('#devicedistribcontent').html(content);
    </script>
    <?endif;?>
    <?foreach($MyDB->Data as $n => $device):?>
        <option value="<?=$device['device'];?>"><?=$device['name'];?></option>
    <?endforeach;?>
    </select>
<?endif;?>
    <div id="devicedistribcontent"></div>
</div>
<script>
$('#deviceselect').change(function(){
    $('#devicedistribcontent').html('');
    var dev = $(this).val();
    $('#devicedistribcontent').html(ln.load);
    var content = syncAjax('/ajax/account/action/distribution_param.php?device='+dev);
    $('#devicedistribcontent').html(content);
});
</script>