<?php
include_once('core/start.php');
?>
<?php

$core->read(TEMPLATE_INDEX);

/* === This block writing content on Your site === */
$startNavi = '/';
$toTop = '';
$naviScript = '';
if(empty($_SESSION['USER_ID'])) {
    $mLogin = M_LOG_IN;
    $mLogLink = 'login';
    $mAccount = '';
}
else {
    $mLogin = M_LOG_OUT;
    $mLogLink = 'logout';
    $mAccount = '<li data-point="account"><a href="/account/">'.M_ACCOUNT.'</a></li>';
}
$mAdmin = '';
if(!empty($_SESSION['USER_GROUP'])&&(in_array(1,$_SESSION['USER_GROUP'])||in_array(2,$_SESSION['USER_GROUP']))) {
    $mAdmin = '<li data-point="admin"><a href="/admin/">'.M_ADMIN.'</a></li>';
}

/*******************************************/
/* ================Get page=============== */
/*$rUri = $_SERVER['REQUEST_URI'];
$uriArr = explode('/',$rUri);
$pageUri = $uriArr[0]==''?$uriArr[1]:$uriArr[0];
echo $pageUri;*/
/*******************************************/

if ($_SERVER['REQUEST_URI']=='/') {
    $startNavi = '';
    $toTop = "<a href=\"#myPage\" title=\"To Top\">
            <span class=\"glyphicon glyphicon-chevron-up\"></span>
        </a>";
        
    $naviScript = '<script scr="'.TEMPLATE_PATH.'/js/start.js></script>';
}
$myPage = '#myPage';
if ($_SERVER['REQUEST_URI']!='/') {
    $myPage = '/';
}
$getLng = '';
if(!empty($_REQUEST['lng'])) {
    $getLng = '?lng='.$_REQUEST['lng'];
}
$core->set_data('[ROOT_URI]',ROOT_URI);
//$core->set_data('[PAGE_HEAD]',$pageinfo->header());
$core->set_data('[TEMPLATE_PATH]',TEMPLATE_PATH);
$core->set_data('[SRC_TEMPLATE_PATH]',SRC_TEMPLATE_PATH);
$core->set_data('[START_NAVI]',$startNavi);
$core->set_data('[MY_PAGE]',$myPage);
$core->set_data('[TO_TOP]',$toTop);
$core->set_data('[NAVI_SCRIPT]',$naviScript);
//$core->set_data('[AUTHFORM]',$form);
$core->set_data('[CONTENT]',$page_output);
/* ----- Menu ----- */
$core->set_data('[M_LOG_IN]',$mLogin);
$core->set_data('[M_LOG_IN_LNK]',$mLogLink);
$core->set_data('[M_ABOUT]',M_ABOUT);
$core->set_data('[M_CONTACTS]',M_CONTACTS);
$core->set_data('[M_NEWS]',M_NEWS);
$core->set_data('[M_DEVS]',M_DEVS);
$core->set_data('[M_ACCOUNT]',$mAccount);
$core->set_data('[M_ADMIN]',$mAdmin);
$core->set_data('[SITE_LANG]',SITE_LANG);
$core->set_data('[FOLLOW_US]',FOLLOW_US);
$core->set_data('[GET_LNG]',$getLng);
/* ----- /Menu ----- */
/* =============================================== */

$core->write();
?>
<?php
//include_once('core/end.php');
?>