<?
/* ----- Menu ----- */
define('M_LOG_IN','Вход');
define('M_LOG_OUT','Выход');
define('M_ABOUT','О нас');
define('M_CONTACTS','Контакты');
define('M_NEWS','Новости');
define('M_SERVICES','Услуги');
define('M_ACCOUNT','Аккаунт');
define('M_ADMIN','Админка');
define('M_DEVS','Разработки');
/* ----- /Menu ----- */

/* ----- Headers ----- */
define('OUR_DEVS','Наши разработки');
define('OUR_NEWS','Наши новости');
define('LOG_IN_PA','Вход в личный кабинет');
/* ----- /Headers ----- */

/* ----- Error ----- */
define('ERROR_404','Ошибка 404');
define('ERROR_404_TEXT','Страница не найдена');
/* ----- /Error ----- */

define('GO_TO_LOGIN','Вернуться на страницу авторизации');
define('AUTH_ERROR','Ошибка авторизации');
define('SIGN_IN','Войти');
define('LOGIN','Пользователь');
define('PASSWORD','Пароль');
define('RE_PASSWORD','Еще раз пароль');
define('EMAIL','E-mail');
define('SUBMIT','Отправить');
define('SIGN_UP','Регистрация');
define('ERROR','Ошибка');
define('EMPTY_LOGIN','Не заполнено имя пользователя');
define('EMPTY_PASS','Не указан пароль');
define('PASS_NOT_MATCH','Пароли не совпадают');
define('EMPTY_EMAIL','Не указан e-mail');
define('SEC_CHECK_FAILED','Проверка безопасности не пройдена');
define('LOGINPASS_FAILED','Неверный логин и/или пароль');
define('LOGIN_IS_BUSY','Логин занят');
define('EMAIL_IS_BUSY','На указанный email уже зарегистрирован аккаунт');
define('REG_ERROR','Ошибка регистрации');
define('REGISTER_COMPLET','Регистрация пройдена');
define('REG_COMPLET_TEXT','Вам отправлено письмо на указанный e-mail. Для завершения регистрации перейдите по ссылке, указанной в сообщении');
define('ACCOUNT_NOT_FOUND','Учетная запись не найдена');
define('ACC_ALREADY_ACTIVE','Учетная запись уже активирована');
define('DB_QUERY_ERROR','Ошибка запроса к базе данных');
define('ACC_IS_ACTIVATE','Ваша учетная запись успешно активирована');
define('UNKNOWN_ERROR','Неизвестная ошибка');
define('REGISTRATION_FINAL','Регистрация завершена');
define('PASS_FORGET','Забыл пароль');
define('RESET_PASSWORD','Сбросить пароль');
define('PASSRESET_TEXT','Вам отправлено письмо на указанный e-mail. Для завершения операции перейдите по ссылке, указанной в сообщении');
define('ENTER_NEWPASS','Введите новый пароль');
define('PASSISCHANGED','Пароль успешно изменен');
define('PASSISNOTCHANGED','Пароль не изменен');
define('EMAIL_NOT_FOUND','E-mail не найден');
define('FOLLOW_US','Присоединяйтесь');
define('TO_BACK','Назад');
define('READ','Читать');
define('DETAIL','Подробно');
define('BUY','Купить');
define('TOOSHORTPATH','Слишком короткий пароль');
//define('','');

/*------- Account -------*/

define('CARALARMSYSTEM','Автомобильная система оповещений');
define('ASO','АСО');
define('MESSAGESOFASO','Сообщения АСО');
define('STATUSESOFASO','Состояния АСО');
define('SOFTWAREUPDATE','Обновление ПО');
define('DISTRIBUTION_OPTIONS','Параметры рассылки');
define('SETTINGS','Настройки');
define('DEVICES','Устройства');
define('DEVICE','Устройство');
define('DEVNAME','Название');
define('SN','SN');
define('SECRET_KEY','Секретный ключ');
define('SERIALNUMBER','Серийный номер');
define('DEVVERSION','Версия устройства');
define('AVVERSION','Доступная версия');
define('RENAMEDEVICE','Переименовать устройство');
define('REMOVEDEVICE','Удалить устройство');
define('REGISTERDEVICE','Зарегистрировать устройство');
define('RENAME','Переименовать');
define('CHANGEPASSWORD','Сменить пароль');
define('OLDPASS','Старый пароль');
define('NEWPASS','Новый пароль');
define('WRONGOLDPASS','Неверный старый пароль');
define('ERRORCHNGPASS','Ошибка сохранения пароля');
define('PASSISCHANGED','Пароль успешно изменен');
define('REGDEV','Зарегистрировать');
define('DEVICENOTFOUND','Устройство не найдено');
define('NEWDEVNAME','Новое название');
define('REGDEVICENOTFOUND','Нет зарегистрированных устройств');
define('USERDATANOTFOUND','Данные пользователя не найдены. Обновите страницу или авторизуйтесь заново.');
define('SELECTDEVICE','Выберите устройство');
/*define('','');
define('','');
define('','');*/
define('NOTDATAFORDEVICE','Нет данных по указанному устройству');
define('DEVSTATUSPARAM','Параметр');
define('DEVSTATUSVALUE','Ошибки');

define('MICCONNECTIONFAILURE','Некорректное подключение микрофона');
define('MICFAILURE','Неработоспособность микрофона');
define('RIGHTSPEAKERFAILURE','Неисправность правого динамика');
define('LEFTSPEAKERFAILURE','Неисправность левого динамика');
define('SPEAKERSFAILURE','Неисправность динамиков');
define('IGNITIONLINEFAILURE','Неисправность при определении состояния линии зажигания');
define('UIMFAILURE','Неисправность БИП');
define('STATUSLNDICATORFAILURE','Неисправность индикатора состояния');
define('BATTERYFAILURE','Неисправность резервной батареи');
define('BATTERYVOLTAGELOW','Разряд резервной батареи ниже допустимого уровня');
define('CRASHSENSORFAILURE','Неисправность датчика автоматической идентификации события ДТП');
define('FIRMWARELMAGECORRUPTION','Нарушение целостности образа программного обеспечения');
define('COMMMODULELNTERFACEFAILURE','Неработоспособность интерфейса коммуникационного модуля GSM и UMTS');
define('GNSSRECEIVERFAILURE','Неработоспособность приемника ГНСС');
define('RAIMPROBLEM','Отсутствие целостности (достоверности) определяемых приемником ГНСС навигационно-временных параметров (функция RAIM)');
define('GNSSANTENNAFAILURE','Неработоспособность (некорректное подключение) внешней антенны ГНСС');
define('COMMMODULEFAILURE','Неработоспособность (некорректное подключение) внешней антенны GSM и UMTS');
define('EVENTSMEMORYOVERFLOW','Переполнение внутренней памяти событий');
define('CRASHPROFILEMEMORYOVERFLOW','Переполнение памяти для записи профилей ускорения');
define('OTHERCRITICALFAILIRES','Другие критические ошибки');
define('OTHERNOTCRITICALFAILURES','Другие некритические ошибки');

/*............................*/

define('ERROR','Код ошибки');
define('DATETIME','Дата и время');
define('GPS','GPS-координаты');
define('DEVICE_CHARGE','Уровень заряда АКБ АСО');
define('CAR_CHARGE','Уровень заряда АКБ автомобиля');
define('CURRENT_TEMP','Текущая температура в салоне');
define('FIRE_TEMP','Температура в салоне, при которой будет зафиксировано возгорание');
define('TEMP_SPEED','Скорость повышения температуры');
define('CAR_SPEED','Скорость автомобиля');
define('ACCEL_DATA','Данные акселерометра');
define('GYRO_DATA','Данные гироскопа');
define('MAG_DATA','Данные магнетометра');
define('VERSION','Версия ПО');

define('VOLT','Вольт');
define('KMH','Км/ч');
define('MSSQ','м/с<sup>2</sup>');
define('RAD','рад.');
define('NTESLA','нТл');
define('ACCELERATION','Ускорение');
define('POSITION','Положение');
define('MAGNETOMETER','Магнетометр');
define('AXES','Ось');

/*............................*/

define('YES','Да');
define('NO','Нет');
define('DATARECIEVED','Данные получены');

define('DISTRIB_PHONE_NUMBERS','Номера телефонов');
define('DISTRIB_EMAILS','Адреса эллектронной почты');
define('PHONE_NUMBER','Номер телефона');
define('SAVE','Сохранить');
define('EDIT','Изменить');
define('REMOVE','Удалить');
define('CANCEL','Отменить');


define('SAVED','Сохранено');
define('DOWNWITHERRORS','Завершено с ошибками');

define('DD_MM_YYYY','дд.мм.гггг');
define('REFRESH_DATA','Обновить данные');

define('UPDATESW','Обновить');
define('UPDATEREQUESTED','Запрошено обновление');

define('REMOVE_FILE','Удалить файл');
define('CHOOSE_FILE_TO_UPLOAD','Выберите файл для загрузки');
define('OR_DRAG_TO_HERE','или перетащите его сюда');
define('UPLOAD_FILE','Загрузить файл');
define('UPLOAD_FILE_NAME','Наименование');
define('UPLOAD_FILE_VERSION','Версия');
define('THE_FILE','Файл');
define('FILE_NOT_FOUND','Файл не найден');
define('LIST_IS_EMPTY','Список пуст');
define('PERSONALAREA','Личный кабинет');
define('GOTO_PAREA','Перейти в личный кабинет');

/*.........................*/

define('ERROR_CODE','Код сообщения');
define('DATETIMEH','Дата/время');
define('GPSDATA','Геолокация');
define('MESSAGETEXT','Сообщение');
define('MESSAGE_TO_FILE','Выгрузить в ');

/*------ /Account -------*/

/* ----- Admin ----- */

define('A_GO_TO_SITE','Вернуться на сайт');
define('A_ARTICLES','Записи');
define('A_USERS','Пользователи');
define('A_SOFTWARE','ПО');
define('A_AVFORDNL','Доступно для скачивания');

/* ----- /Admin ----- */

?>