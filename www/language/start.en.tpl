<div class="jumbotron text-center">
        <!--<h1>AutoTech</h1>-->
        <img src="[TEMPLATE_PATH]/img/autotech.png" width="200" height="200" alt="AutoTech" style="margin-top:3px" />
        <!--
        <p>Исследования и разработки электронных систем и программного обеспечения</br>в области безопасности и коммуникации на транспорте</p>
        <p></p>
        --!>
        <form>
            <!--
            <div class="input-group">
                <input type="email" class="form-control" size="50" placeholder="Email Address" required>
                <div class="input-group-btn">
                    <button type="button" class="btn btn-danger">Подписаться</button>
                </div>
            </div>
            
        </form>-->
    </div>

    <!-- Container (About Section) -->
    <div id="about" class="container-fluid bg-grey">
        <div class="row">
            <div class="col-sm-8">
                <span class="glyphicon glyphicon-info-sign logo-small slideanim"></span>
                <h2>О НАС</h2><br>
                <h4>Разработка электронных устройств и промышленного программного обеспечения</h4><br>
                
                <p>
                    Компания АВТОТЕХ вошла в число победителей конкурса СТАРТ-16. «Старт» (Старт-1) проходил с 20 мая по 31 июля 2016 года. </br>
                    Итоги конкурса опубликованы на <a href="http://www.fasie.ru">сайте Фонда содействия инновациям</a>. Компания АВТОТЕХ представила на конкурс устройство для автомобилей <a href="/ans/">Автомобильная система оповещений</a>
</p>
                <!--<br><button class="btn btn-default btn-lg">Get in Touch</button>-->
            </div>
            <div class="col-sm-4">
                <!--<span class="glyphicon glyphicon-signal logo"></span> -->
                <a href="http://www.fasie.ru"><img src="[TEMPLATE_PATH]/img/fasie_en.png" width="300" height="100" alt="FASIE" style="margin-top:200px" /></a>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-4">
                <span class="glyphicon glyphicon-shopping-cart logo slideanim"></span>
            </div>
            <div class="col-sm-8">
                <h3>Наши продукты:</h3><br>
                <p>
                <strong><a href="/ans/"><strong>АТ АСО:</strong> Автомобильная система оповещений</strong></a><br>
                <strong>Описание:</strong> Контроль состояния автомобиля в реальном времени, с функционалом класса ЭРА-Глонасс. ГОСТ Р 54620-2011
                </p>
                
            </div>
        </div>
    </div>



    <!-- Container (Services Section) -->
    
    <div id="services" class="container-fluid text-center bg-grey">
        <h2>УСЛУГИ</h2>
        <h4>Что мы предлагаем</h4>
        <br>
        <div class="row slideanim">
            <div class="col-sm-4">
                <span class="glyphicon glyphicon-cog logo-small"></span>
                <h4>НИОКР</h4>
                <p>Исследования в области технических </br>и микроэлектронных устройств и компонентов</p>
            </div>
            <div class="col-sm-4">
                <span class="glyphicon glyphicon-blackboard logo-small"></span>
                <h4>РАЗРАБОТКА</h4>
                <p>Электронных устройств </br>и промышленного программного обеспечения</p>
            </div>
            <div class="col-sm-4">
                <span class="glyphicon glyphicon-lock logo-small"></span>
                <h4>ПОД КЛЮЧ</h4>
                <p>Полный цикл разработки</br>и сопутствующая документация</p>
            </div>
        </div>
        <br><br>
        <div class="row slideanim">
            <div class="col-sm-4">
                <span class="glyphicon glyphicon-leaf logo-small"></span>
                <h4>ЭНЕРГОСБЕРЕЖЕНИЕ</h4>
                <p>Разработка энергосберегающих систем</br>для частного и промышленного применения</p>
            </div>
            <div class="col-sm-4">
                <span class="glyphicon glyphicon-list logo-small"></span>
                <h4>ГОСТ</h4>
                <p>Все разработки соответствуют</br>ГОСТ Российской Федерации</p>
            </div>
            <div class="col-sm-4">
                <span class="glyphicon glyphicon-wrench logo-small"></span>
                <h4 style="color:#303030;">СЕРВИС</h4>
                <p>Сервисные центры</br>и поддержка пользователей</p>
            </div>
        </div>
    </div>
    


    <!-- Container (Contact Section) -->
    <div id="contact" class="container-fluid bg-grey">
        <span class="glyphicon glyphicon-phone-alt logo-small"></span>
        <h2 class="text-center">КОНТАКТЫ</h2>
        <div class="row">
            <div class="col-sm-5">
                <p>Напишите нам и мы ответим Вам в ближайшее время. Или звоните</p>
                <p><span class="glyphicon glyphicon-map-marker"></span> Калининград, Россия</p>
                <p><span class="glyphicon glyphicon-phone"></span> +7 9622 533888</p>
                <p><span class="glyphicon glyphicon-envelope"></span> mail@autotech39.com</p>
            </div>
            <div class="col-sm-7 slideanim">
                <!--
                <div class="row">
                    <div class="col-sm-6 form-group">
                        <input class="form-control" id="name" name="name" placeholder="Name" type="text" required>
                    </div>
                    <div class="col-sm-6 form-group">
                        <input class="form-control" id="email" name="email" placeholder="Email" type="email" required>
                    </div>
                </div>
                <textarea class="form-control" id="comments" name="comments" placeholder="Comment" rows="5"></textarea><br>
                <div class="row">
                    <div class="col-sm-12 form-group">
                       <!-- <button class="btn btn-default pull-right" type="submit">Send</button> -->
                
                    </div>
                </div>
            </div>
        </div>
    </div>



    <script>
        $(document).ready(function () {
            // Add smooth scrolling to all links in navbar + footer link
            $(".navbar a, footer a[href='#myPage']").on('click', function (event) {
                // Make sure this.hash has a value before overriding default behavior
                if (this.hash !== "") {
                    // Prevent default anchor click behavior
                    event.preventDefault();

                    // Store hash
                    var hash = this.hash;

                    // Using jQuery's animate() method to add smooth page scroll
                    // The optional number (900) specifies the number of milliseconds it takes to scroll to the specified area
                    $('html, body').animate({
                        scrollTop: $(hash).offset().top
                    }, 900, function () {

                        // Add hash (#) to URL when done scrolling (default click behavior)
                        window.location.hash = hash;
                    });
                } // End if
            });

            $(window).scroll(function () {
                $(".slideanim").each(function () {
                    var pos = $(this).offset().top;

                    var winTop = $(window).scrollTop();
                    if (pos < winTop + 600) {
                        $(this).addClass("slide");
                    }
                });
            });
        })
    </script>
