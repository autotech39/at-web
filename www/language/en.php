<?
/* ----- Menu ----- */
define('M_LOG_IN','Log in');
define('M_LOG_OUT','Log out');
define('M_ABOUT','About');
define('M_CONTACTS','Contacts');
define('M_NEWS','News');
define('M_SERVICES','Service');
define('M_ACCOUNT','Account');
define('M_ADMIN','AdminPanel');
define('M_DEVS','Developments');
/* ----- /Menu ----- */

/* ----- Headers ----- */
define('OUR_DEVS','Our developments');
define('OUR_NEWS','Our news');
define('LOG_IN_PA','Log in personal area');
/* ----- /Headers ----- */

/* ----- Error ----- */
define('ERROR_404','Error 404');
define('ERROR_404_TEXT','Page not found');
/* ----- /Error ----- */

define('GO_TO_LOGIN','Go back to authorization page');
define('AUTH_ERROR','Authentication error');
define('SIGN_IN','Sign in');
define('LOGIN','Login');
define('PASSWORD','Password');
define('RE_PASSWORD','Repeat password');
define('EMAIL','E-Mail');
define('SUBMIT','Send');
define('SIGN_UP','Sign up');
define('ERROR','Error');
define('EMPTY_LOGIN','Empty login');
define('EMPTY_PASS','Empty password');
define('PASS_NOT_MATCH','Passwords not match');
define('EMPTY_EMAIL','Empty e-mail');
define('SEC_CHECK_FAILED','Security check failed');
define('LOGINPASS_FAILED','Invalid login and/or password');
define('LOGIN_IS_BUSY','Login is busy');
define('EMAIL_IS_BUSY','Email is busy');
define('REG_ERROR','Registration error');
define('REGISTER_COMPLET','Registration passed');
define('REG_COMPLET_TEXT','You have been sent an email to the specified e-mail. To complete registration, click on the link specified in the message');
define('ACCOUNT_NOT_FOUND','Account not found');
define('ACC_ALREADY_ACTIVE','Your account has already been activated');
define('DB_QUERY_ERROR','Database query error');
define('ACC_IS_ACTIVATE','Your account is activated');
define('UNKNOWN_ERROR','Unknown error');
define('REGISTRATION_FINAL','Registration completed');
define('PASS_FORGET','Forgot your password?');
define('RESET_PASSWORD','Reset password');
define('PASSRESET_TEXT','You have been sent an email to the specified e-mail. To complete operation, click on the link specified in the message');
define('ENTER_NEWPASS','Enter a new password');
define('PASSISCHANGED','Password changed successfully');
define('PASSISNOTCHANGED','Password is not changed');
define('EMAIL_NOT_FOUND','E-mail not found');
define('FOLLOW_US','Follow us');
define('TO_BACK','Back');
define('READ','Read');
define('DETAIL','Detail');
define('BUY','Buy');
define('TOOSHORTPATH','Pasword is too short');
//define('','');

/*------- Account -------*/

define('CARALARMSYSTEM','Car Alarm System');
define('ASO','CAS');
define('MESSAGESOFASO','CAS Messages');
define('STATUSESOFASO','CAS Statuses');
define('SOFTWAREUPDATE','Software Update');
define('DISTRIBUTION_OPTIONS','Distribution options');
define('SETTINGS','Settings');
define('DEVICES','Devices');
define('DEVICE','Device');
define('DEVNAME','Name');
define('SN','SN');
define('SECRET_KEY','Secret key');
define('SERIALNUMBER','Serial number');
define('DEVVERSION','Device version');
define('AVVERSION','Available version');
define('RENAMEDEVICE','Rename device');
define('REMOVEDEVICE','Remove device');
define('REGISTERDEVICE','Register device');
define('RENAME','Rename');
define('CHANGEPASSWORD','Change password');
define('OLDPASS','Old password');
define('NEWPASS','New password');
define('WRONGOLDPASS','Wrong old password');
define('ERRORCHNGPASS','Error of change password, DB error');
define('PASSISCHANGED','Password is changed');
define('REGDEV','Register');
define('DEVICENOTFOUND','Device not found');
define('NEWDEVNAME','New name');
define('REGDEVICENOTFOUND','Registered device not found');
define('USERDATANOTFOUND','User data not found. Please refresh this page or log in again.');
define('SELECTDEVICE','Select device');
/*define('','');
define('','');
define('','');*/
define('NOTDATAFORDEVICE','Data for this device not found');
define('DEVSTATUSPARAM','Parametr');
define('DEVSTATUSVALUE','Value');

define('MICCONNECTIONFAILURE','Mic Connection Failure');
define('MICFAILURE','Mic Failure');
define('RIGHTSPEAKERFAILURE','Right Speaker Failure');
define('LEFTSPEAKERFAILURE','Left Speaker Failure');
define('SPEAKERSFAILURE','Speakers Failure');
define('IGNITIONLINEFAILURE','Ignition Line Failure');
define('UIMFAILURE','Uim Failure');
define('STATUSLNDICATORFAILURE','Status Indicator Failure');
define('BATTERYFAILURE','Battery Failure');
define('BATTERYVOLTAGELOW','Battery Voltage Low');
define('CRASHSENSORFAILURE','Crash Sensor Failure');
define('FIRMWARELMAGECORRUPTION','Firmware Image Corruption');
define('COMMMODULELNTERFACEFAILURE','Comm. Module Interface Failure');
define('GNSSRECEIVERFAILURE','Gnss Receiver Failure');
define('RAIMPROBLEM','Raim Problem');
define('GNSSANTENNAFAILURE','Gnss Antenna Failure');
define('COMMMODULEFAILURE','Comm. Module Failure');
define('EVENTSMEMORYOVERFLOW','Events Memory Overflow');
define('CRASHPROFILEMEMORYOVERFLOW','Crash Profile Memory Overflow');
define('OTHERCRITICALFAILIRES','Other Critical Failires');
define('OTHERNOTCRITICALFAILURES','Other Not Critical Failures');

/*............................*/

define('ERROR','Код ошибки');
define('DATETIME','Дата и время');
define('GPS','GPS-координаты');
define('DEVICE_CHARGE','Уровень заряда АКБ АСО');
define('CAR_CHARGE','Уровень заряда АКБ автомобиля');
define('CURRENT_TEMP','Текущая температура в салоне');
define('FIRE_TEMP','Температура в салоне, при которой будет зафиксировано возгорание');
define('TEMP_SPEED','Скорость повышения температуры');
define('CAR_SPEED','Скорость автомобиля');
define('ACCEL_DATA','Данные акселерометра');
define('GYRO_DATA','Данные гироскопа');
define('MAG_DATA','Данные магнетометра');
define('VERSION','Версия ПО');

define('VOLT','Volt');
define('KMH','Km/H');
define('MSSQ','m/sec<sup>2</sup>');
define('RAD','rad.');
define('NTESLA','nT');
define('ACCELERATION','Acceleration');
define('POSITION','Position');
define('MAGNETOMETER','Magnetometer');
define('AXES','Axes');

/*............................*/

define('YES','Yes');
define('NO','No');
define('DATARECIEVED','Data received');

define('DISTRIB_PHONE_NUMBERS','Phone numbers');
define('DISTRIB_EMAILS','E-mails');
define('PHONE_NUMBER','Phone number');
define('SAVE','Save');
define('EDIT','Edit');

define('SAVED','Saved');
define('DOWNWITHERRORS','Down with errors');

define('DD_MM_YYYY','dd.mm.yyyy');
define('REFRESH_DATA','Refresh data');

define('UPDATESW','Update');
define('UPDATEREQUESTED','Update requested');

define('REMOVE_FILE','Remove file');
define('CHOOSE_FILE_TO_UPLOAD','Select file to upload');
define('OR_DRAG_TO_HERE','or drag it here');
define('UPLOAD_FILE','Upload file');
define('UPLOAD_FILE_NAME','Name');
define('UPLOAD_FILE_VERSION','Version');
define('THE_FILE','File');
define('FILE_NOT_FOUND','File not found');
define('LIST_IS_EMPTY','List is empty');
define('PERSONALAREA','Personal area');
define('GOTO_PAREA','Go to personal area');

/*.........................*/

define('ERROR_CODE','Error code');
define('DATETIMEH','Date/Time');
define('GPSDATA','Location');
define('MESSAGETEXT','Message');
define('MESSAGE_TO_FILE','Download as ');

/*------ /Account -------*/

/* ----- Admin ----- */

define('A_GO_TO_SITE','Go to site');
define('A_ARTICLES','Articles');
define('A_USERS','Users');
define('A_SOFTWARE','Software');
define('A_AVFORDNL','Available for download');

/* ----- /Admin ----- */

?>