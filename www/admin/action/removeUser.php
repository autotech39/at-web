<?
session_start();
include_once($_SERVER['DOCUMENT_ROOT'].'/core/config.php');
//print_r($_REQUEST);
$logName = 'users';
$logPath = 'admin';
?>
<?if (empty($_SESSION['USER_ID']) || empty($_SESSION['USER_GROUP']) || !in_array(1,$_SESSION['USER_GROUP'])):?>
{
    "answer": 0,
    "error": "9",
    "description": "Access denied"
} 
<?else:?>
    <?if(empty($_REQUEST['user'])):?>
    {
        "answer": 0,
        "error": "10",
        "description": "Invalid parameter"
    }  
    <?else:?>
    <?
    if (empty($MyDB)) {
        $MyDB = new dbconnect;
    }
    $MyDB->Connect();
    ?>
        <?
        $errTxt= '';
        $MyDB->Text = "BEGIN;";
        $MyDB->Query();
        $errTxt .= ' || '.$MyDB->ErrorText();
        $MyDB->Text = "DELETE FROM `devicemessage` WHERE `user`='".$_REQUEST['user']."';";
        $MyDB->Query();
        $errTxt .= ' || '.$MyDB->ErrorText();
        $MyDB->Text = "DELETE FROM `device_state` WHERE `user`='".$_REQUEST['user']."';";
        $MyDB->Query();
        $errTxt .= ' || '.$MyDB->ErrorText();
        $MyDB->Text = "DELETE FROM `userdevices` WHERE `user`='".$_REQUEST['user']."';";
        $MyDB->Query();
        $errTxt .= ' || '.$MyDB->ErrorText();
        $MyDB->Text = "DELETE FROM `sys_user` WHERE `id`='".$_REQUEST['user']."';";
        $MyDB->Query();
        $errTxt .= ' || '.$MyDB->ErrorText();
        $MyDB->Text = "COMMIT;";
        $MyDB->Query();
        $errTxt .= ' || '.$MyDB->ErrorText();
        $MyDB->ErrorNum();
        $DBErr = $MyDB->Error;
        ?>
        <?if (empty($MyDB->Error)):?>
            <?
            $event = 'User is removed. ID: '.$_REQUEST['user'].'. User ID: '.$_SESSION['USER_ID'];
            writeLog ($logName,$event,$logPath,true);
            ?>
            {
                "answer": 1,
                "user": "<?=$_REQUEST['user'];?>",
                "description": "User is removed"
            }
        <?else:?>
        <?
        $event = 'User is NOT removed. DB error: '.$DBErr.'. User ID: '.$_SESSION['USER_ID'];
        writeLog ($logName,$event,$logPath,true);
        ?>
        {
            "answer": 0,
            "error": "6",
            "description": "Database query error."
        } 
        <?endif;?>
    <?endif;?>
<?endif;?>