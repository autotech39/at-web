<?
session_start();
include_once($_SERVER['DOCUMENT_ROOT'].'/core/config.php');
?>
<?if (empty($_SESSION['USER_ID']) || empty($_SESSION['USER_GROUP']) || !in_array(1,$_SESSION['USER_GROUP'])):?>

<?else:?>
<?
if (empty($MyDB)) {
    $MyDB = new dbconnect;
}
$MyDB->Connect();
$MyDB->Text = "SELECT `id`, `name`
FROM `sys_user_group`
WHERE 1";
$MyDB->Query();
$MyDB->Assoc();
$groupArr = $MyDB->Data;

$MyDB->Text = "SELECT su.`id`, 
    su.`name` AS \"login\",
    `email`, 
    `status`, 
    `datetime`, 
    ln.`code` AS \"language\",
    ug.`id` AS \"group\",
    ug.`name` AS \"groupname\"
FROM `sys_user` su
LEFT JOIN `language` ln
ON su.`language` = ln.`id`
LEFT JOIN `sys_user_group_join` ugj
ON su.`id` = ugj.`user`
LEFT JOIN `sys_user_group` ug
ON ugj.`group` = ug.`id`";
$MyDB->Query();
$MyDB->Assoc();
$usersArr = $MyDB->Data;
?>
<table class="table">
    <thead>
        <tr>
            <th>ID</th>
            <th>Логин</th>
            <th>Email</th>
            <th>Группа</th>
            <th></th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>
                *
            </td>
            <td>
                <input type="text" class="form-control short-input" name="newlogin" id="newlogin" />
            </td>
            <td>
                <input type="email" class="form-control short-input" name="neweamil" id="neweamil" />
            </td>
            <td>
                <select id="deviceselect" class="form-control short-input">
        <?foreach($groupArr as $gk => $group):?>
                    <option value="<?=$group['id'];?>"<?=$group['id']==3?' selected':'';?>><?=$group['name'];?></option>
        <?endforeach;?>
                </select>
            </td>
            <td colspan="2">
                <input type="button" class="btn btn-primary wide-button" onclick="addNewUser();" value="Добавить" />
            </td>
        </tr>
<?if(empty($usersArr)):?>
        <tr>
            <td colspan="6" class="text-center">
                Нет данных о пользователях
            </td>
        </tr>
<?else:?>
    <?foreach($usersArr as $uk => $userArr):?>
        <?
        if($userArr['id']==$_SESSION['USER_ID']) {
            continue;
        }
        ?>
        <tr class="string-lighting">
            <td>
                <?=$userArr['id'];?>
            </td>
            <td>
                <?=$userArr['login'];?>
            </td>
            <td>
                <?=$userArr['email'];?>
            </td>
            <td>
                <select id="deviceselect_<?=$userArr['id'];?>" class="form-control short-input" onchange="changeGroup(<?=$userArr['id'];?>)">
        <?foreach($groupArr as $gk => $group):?>
                    <option value="<?=$group['id'];?>"<?=$group['id']==$userArr['group']?' selected':'';?>><?=$group['name'];?></option>
        <?endforeach;?>
                </select>
            </td>
            <td>
                <input type="button" class="btn <?=$userArr['status']==1?'btn-default':'btn-primary';?> wide-button-fix150" id="blockuser_<?=$userArr['id'];?>" onclick="blockUser(<?=$userArr['id'];?>,)" value="<?=$userArr['status']==1?'За':'Раз';?>блокировать" />
            </td>
            <td>
                <input type="button" class="btn btn-primary wide-button-fix150" id="deleteuser_<?=$userArr['id'];?>" onclick="deleteUser(<?=$userArr['id'];?>)" value="Удалить" />
            </td>
        </tr>
    <?endforeach;?>
<?endif;?>
    </tbody>
</table>
<div id="loadfront">
    Загрука...
</div>
<script>
function changeGroup(user) {
    if (confirm('Вы действительно хотите изменить группу для этого пользователя?')) {
        var group = $('#deviceselect_'+user).val();
        var jsonResult = sAjax('/admin/action/changeGroup.php?user='+user+'&group='+group);
        var result = JSON.parse(jsonResult);
        if (result.answer==1) {
            $('#loadfront').show();
            var updContent = sAjax('/admin/action/userList.php');
            $('#usercontent').html(updContent);
            $('#loadfront').hide();
        }
        else {
            alert(result.description);
        }
    }
    else {
        return false;
    }
}
function addNewUser() {
    var login = $('#newlogin').val();
    var email = $('#neweamil').val();
    var group = $('#deviceselect').val();
    var jsonResult = sAjax('/admin/action/createUser.php?login='+login+'&email='+email+'&group='+group);
    var result = JSON.parse(jsonResult);
    if (result.answer==1) {
        $('#loadfront').show();
        var updContent = sAjax('/admin/action/userList.php');
        $('#usercontent').html(updContent);
        $('#loadfront').hide();
        alert('Пароль для нового пользователя: '+result.password);
    }
    else {
        alert(result.description);
    }
}
function blockUser(user,status) {
    var text = '';
    if (status==1) {
        text = 'разблокировать';
    }
    else {
        text = 'заблокировать';
    }
    if (confirm('Вы действительно хотите '+text+' этого пользователя?')) {
        var jsonResult = sAjax('/admin/action/changeUserStatus.php?user='+user+'&status='+status);
        var result = JSON.parse(jsonResult);
        if (result.answer==1) {
            $('#loadfront').show();
            var updContent = sAjax('/admin/action/userList.php');
            $('#usercontent').html(updContent);
            $('#loadfront').hide();
        }
        else {
            alert(result.description);
        }
    }
}
function deleteUser(user) {
    if (confirm('Вы действительно хотите удалить этого пользователя? Будут удалены все данные зарегистрированных пользователем устройств. Данное действие необратимо!')) {
        var jsonResult = sAjax('/admin/action/removeUser.php?user='+user);
        var result = JSON.parse(jsonResult);
        if (result.answer==1) {
            $('#loadfront').show();
            var updContent = sAjax('/admin/action/userList.php');
            $('#usercontent').html(updContent);
            $('#loadfront').hide();
        }
        else {
            alert(result.description);
        }
    }
}
</script>

<?endif;?>
