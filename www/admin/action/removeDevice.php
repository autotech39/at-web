<?
session_start();
include_once($_SERVER['DOCUMENT_ROOT'].'/core/config.php');
$logName = 'devices';
$logPath = 'admin';
?>
<?if (empty($_SESSION['USER_ID']) || empty($_SESSION['USER_GROUP']) || (!in_array(1,$_SESSION['USER_GROUP']) && !in_array(2,$_SESSION['USER_GROUP']))):?>
<?
$error = 1;
?>
<?else:?>
    <?
    $error = 2;
    if (empty($MyDB)) {
        $MyDB = new dbconnect;
    }
    $MyDB->Connect();
    ?>
    <?if(!empty($_REQUEST['device'])):?>
        <?
        $MyDB->Text = "DELETE FROM `devices`
        WHERE `id`='".$_REQUEST['device']."'";
        $MyDB->Query();
        $MyDB->ErrorNum();
        if (empty($MyDB->Error)) {
            $event = 'Device is removed. ID: '.$_REQUEST['device'].'. User ID: '.$_SESSION['USER_ID'];
            writeLog ($logName,$event,$logPath,true);
            $error = 0;
        }
        else {
            $event = 'Device is NOT removed. DB error: '.$MyDB->Error.'. ID: '.$_REQUEST['device'].'. User ID: '.$_SESSION['USER_ID'];
            writeLog ($logName,$event,$logPath,true);
            $error = 3;
        }
        ?>
    <?endif;?>
<?endif;?>
<?=$error;?>