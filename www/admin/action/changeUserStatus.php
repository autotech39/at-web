<?
session_start();
include_once($_SERVER['DOCUMENT_ROOT'].'/core/config.php');
//print_r($_REQUEST);
$logName = 'users';
$logPath = 'admin';
?>
<?if (empty($_SESSION['USER_ID']) || empty($_SESSION['USER_GROUP']) || !in_array(1,$_SESSION['USER_GROUP'])):?>
{
    "answer": 0,
    "error": "9",
    "description": "Access denied"
} 
<?else:?>
    <?if(empty($_REQUEST['user'])):?>
    {
        "answer": 0,
        "error": "10",
        "description": "Invalid parameter"
    }  
    <?else:?>
    <?
    if (empty($MyDB)) {
        $MyDB = new dbconnect;
    }
    $MyDB->Connect();
    ?>
        <?if(!isset($_REQUEST['status'])||$_REQUEST['status']!=1||$_REQUEST['status']!=0):?>
        <?
        //UPDATE `sys_user` SET `status`=(IF(`status`=0,1,0)) WHERE `id`='18'
        $MyDB->Text = "UPDATE `sys_user` SET `status`=(IF(`status`=0,1,0)) WHERE `id`='".$_REQUEST['user']."'";
        ?>
        <?else:?>
        <?
        $MyDB->Text = "UPDATE `sys_user` SET `status`='".$_REQUEST['status']."' WHERE `id`='".$_REQUEST['user']."'";
        ?>
        <?endif;?>
        <?
        $MyDB->Query();
        $MyDB->ErrorNum();
        $DBErr = $MyDB->Error;
        ?>
        <?if (empty($MyDB->Error)):?>
        <?
        $MyDB->Text = "SELECT `id`, `status` FROM `sys_user` WHERE `id`='".$_REQUEST['user']."'";
        $MyDB->Query();
        $MyDB->Assoc();
        ?>
            <?if(empty($MyDB->Data)):?>
            <?
            $event = 'Status is NOT changed. Data not found. ID: '.$_REQUEST['user'].'. User ID: '.$_SESSION['USER_ID'];
            writeLog ($logName,$event,$logPath,true);
            ?>
            {
                "answer": 0,
                "error": "11",
                "description": "Data not found"
            } 
            <?else:?>
            <?
            $event = 'Status is changed. ID: '.$MyDB->Data[0]['id'].', status: '.$MyDB->Data[0]['status'].'. User ID: '.$_SESSION['USER_ID'];
            writeLog ($logName,$event,$logPath,true);
            ?>
            {
                "answer": 1,
                "user": "<?=$MyDB->Data[0]['id'];?>",
                "status": "<?=$MyDB->Data[0]['status'];?>",
                "description": "Status is changed"
            }
            <?endif;?>
        <?else:?>
        <?
        $event = 'Status is NOT changed. DB error: '.$DBErr.'. User ID: '.$_SESSION['USER_ID'];
        writeLog ($logName,$event,$logPath,true);
        ?>
        {
            "answer": 0,
            "error": "6",
            "description": "Database query error"
        } 
        <?endif;?>
    <?endif;?>
<?endif;?>