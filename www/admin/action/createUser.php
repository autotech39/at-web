<?
session_start();
include_once($_SERVER['DOCUMENT_ROOT'].'/core/config.php');
/*include_once($_SERVER['DOCUMENT_ROOT'].'/api/classes/sign.php');
$reg = new RegAuth;
echo $reg->cryptSymbols.' || '.$reg->requestCode;*/
$symb = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz!@#$%&*?_';
$sln = strlen($symb)-1;
function getRandSybol($symb) {
    $sln = strlen($symb)-1;
    return substr($symb, mt_rand(0,$sln),1);
}
$cryptSymbols = substr($symb, mt_rand(0,$sln),1).substr($symb, mt_rand(0,$sln),1);
$requestCode = dechex(time()*mt_rand(1000000001,9999999999));
$requestCode .= crypt($requestCode,$cryptSymbols);
$pass = getRandSybol($symb).getRandSybol($symb).getRandSybol($symb).getRandSybol($symb).getRandSybol($symb).getRandSybol($symb);
//echo $pass;
$cryptPass = crypt($pass,$cryptSymbols);

//echo $cryptSymbols.' || '.$requestCode;
//print_r($_REQUEST);
$logName = 'users';
$logPath = 'admin';
?>
<?if (empty($_SESSION['USER_ID']) || empty($_SESSION['USER_GROUP']) || !in_array(1,$_SESSION['USER_GROUP'])):?>
{
    "answer": 0,
    "error": "9",
    "description": "Access denied"
} 
<?else:?>
    <?if(empty($_REQUEST['login'])||empty($_REQUEST['email'])||empty($_REQUEST['group'])):?>
    {
        "answer": 0,
        "error": "10",
        "description": "Invalid parameter"
    }  
    <?else:?>
    <?
    if (empty($MyDB)) {
        $MyDB = new dbconnect;
    }
    $MyDB->Connect();
    ?>
        <?
        $errTxt= '';
        $MyDB->Text = "BEGIN;";
        $MyDB->Query();
        $errTxt .= ' || '.$MyDB->ErrorText();
        $MyDB->Text = "INSERT INTO `sys_user`(`name`, `password`, `email`, `request_code`, `status`) VALUES ('".$_REQUEST['login']."','".$cryptPass."','".$_REQUEST['email']."','".$requestCode."','1')";
        $MyDB->Query();
        $errTxt .= ' || '.$MyDB->ErrorText();
        $MyDB->Text = "INSERT INTO `sys_user_group_join`(`user`, `group`) VALUES ((SELECT `id` FROM `sys_user` WHERE `name`='".$_REQUEST['login']."' AND `email`='".$_REQUEST['email']."'),'".$_REQUEST['group']."')";
        $MyDB->Query();
        $errTxt .= ' || '.$MyDB->ErrorText();
        /*$MyDB->Text = "DELETE FROM `device_state` WHERE `user`='".$_REQUEST['user']."';";
        $MyDB->Query();
        $errTxt .= ' || '.$MyDB->ErrorText();
        $MyDB->Text = "DELETE FROM `userdevices` WHERE `user`='".$_REQUEST['user']."';";
        $MyDB->Query();
        $errTxt .= ' || '.$MyDB->ErrorText();
        $MyDB->Text = "DELETE FROM `sys_user` WHERE `id`='".$_REQUEST['user']."';";
        $MyDB->Query();
        $errTxt .= ' || '.$MyDB->ErrorText();*/
        $MyDB->Text = "COMMIT;";
        $MyDB->Query();
        $errTxt .= ' || '.$MyDB->ErrorText();
        $MyDB->ErrorNum();
        $DBErr = $MyDB->Error;
        ?>
        <?if (empty($MyDB->Error)):?>
            <?
            $MyDB->Text = "SELECT `id` FROM `sys_user` WHERE `name`='".$_REQUEST['login']."' AND `email`='".$_REQUEST['email']."'";
            $MyDB->Query();
            $MyDB->Assoc();
            $userId = $MyDB->Data[0]['id'];
            $event = 'User is created. ID: '.$userId.'. User ID: '.$_SESSION['USER_ID'];
            writeLog ($logName,$event,$logPath,true);
            ?>
            {
                "answer": 1,
                "user": "<?=$userId;?>",
                "password": "<?=$pass;?>",
                "description": "User is created"
            }
        <?else:?>
        <?
        $event = 'User is NOT created. DB error: '.$DBErr.'. User ID: '.$_SESSION['USER_ID'];
        writeLog ($logName,$event,$logPath,true);
        ?>
        {
            "answer": 0,
            "error": "6",
            "description": "Database query error."
        } 
        <?endif;?>
    <?endif;?>
<?endif;?>