<?
ini_set('session.gc_maxlifetime', 2592000);
ini_set('session.cookie_lifetime', 2592000);
session_start();
//print_r($_SESSION);
include_once('../core/config.php');
include_once('../api/classes/sign.php');
if ($_GET['action']=='signin') {
    include('signin.php');
    exit;
}
if ($_GET['action']=='signout') {
    include('signout.php');
    exit;
}
if (empty($Sign)) {
    $Sign = new RegAuth;
}
if($Sign->CheckUser()&&(!$Sign->CheckGroup(1)&&!$Sign->CheckGroup(2))) {
    header('Location: http://'.SITE_URL);
}
?>
<?include('head.php');?>
<?if($Sign->CheckUser()):?>
    <?if($Sign->CheckGroup(1)||$Sign->CheckGroup(2)):?>
        <?include('header.php');?>
        <?include('content.php');?>
        <?include('footer.php');?>
    <?endif;?>
<?else:?>
    <?include('login.php')?>
<?endif;?>
<?include('foot.php');?>