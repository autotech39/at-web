<main id="main">
<?
if (empty($_GET['action'])) {
    if(file_exists('page/start.php')) {
        include ('page/start.php');
    }
}
else {
    if (file_exists('page/'.$_GET['action'].'.php')) {
        include ('page/'.$_GET['action'].'.php');
    }
    else {
        include ('error/404.php');
    }
}
?>
</main>