<?
session_start();
include_once($_SERVER['DOCUMENT_ROOT'].'/core/config.php');
?>
<?if (empty($_SESSION['USER_ID']) || empty($_SESSION['USER_GROUP']) || (!in_array(1,$_SESSION['USER_GROUP']) && !in_array(2,$_SESSION['USER_GROUP']))):?>

<?else:?>

<h2><?=DEVICES;?></h2>
<?
if (empty($MyDB)) {
    $MyDB = new dbconnect;
}
$MyDB->Connect();
?>
<?if(!empty($_REQUEST['serial'])&&!empty($_REQUEST['skey'])):?>
<?
$logName = 'devices';
$logPath = 'admin';
$MyDB->Text = "SELECT `id`, `serial`, `secretkey`
FROM `devices` 
WHERE `serial`='".$_REQUEST['serial']."'";
$MyDB->Query();
if(!empty($MyDB->Assoc())) {
    $event = 'Device is NOT added. SN '.$_REQUEST['serial'].' is already exist. User ID: '.$_SESSION['USER_ID'];
    writeLog ($logName,$event,$logPath,true);
    echo "Ошибка! Дублирование серийного номера!";
}
else {
    if($_REQUEST['device']==0) {
        $MyDB->Text = "INSERT INTO `devices`
        (`serial`, `secretkey`)
        VALUES ('".$_REQUEST['serial']."','".$_REQUEST['skey']."')";
        $MyDB->Query();
        $MyDB->ErrorNum();
        if (empty($MyDB->Error)) {
            $event = 'Device is added. SN: '.$_REQUEST['serial'].'. User ID: '.$_SESSION['USER_ID'];
            writeLog ($logName,$event,$logPath,true);
            echo "Устройство добавлено";
        }
        else {
            $event = 'Device is NOT added. DB error: '.$MyDB->Error.'. SN: '.$_REQUEST['serial'].'. User ID: '.$_SESSION['USER_ID'];
            writeLog ($logName,$event,$logPath,true);
            echo "Ошибка!";
        }
    }
}

?>
<?endif;?>
<?
$MyDB->Text = "SELECT `id`, `serial`, `secretkey`
FROM `devices` 
WHERE 1";
$MyDB->Query();
$MyDB->Assoc();
$deviceArr = $MyDB->Data;
?>
<div class="devicecontent">
    <div style="height: 30px;">
        <div id="deviceeditresult" class="red-text"></div>
    </div>
<table class="table">
    <thead>
        <tr>
            <th>
                #
            </th>
            <th>
                <?=SERIALNUMBER;?>
            </th>
            <th>
                <?=SECRET_KEY;?>
            </th>
            <th>
                
            </th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>
                *
                <input form="newdevform" type="hidden" class="form-control tableinput" name="device" id="device_0" value="0" />
            </td>
            <td>
                <input form="newdevform" type="text" class="form-control tableinput" name="serial" id="serial_0" />
            </td>
            <td>
                <input form="newdevform" type="text" class="form-control tableinput" name="skey" id="skey_0" />
            </td>
            <td>
                <input form="newdevform" type="submit" class="btn btn-primary tablebutton" name="newdevice" id="newdevice" value="<?=SAVE;?>"
            </td>
        </tr>
    <?if(empty($deviceArr)):?>
        <tr>
            <td colspan="4">
                <?=LIST_IS_EMPTY;?>
            </td>
        </tr>
    <?else:?>
        <?foreach($deviceArr as $dk => $device):?>
        <tr id="devicestring_<?=$device['id'];?>">
            <td>
                 <input type="text" class="form-control tableinput" style="width: 50px !important;" name="device_<?=$device['id'];?>" id="device_<?=$device['id'];?>" value="<?=$device['id'];?>" readonly="readonly" />
            </td>
            <td>
                <input type="text" class="form-control tableinput" name="serial_<?=$device['id'];?>" id="serial_<?=$device['id'];?>" value="<?=$device['serial'];?>" readonly="readonly" />
            </td>
            <td>
                <input type="text" class="form-control tableinput" name="skey_<?=$device['id'];?>" id="skey_<?=$device['id'];?>" value="<?=$device['secretkey'];?>" readonly="readonly" />
            </td>
            <td style="width: 200px;">
                <input type="button" class="btn btn-default tablebutton" name="editdevice_<?=$device['id'];?>" id="editdevice_<?=$device['id'];?>" value="<?=EDIT;?>" onclick="setForChange(this,<?=$device['id'];?>);" />
                <div class="hiddenblock" id="buttons_<?=$device['id'];?>">
                    <button class="btn btn-primary glyphicon glyphicon-floppy-save" name="changedevice_<?=$device['id'];?>" id="changedevice_<?=$device['id'];?>" title="<?=SAVE;?>" onclick="saveDeviceEdit(<?=$device['id'];?>);"></button>
                    <button class="btn btn-primary glyphicon glyphicon-remove" name="removedevice_<?=$device['id'];?>" id="removedevice_<?=$device['id'];?>" title="<?=REMOVE;?>" onclick="removeDevice(<?=$device['id'];?>);"></button>
                    <button class="btn btn-default glyphicon glyphicon-share-alt isreflex" name="canceldevice_<?=$device['id'];?>" id="canceldevice_<?=$device['id'];?>" onclick="notForChange(<?=$device['id'];?>);" title="<?=CANCEL;?>"></button>
                </div>
            </td>
        </tr>
        <?endforeach;?>
    <?endif;?>
    </tbody>
</table>
<form name="newdevform" id="newdevform" action="" method="post" onsubmit="return checkForm('0');"></form>
</div>
<script>
function checkForm(n) {
    var sn = $('#serial_'+n).val();
    var sk = $('#skey_'+n).val();
    if (isEmpty(sn)||isEmpty(sk)) {
        if (isEmpty(sn)) {
            $('#serial_'+n).addClass('red-light');
        }
        if (isEmpty(sk)) {
            $('#skey_'+n).addClass('red-light');
        }
        return false;
    }
    else {
        return true;
    }
}
function setForChange(th,dev) {
    if (!isEmpty(dev)) {
        $('#buttons_'+dev).show();
        $('#serial_'+dev).prop('readonly',false);
        $('#skey_'+dev).prop('readonly',false);
        localStorage.setItem('sn_'+dev, $('#serial_'+dev).val());
        localStorage.setItem('sk_'+dev, $('#skey_'+dev).val());
        //$('#editdevice_'+dev).css('display','none !important');
        $(th).hide();
    }
}
function notForChange(dev) {
    if (!isEmpty(dev)) {
        $('#buttons_'+dev).hide();
        $('#editdevice_'+dev).show();
        $('#serial_'+dev).attr('readonly','readonly');
        $('#skey_'+dev).attr('readonly','readonly');
        $('#serial_'+dev).val(localStorage.getItem('sn_'+dev));
        $('#skey_'+dev).val(localStorage.getItem('sk_'+dev));
        localStorage.removeItem('sn_'+dev);
        localStorage.removeItem('sk_'+dev);
    }
}
function saveDeviceEdit(dev) {
    var sn = $('#serial_'+dev).val();
    var sk = $('#skey_'+dev).val();
    var result = sAjax('/admin/action/changeDevice.php?device='+dev+'&serial='+sn+'&skey='+sk);
    result = Number(result);
    if (result>0) {
        $('#deviceeditresult').html('');
        $('#deviceeditresult').html(ln.error[16]);
        $('#deviceeditresult').show();
        $('#deviceeditresult').fadeOut(4000);
    }
    else {
        $('#deviceeditresult').html('');
        $('#deviceeditresult').html(ln.success[16]);
        $('#deviceeditresult').show();
        $('#deviceeditresult').fadeOut(4000);
        $('#buttons_'+dev).hide();
        $('#editdevice_'+dev).show();
        $('#serial_'+dev).attr('readonly','readonly');
        $('#skey_'+dev).attr('readonly','readonly');
    }
}
function removeDevice(dev) {
    var sn = $('#serial_'+dev).val();
    if (confirm(ln.suredevremove+' '+sn+'?')) {
        var result = sAjax('/admin/action/removeDevice.php?device='+dev);
        result = Number(result);
        if(result>0) {
            $('#deviceeditresult').html('');
            $('#deviceeditresult').html(ln.error[17]);
            $('#deviceeditresult').show();
            $('#deviceeditresult').fadeOut(4000);
        }
        else {
            $('#deviceeditresult').html('');
            $('#deviceeditresult').html(ln.success[17]);
            $('#deviceeditresult').show();
            $('#deviceeditresult').fadeOut(4000);
            $('#devicestring_'+dev).remove();
        }
    }
}
$('input[type="text"]').on('input',function(){
    $(this).removeClass('red-light');
});
</script>
<?endif;?>