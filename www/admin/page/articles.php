<?
session_start();
include_once($_SERVER['DOCUMENT_ROOT'].'/core/config.php');
?>
<?if (empty($_SESSION['USER_ID']) || empty($_SESSION['USER_GROUP']) || (!in_array(1,$_SESSION['USER_GROUP']) && !in_array(2,$_SESSION['USER_GROUP']))):?>

<?else:?>
<?
    //print_r($_POST);
    //print_r($_FILES);
if (!empty($_POST)) {
    $result = false;
    if (empty($MyDB)) {
        $MyDB = new dbconnect;
    }
    $MyDB->Connect();
    $uploaddir = UPLOAD_PATH;
    $start = "NULL";
    $end = "NULL";
    $sTime = '00:00:00';
    $eTime = '23:59:59';
    if(!empty($_POST['starttime'])) {
        $sTime = $_POST['starttime'].':00';
    }
    if(!empty($_POST['endtime'])) {
        $eTime = $_POST['endtime'].':59';
    }
    if(!empty($_POST['start'])) {
        $startArr = explode('.',$_POST['start']);
        $start = "'".$startArr[2].'-'.$startArr[1].'-'.$startArr[0]." ".$sTime."'";
    }
    if(!empty($_POST['end'])) {
        $endArr = explode('.',$_POST['end']);
        $end = "'".$endArr[2].'-'.$endArr[1].'-'.$endArr[0]." ".$eTime."'";
    }
    $picture = $uploaddir.basename($_FILES['miniature']['name']);
    /*if($_FILES['miniature']['size'] > 1048576) {
        echo "Error1";
        exit;
    }*/
    $miniatureName = "'".$_POST['pre_miniature']."'";
    if (!empty($_FILES['miniature']['name'])&&$_FILES['miniature']['size']>0) {
        $filetype=$_FILES['miniature']['type'];
        $ftype=explode("/",$filetype);
        $n_name=time();
        if ($ftype[1]=='jpeg'||$ftype[1]=='jpg'||$ftype[1]=='png'||$ftype[1]=='gif') {
            if ($ftype[1]=='jpeg') {$nftype='jpg';}
            else {$nftype=$ftype[1];}
            $new_name=ROOT_PATH.'/'.$uploaddir.$n_name.".".$nftype;
            $dbbildname = '/'.$uploaddir.$n_name.".".$nftype;
            if (move_uploaded_file($_FILES['miniature']['tmp_name'], $new_name))	{
                $miniatureName = "'".$dbbildname."'";
            }
        }
    }
    $status = 0;
    if (!empty($_POST['status'])&&$_POST['status']=='on') {
        $status = 1;
    }
    if ($_POST['key']>0) {
        
        $MyDB->Text = "UPDATE `article`
        SET `title`= '".$_POST['title']."',
            `datestart` = ".$start.",
            `dateend` = ".$end.",
            `content` = '".htmlspecialchars($_POST['maintext'])."',
            `image` = ".$miniatureName.",
            `category` =  '".$_POST['category']."',
            `type` = '".$_POST['type']."',
            `language` = '".$_POST['language']."',
            `status` = '".$status."'
        WHERE `id`='".$_POST['key']."'";
        $MyDB->Query();
        $MyDB->ErrorNum();
        if (empty($MyDB->Error)) {
            $result = true;
        }
    }
    elseif ($_POST['key']==0) {
        
        $MyDB->Text = "INSERT INTO
        `article`
        (
            `title`,
            `datestart`,
            `dateend`,
            `content`,
            `image`,
            `category`,
            `type`,
            `language`,
            `status`
        )
        VALUES
        (
            '".$_POST['title']."',
            ".$start.",
            ".$end.",
            '".htmlspecialchars($_POST['maintext'])."',
            ".$miniatureName.",
            '".$_POST['category']."',
            '".$_POST['type']."',
            '".$_POST['language']."',
            '".$status."'
        )";
        $MyDB->Query();
        $MyDB->ErrorNum();
        if (empty($MyDB->Error)) {
            $result = true;
        }
    }
    else {
        
    }
    $logName = 'articles';
    $logPath = 'admin';
    $actionId = 'New';
    if ($_POST['key']>0) {
        $actionId = 'Update, ID: '.$_POST['key'];
    }
    if ($result) {
        $event = 'Article "'.$_POST['title'].'" is saved ('.$actionId.'). Category: '.$_POST['category'].', type: '.$_POST['type'].', language: '.$_POST['language'].'. User ID: '.$_SESSION['USER_ID'];
        writeLog ($logName,$event,$logPath,true);
        echo "Запись \"".$_POST['title']."\" сохранена";
    }
    else {
        $event = 'Article "'.$_POST['title'].'" is NOT saved ('.$actionId.'). Category: '.$_POST['category'].', type: '.$_POST['type'].', language: '.$_POST['language'].'. User ID: '.$_SESSION['USER_ID'];
        writeLog ($logName,$event,$logPath,true);
        echo "Ошибка!";
    }
}
//$start_articles = getArticles (3,2,1,SITE_LANG);

?>

 <script src="/admin/js/tinymce/tinymce.min.js"></script>
 <script>

</script>

<div id="articleapp">Load...</div>
<script type="text/babel" src="/admin/js/article.js"></script> 
 <script>

 </script>
<?endif;?>