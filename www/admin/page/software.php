<?
session_start();
include_once($_SERVER['DOCUMENT_ROOT'].'/core/config.php');
$logName = 'software';
$logPath = 'admin';
?>
<?if (empty($_SESSION['USER_ID']) || empty($_SESSION['USER_GROUP']) || (!in_array(1,$_SESSION['USER_GROUP']) && !in_array(2,$_SESSION['USER_GROUP']))):?>

<?else:?>

<h2><?=A_SOFTWARE;?></h2>

<?
if (empty($MyDB)) {
    $MyDB = new dbconnect;
}
$MyDB->Connect();
if (!empty($_POST['filetoremove'])) {
    $MyDB->Text = "SELECT `id`, `name`, `version`, `file`, `status`
    FROM `software` 
    WHERE `id`='".$_POST['filetoremove']."'
    LIMIT 1";
    $MyDB->Query();
    $MyDB->Assoc();
    $filePathToRemove = ROOT_PATH.$MyDB->Data[0]['file'];
    if (file_exists($filePathToRemove)) {
        if (unlink($filePathToRemove)) {
            $MyDB->Text = "DELETE FROM `software` 
            WHERE `id`='".$_POST['filetoremove']."'";
            $MyDB->Query();$MyDB->ErrorNum();
            if (empty($MyDB->Error)) {
                $event = 'File is removed. File: '.$filePathToRemove.', ID: '.$_POST['filetoremove'].'. User ID: '.$_SESSION['USER_ID'];
                writeLog ($logName,$event,$logPath,true);
                echo 'Файл удален';
            }
            else {
                $event = 'File is NOT removed. DB error: '.$MyDB->Error.'. File: '.$filePathToRemove.', ID: '.$_POST['filetoremove'].'. User ID: '.$_SESSION['USER_ID'];
                writeLog ($logName,$event,$logPath,true);
                echo 'Файл удален. При удалении записи о фале возникла ошибка';
            }
        }
        else {
            $event = 'File is NOT removed. File: '.$filePathToRemove.'. User ID: '.$_SESSION['USER_ID'];
            writeLog ($logName,$event,$logPath,true);
            echo 'Ошибка. Файл не удален';
        }
    }
    else {
        $MyDB->Text = "DELETE FROM `software` 
        WHERE `id`='".$_POST['filetoremove']."'";
        $MyDB->Query();$MyDB->ErrorNum();
        if (empty($MyDB->Error)) {
            $event = 'Record is removed. ID: '.$_POST['filetoremove'].'. User ID: '.$_SESSION['USER_ID'];
            writeLog ($logName,$event,$logPath,true);
            echo 'Запись удалена';
        }
        else {
            $event = 'Record is NOT removed. ID: '.$_POST['filetoremove'].'. User ID: '.$_SESSION['USER_ID'];
            writeLog ($logName,$event,$logPath,true);
            echo 'При удалении записи о фале возникла ошибка';
        }
    }
}
elseif (!empty($_POST)) {
    $result = false;
    $fileisuploaded = false;
    $uploaddir = SOFTWARE_PATH;
    $filename = $uploaddir.basename($_FILES['updatefile']['name']);
    //$swfileName = "'".$_POST['pre_swfile']."'";
    $swfileName = '';
    if (!empty($_FILES['updatefile']['name'])&&$_FILES['updatefile']['size']>0) {
        $filetype=$_FILES['updatefile']['type'];
        //print_r($filetype);
        $ftype=explode("/",$filetype);
        //$n_name=time();
        if ($ftype[1]=='x-hex') {
            /*if (file_exists(ROOT_PATH.'/'.$uploaddir.$_FILES['updatefile']['name'])) {
                echo 'File already exist! ';
            }*/
            //else {
                $new_name=ROOT_PATH.'/'.$uploaddir.$_FILES['updatefile']['name'];
                $dbfilename = '/'.$uploaddir.$_FILES['updatefile']['name'];
                if (move_uploaded_file($_FILES['updatefile']['tmp_name'], $new_name))	{
                    $swfileName = "'".$dbfilename."'";
                    $fileisuploaded = true;
                }
            //}
            
        }
    }
    $status = 0;
    if (!empty($_POST['status'])&&$_POST['status']=='on') {
        $status = 1;
    }
    /*---------------*/
    $status = 1;
    /*---------------*/
    if ($fileisuploaded) {
        $MyDB->Text = "INSERT INTO
        `software`
        (
            `name`,
            `version`,
            `file`,
            `status`
        )
        VALUES
        (
            '".$_POST['name']."',
            '".$_POST['version']."',
            ".$swfileName.",
            '".$status."'
        )";
        $MyDB->Query();
        $MyDB->ErrorNum();
        if (empty($MyDB->Error)) {
            $result = true;
        }
    }
    
    if ($result) {
        $event = 'File is uploaded. File: '.$dbfilename.'. User ID: '.$_SESSION['USER_ID'];
        writeLog ($logName,$event,$logPath,true);
        echo "Запись \"".$_POST['name']."\" сохранена";
    }
    else {
        $event = 'Error of upload file or insert in DB. File: '.$dbfilename.'. User ID: '.$_SESSION['USER_ID'];
        writeLog ($logName,$event,$logPath,true);
        echo "Ошибка!";
    }
}

?>
<div class="table-block" style="width: 100%;">
<div class="table-row">
<div class="table-cell updatecontent">
    <table class="table upload-table">
        <thead>
            <tr>
                <th>
                    <?=UPLOAD_FILE_NAME;?>
                </th>
                <th>
                    <?=UPLOAD_FILE_VERSION;?>
                </th>
                <th>
                    <?=THE_FILE;?>
                </th>
                <th>
                    
                </th>
            </tr>
        </thead>
        <tbody>
    <?
    $MyDB->Text = "SELECT `id`, `name`, `version`, `file`, `status`
    FROM `software`
    ";
    $MyDB->Query();
    $MyDB->Assoc();
    $softwareArr = $MyDB->Data;
    ?>
    <?if(empty($softwareArr)):?>
            <tr>
                <td colspan="4">
                    <?=LIST_IS_EMPTY;?>
                </td>
            </tr>
    <?else:?>
        <?foreach($softwareArr as $sw => $software):?>
            <?
            $notFileClass = '';
            $notFileTitle = '';
            if(!file_exists(ROOT_PATH.$software['file'])) {
                $notFileClass = ' class="red-text"';
                $notFileTitle = ' title="'.FILE_NOT_FOUND.'"';
            }
            ?>
            <tr>
                <td>
                    <?=$software['name'];?>
                </td>
                <td>
                    <?=$software['version'];?>
                </td>
                <td<?=$notFileClass.$notFileTitle;?>>
                    <?=$software['file'];?>
                </td>
                <td>
                    <form action="" name="removefile_<?=$software['id'];?>" id="removefile_<?=$software['id'];?>" method="post">
                        <input form="removefile_<?=$software['id'];?>" type="hidden" name="filetoremove" value="<?=$software['id'];?>" />
                        <button form="removefile_<?=$software['id'];?>" type="submit" class="btn btn-primary glyphicon glyphicon-remove" name="removedata_<?=$software['id'];?>" id="removedata_<?=$software['id'];?>" title="Удалить" onclick="return checkDelFile('<?=$software['file'];?>',<?=file_exists(ROOT_PATH.$software['file'])?0:1;?>)"></button>
                    </form>
                </td>
            </tr>
        <?endforeach;?>
    <?endif;?>
        </tbody>
    </table>
</div>
<div class="table-cell updatecontent left-light-border" style="width: 300px;">
    <div class="file-upload-box">
        <span class="file-upload-title"></span>
        <div class="upload-text" id="uploadtext"><?=CHOOSE_FILE_TO_UPLOAD?><span><br><?=OR_DRAG_TO_HERE?></span></div>
        <input form="softwareform" type="file" name="updatefile" id="updatefile" class="file-upload-button" title="<?=CHOOSE_FILE_TO_UPLOAD?>" onChange="changeFile(this);" />
        <input form="softwareform" type="hidden" name="pre_swfile" id="pre_swfile" value="" />
        <span class="delete-file" onClick="removeFile();" title="<?=REMOVE_FILE;?>">&#215;</span>
    </div>
    <br />
    <input form="softwareform" type="text" name="name" id="name" placeholder="<?=UPLOAD_FILE_NAME;?>" />
    <br />
    <br />
    <input form="softwareform" type="text" name="version" id="version" placeholder="<?=UPLOAD_FILE_VERSION;?>" />
    <br />
    <br />
    <!--<input type="checkbox" name="status" id="status" value="on" />
    <label for="status"><?//=A_AVFORDNL;?></label>
    <br />
    <br />-->
    <input form="softwareform" type="submit" id="refreshmessages" class="uploaddata" value="<?=UPLOAD_FILE;?>" />
</div>
</div>
</div>
</div>
<form name="softwareform" id="softwareform" action="" enctype="multipart/form-data" method="post">
</form>
<script>
function checkDelFile(fstr,isrec=0) {
    var alertStr = isrec===0 ? 'surefileremove' : 'surercordremove';
    return confirm(ln[alertStr]+'"'+fstr+'"?');
}
function updateUploadText(th) {
    var text = $(th).val();
    var defaultText = '<?=CHOOSE_FILE_TO_UPLOAD?><span><br><?=OR_DRAG_TO_HERE?>';
    if(isEmpty(text)) {
        text = defaultText;
    }
    $('#uploadtext').html(text);
    $('#pre_swfile').val(text);
}
function changeFile(th) {
    updateUploadText(th);
}
function removeFile() {
    $('#updatefile').val('');
    updateUploadText($('#updatefile'));
}
</script>

<?endif;?>