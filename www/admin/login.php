<?
$errortext = '';
if (isset($err)&&$err==2) {
    $errortext = '<span class="red-text">'.SEC_CHECK_FAILED.'</span>';
}
if (isset($err)&&$err==1) {
    $errortext = '<span class="red-text">'.LOGINPASS_FAILED.'</span>';
}
?>

<div class="page_container">
    <h2><?=SIGN_IN;?></h2>
    <?=$errortext?>
    <form action="http://<?=SITE_URL;?>/admin/signin/" name="login-form" id="login-form" method="post" onsubmit="return sendForm(this);">
        <input type="text" class="signform<?=isset($err)&&$err==1?' red-light':'';?>" name="login" id="login" placeholder="<?=LOGIN;?>" value="<?=!empty($_POST['login'])?$_POST['login']:'';?>" />
        <br />
        <input type="password" class="signform<?=isset($err)&&$err==1?' red-light':'';?>" name="password" id="password" placeholder="<?=PASSWORD;?>" />
        <br />
        <div class="rcpt<?=isset($err)&&$err==2?' red-light':'';?>">
        <div class="g-recaptcha" data-sitekey="<?=RECAPTCHA1;?>"></div>
        </div>
        <br />
        <input type="submit" class="signform" name="signin" id="signin" value="<?=SIGN_IN;?>" />
    </form>
    <input type="hidden" id="checkcpt" value="0" />
</div>