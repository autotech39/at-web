<header id="header">
    <a id="logo_mini" href="http://<?=SITE_URL;?>" title="<?=A_GO_TO_SITE;?>"></a>
    <input type="checkbox" id="showmenu" />
    <label for="showmenu" class="show-menu">&#9776;</label>
    <div id="menu_block">
        <ul>
            <li><a href="/admin/articles/"><?=A_ARTICLES;?></a></li>
            <?if(in_array(1,$_SESSION['USER_GROUP'])):?>
            <li><a href="/admin/users/"><?=A_USERS;?></a></li>
            <li><a href="/admin/software/"><?=A_SOFTWARE;?></a></li>
            <li><a href="/admin/devices/"><?=DEVICES;?></a></li>
            <?endif;?>
            <li><a href="/admin/signout/"><?=M_LOG_OUT;?></a></li>
        </ul>
    </div>
</header>