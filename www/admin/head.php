<!DOCTYPE html>
<html lang="en">
<head>
    <title>AutoTech R&D of electronic systems and devices</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Cache-Control" content="no-cache">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Sansita" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="<?=SRC_TEMPLATE_PATH;?>/style.css">
    <link rel="stylesheet" type="text/css" href="<?=SRC_TEMPLATE_PATH;?>/private.css">
    <link rel="stylesheet" type="text/css" href="/admin/style.css">
    <link rel="stylesheet" type="text/css" href="/admin/js/jquery-ui/jquery-ui.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="/admin/js/react/react.js"></script>
    <script src="/admin/js/react/react-dom.js"></script>
    <script src="/admin/js/react/browser.js"></script>
    <script src="/admin/js/jquery-ui/jquery-ui.min.js"></script>
    <script src="/admin/js/function.js"></script>
    <script src="/admin/js/lng/<?=SITE_LANG;?>.js"></script>
    <script src="https://www.google.com/recaptcha/api.js?hl=<?=SITE_LANG;?>"></script>
</head>
<body id="myPage" data-spy="scroll" data-target=".navbar" data-offset="60">