var jsonData;
let DATA = [];
if(jsonData = sAjax('/api/user','','get')) {
    DATA = JSON.parse(jsonData);
}

var User = React.createClass({
    getInitialState: function() {
        return {
            userData: this.props.userData
        }
    },
    componentWillUpdate: function(nextProps) {
        if(this.props.userData!=nextProps.userData) {
            this.setState({
                userData: nextProps.userData
            });
        }
    },
    changeStatus: function() {
        /*var th = this;
        console.log(this.state.);*/
        //if(this.state.UserData.status==1) {
        var result = '';
        if(confirm(ln.areyouchangestatus)) {
            var statusResult = false;
            if(jsonData = sAjax('/api/user/status/'+this.state.userData.id,'','get')) {
                statusResult = JSON.parse(jsonData);
                if (statusResult.answer=='1') {
                    this.props.updateData();
                    result = ln.statusischanged;
                    /*this.setState({
                    });*/
                }
                else {
                    result = ln.error[statusResult.error];
                }
                this.props.changeResult(result);
                //alert(statusResult.description);
            }
            else {
                console.log('Error');
            }
        }
            
        //}
    },
    render: function() {
        var blockClass = 'userunblocked';
        if(this.state.userData.status==0) {
            blockClass = 'userblocked';
        }
        else {
            blockClass = 'userunblocked';
        }
        //var uid = this.state.UserData.id;
        return (
            <div className="userstring table-row">
                <div className="usercell table-cell">{this.state.userData['id']}</div>
                <div className="usercell table-cell">{this.state.userData['login']}</div>
                <div className="usercell table-cell">{this.state.userData['email']}</div>
                <div className="usercell table-cell">{this.state.userData['groupname']}</div>
                <div className="usercell table-cell">{this.state.userData['language']}</div>
                <div className="usercell table-cell">{this.state.userData['status']}</div>
                <div className="usercell table-cell">
                    <span className={blockClass} onClick={this.changeStatus}></span>
                </div>
            </div>
        );
    }
});

var Users = React.createClass({
    getInitialState: function() {
        return {
            usersData: this.props.usersData,
            changeResult: ''
        }
    },
    componentWillUpdate: function(nextProps) {
        if(this.props.usersData!=nextProps.usersData) {
            this.setState({
                usersData: nextProps.usersData
            });
        }
    },
    changeResult: function(txt) {
        this.setState({
            changeResult: txt
        });
        $('#resulttext').show();
        $('#resulttext').fadeOut(5000);
    },
    render: function() {
        var th = this;
        return (
            <div>
                <div className="resultbox">
                    <span id="resulttext">{this.state.changeResult}</span></div>
                <div className="userblock table-block">
                    <div className="userhead table-row">
                        <div className="usercell table-cell">ID</div>
                        <div className="usercell table-cell">{ln.login}</div>
                        <div className="usercell table-cell">{ln.email}</div>
                        <div className="usercell table-cell">{ln.group}</div>
                        <div className="usercell table-cell">{ln.language}</div>
                        <div className="usercell table-cell">{ln.status}</div>
                        <div className="usercell table-cell"></div>
                    </div>
                    {
                        this.state.usersData.map(function(user){
                            return (
                                <User
                                    key={user.id}
                                    userData={user}
                                    updateData={th.props.updateData}
                                    changeResult={th.changeResult}
                                />
                            )
                        })
                    }
                </div>
            </div>
        );
    }
});


var App = React.createClass({
    getInitialState: function() {
        return {
            usersData: DATA
        }
    },
    updateData: function() {
        var newData = [];
        if(jsonData = sAjax('/api/user','','get')) {
            newData = JSON.parse(jsonData);
        }
        this.setState({
            usersData: newData
        });
    },
    render: function() {
        return (
            <Users usersData={this.state.usersData} updateData={this.updateData} />
        );
    }
});


ReactDOM.render(
    <App />,
    document.getElementById('appbox')
);