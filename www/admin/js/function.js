function isEmpty(val) {
    if (val===''||val===undefined||val=='undefined'||val===false||val===0||val===null) {
        return true;
    }
    else {
        return false;
    }
}

function aXhr () {
    try {
        xhr = new ActiveXObject("Msxml2.XMLHTTP");
    } catch (e) {
        try {
            xhr = new ActiveXObject("Microsoft.XMLHTTP");
        } catch (E) {
            xhr = false;
        }
    }
    if (!xhr && typeof XMLHttpRequest!='undefined') {
        xhr = new XMLHttpRequest();
    }
    return xhr;
}

function ajax_xhr () { //Объект XMLHttpRequest для ajax-запроса
    var xhr;
    try {
        xhr = new ActiveXObject("Msxml2.XMLHTTP");
    } catch (e) {
        try {
            xhr = new ActiveXObject("Microsoft.XMLHTTP");
        } catch (E) {
            xhr = false;
        }
    }
    if (!xhr && typeof XMLHttpRequest!='undefined') {
        xhr = new XMLHttpRequest();
    }
    return xhr;
}


function getAjax (src,async=true) {
    return new Promise(function(resolve, reject) {
		var axhr = ajax_xhr();
		axhr.open('get', src,async);
        //axhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
		axhr.onreadystatechange = function() {
			if(axhr.readyState === 4) {
				resolve(axhr.responseText);
			}
		};
		axhr.send(null);
	})
}

function sAjax (src,param,meth) { //Синхронный ajax-запрос
    var axhr = this.ajax_xhr();
    var sajax_result;
    if (meth===''||meth=='POST'||meth=='post'||meth=='undefined'||meth=='null'||meth=='false'||meth===undefined||meth===null||meth===false) {
        axhr.onload = axhr.onerror = function() {
            if (this.status == 200) {
                sajax_result = this.responseText;
            } else {
                sajax_result = false;
            }
        }
        //alert(elfCore.sajax_result);
        axhr.open("POST", src, false);
        axhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        axhr.send(param);
    }
    else if (meth=='get'||meth=='GET') {
        if (param!==''&&param!='undefined'&&param!==undefined) {
            param = '?'+param;
        }
        axhr.onload = axhr.onerror = function() {
            if (this.status == 200) {
                sajax_result = this.responseText;
              
            } else {
                sajax_result = false;
            }
        }
        axhr.open("GET", src+param, false);
        axhr.send(null);
    }
    return sajax_result;
}