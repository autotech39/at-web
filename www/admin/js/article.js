var jsonData;
let ARTICLES = [];
if(jsonData = sAjax('/api/articles/date/desc','','get')) {
    ARTICLES = JSON.parse(jsonData);
}
let CATEGORY = [];
if(jsonData = sAjax('/api/articles/category','','get')) {
    CATEGORY = JSON.parse(jsonData);
}
let TYPE = [];
if(jsonData = sAjax('/api/articles/type','','get')) {
    TYPE = JSON.parse(jsonData);
}
let LNG = [];
if(jsonData = sAjax('/api/language','','get')) {
    LNG = JSON.parse(jsonData);
}
var d=new Date();
var day=d.getDate();
if (day < 10) {
    day = '0'+day;
}
var month=d.getMonth() + 1;
if (month < 10) {
    month = '0'+month;
}
var year=d.getFullYear();
let currentDate =day + "." + month + "." + year;

function expandArticleForm () {
    var header = $('#ui-id-1');
    var headtext = $('#ui-id-1 span');
    var box = $('#ui-id-2');
    header.removeClass('ui-accordion-header-collapsed ui-corner-all');
    header.addClass('ui-accordion-header-active ui-state-active');
    headtext.removeClass('ui-icon-triangle-1-e');
    headtext.addClass('ui-icon-triangle-1-s');
    header.attr('aria-selected','true');
    header.attr('aria-expanded','true');
    box.addClass('ui-accordion-content-active');
    box.show();
}

function addTinyMCE(box='#maintext') {
    tinymce.remove(box);
        tinymce.init({
            selector: box,
            language: 'ru',
            menubar: false,
            plugins: [
                'advlist autolink lists link image jbimages charmap print preview anchor',
                'searchreplace visualblocks code fullscreen',
                'insertdatetime media table contextmenu paste code'
            ],
            toolbar: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image jbimages'
        });
}

var miniatureBg = '/admin/img/foto.png';

var Article = React.createClass({
    getInitialState: function() {
        return {
            articleData: this.props.articleData
        }
    },
    render: function() {
        return (
            
            <div className="articlesting table-row">
                <div className="table-cell">{this.state.articleData['id']}</div>
                <div className="table-cell">{this.state.articleData['title']}</div>
                <div className="table-cell">{this.state.articleData['datestart']}</div>
                <div className="table-cell">{this.state.articleData['dateend']}</div>
                <div className="table-cell">{this.state.articleData['category_name']}</div>
                <div className="table-cell">{this.state.articleData['type_name']}</div>
                <div className="table-cell">{this.state.articleData['language_code']}</div>
                <div className="table-cell">{this.state.articleData['timestamp']}</div>
                <div className="table-cell">{this.state.articleData['status']}</div>
                <div className="table-cell">
                    <span className="articleeditbutton" onClick={this.props.editArticle.bind(null,this.state.articleData['id'])}></span>
                </div>
            </div>
        );
    }
});

var ArticleBox = React.createClass({
    getInitialState: function() {
        return {
            articlesData: this.props.articlesData
        }
    },
    render: function() {
        let th = this;
        return (
            <div id="articlelistbox">
                <h3>{ln.articles}</h3>
                <div>
                    <div className="articlelist table-block">
                        <div className="articlelisthead table-row">
                            <div className="table-cell">ID</div>
                            <div className="table-cell">{ln.title}</div>
                            <div className="table-cell">{ln.datestart}</div>
                            <div className="table-cell">{ln.dateend}</div>
                            <div className="table-cell">{ln.category}</div>
                            <div className="table-cell">{ln.type}</div>
                            <div className="table-cell">{ln.language}</div>
                            <div className="table-cell">{ln.datecreate}</div>
                            <div className="table-cell">{ln.status}</div>
                            <div className="table-cell"></div>
                        </div>
                        {
                            this.state.articlesData.map(function(article){
                                //console.log(article);
                                return (
                                    <Article
                                        key={article.id}
                                        articleData={article}
                                        editArticle={th.props.editArticle}
                                    />
                                )
                            })
                        }
                    </div>
                </div>
             </div>
            
        );
    }
});

var Languages = React.createClass({
    getInitialState: function() {
        return {
            lngData: LNG,
            lngDefault: this.props.language
        }
    },
    componentWillUpdate: function(nextProps) {
        if(this.props.language!=nextProps.language) {
            this.setState({lngDefault: nextProps.language});
        }
    },
    changeValue: function(e) {
        this.setState({lngDefault: e.target.value});
    },
    render: function() {
        var th = this;
        return (
            <select name="language" id="language" value={th.state.lngDefault} onChange={this.changeValue}>
            {
                this.state.lngData.map(function(lng){
                    return (
                        <option key={lng.id} value={lng.id}>{lng.name}</option>
                    )
                })
            }
            </select>
        );
    }
});

var Types = React.createClass({
    getInitialState: function() {
        return {
            typeData: TYPE,
            typeDefault: this.props.types
        }
    },
    componentWillUpdate: function(nextProps) {
        if(this.props.types!=nextProps.types) {
            this.setState({typeDefault: nextProps.types});
        }
    },
    changeValue: function(e) {
        this.setState({typeDefault: e.target.value});
    },
    render: function() {
        return (
            <select name="type" id="type" value={this.state.typeDefault} onChange={this.changeValue}>
            {
                this.state.typeData.map(function(tp){
                    return (
                        <option key={tp.id} value={tp.id}>{tp.name}</option>
                    )
                })
            }
            </select>
        );
    }
});

var Categories = React.createClass({
    getInitialState: function() {
        return {
            catData: CATEGORY,
            catDefault: this.props.category
        }
    },
    componentWillUpdate: function(nextProps) {
        if(this.props.category!=nextProps.category) {
            this.setState({catDefault: nextProps.category});
        }
    },
    changeValue: function(e) {
        this.setState({catDefault: e.target.value});
    },
    render: function() {
        return (
            <select name="category" id="category" value={this.state.catDefault} onChange={this.changeValue}>
            {
                this.state.catData.map(function(cat){
                    return (
                        <option key={cat.id} value={cat.id}>{cat.name}</option>
                    )
                })
            }
            </select>
        );
    }
});

var ArticleForm = React.createClass({
    getInitialState: function() {
        return {
            boxHeader: ln.newarticle,
            articleId: 0,
            articleTitle: '',
            articleStart: currentDate,
            articleTimeStart: '00:00',
            articleEnd: '',
            articleTimeEnd: '',
            articleContent: '',
            articleLng: 1,
            articleCategory: 1,
            articleType: 1,
            articleImage: miniatureBg,
            preArticleImage: '',
            isActive: false,
            isChange: 0
        }
    },
    componentDidMount: function(){
        addTinyMCE();
    },
    componentDidUpdate: function(prevProps,prevState){
        addTinyMCE();
        //alert(this.state.isChange+' :: '+prevState.isChange);
        if(this.state.isChange!=prevState.isChange) {
            this.props.setChange(this.state.isChange)
            //alert(this.state.isChange+' :: '+prevState.isChange);
        }
        
    },
    componentWillUpdate: function(nextProps) {
        if(this.props.editableData!=nextProps.editableData) {
            var dStart = new Date(nextProps.editableData.datestart);
            var ds = dStart.getDate();
            var ms = dStart.getMonth()+1;
            var ys = dStart.getFullYear();
            if (ds < 10) {
                ds = '0' + ds;
            }
            if (ms < 10) {
                ms = '0' + ms;
            }
            var sDate = ds+'.'+ms+'.'+ys;
            var hs = dStart.getHours();
            var ns = dStart.getMinutes();
            if (hs < 10) {
                hs = '0' + hs;
            }
            if (ns < 10) {
                ns = '0' + ns;
            }
            var sTime = hs+':'+ns;
            var eDate = '';
            if (!isEmpty(nextProps.editableData.dateend)) {
                var dEnd = new Date(nextProps.editableData.dateend);
                var de = dEnd.getDate();
                var me = dEnd.getMonth()+1;
                var ye = dEnd.getFullYear();
                if (de < 10) {
                    de = '0'+de;
                }
                if (ms < 10) {
                    me = '0'+me;
                }
                eDate = de+'.'+me+'.'+ye;
                
                var he = dEnd.getHours();
                var ne = dEnd.getMinutes();
                if (he < 10) {
                    he = '0' + he;
                }
                if (ne < 10) {
                    ne = '0' + ne;
                }
                var eTime = he+':'+ne;
            }
            var artMiniatureImg = miniatureBg;
            if (nextProps.editableData.image!==null&&nextProps.editableData.image!='') {
                artMiniatureImg = nextProps.editableData.image;
            }
            var active = false;
            if (nextProps.editableData.status==1) {
                active = true;
            }
            this.setState({
                articleId: nextProps.editableData.id,
                articleTitle: nextProps.editableData.title,
                articleStart: sDate,
                articleTimeStart: sTime,
                articleEnd: eDate,
                articleTimeEnd: eTime,
                articleContent: nextProps.editableData.content,
                articleLng: nextProps.editableData.language,
                articleCategory: nextProps.editableData.category,
                articleType: nextProps.editableData.type,
                articleImage: artMiniatureImg,
                isActive: active,
                preArticleImage: nextProps.editableData.image
            });
            if (nextProps.editableData.id>0) {
                this.setState({boxHeader: ln.editarticle});
            }
        }
    },
    changeTitle: function(e) {
        this.setState({
            articleTitle: e.target.value,
            isChange: 1
        });
    },
    changeContent: function(e) {
        this.setState({
            articleContent: e.target.value,
            isChange: 1
        });
    },
    changeDate: function() {
        this.setState({
            isChange: 1
        });
        return true;
    },
    changeStartTime: function(e) {
        this.setState({
            articleTimeStart: e.target.value,
            isChange: 1
        });
    },
    changeEndTime: function(e) {
        this.setState({
            articleTimeEnd: e.target.value,
            isChange: 1
        });
    },
    newArticle: function() {
        if(this.checkChange()) {
            this.setState({
                articleId: 0,
                articleTitle: '',
                articleStart: currentDate,
                articleTimeStart: '00:00',
                articleEnd: '',
                articleTimeEnd: '',
                articleContent: '',
                articleLng: 1,
                articleCategory: 1,
                articleType: 1,
                articleImage: miniatureBg,
                preArticleImage: '',
                isActive: false,
                boxHeader: ln.newarticle
            });
            addTinyMCE();
        }
    },
    checkChange: function() {
        if(this.state.isChange==1) {
            return confirm ('Изменения будут потеряны. Продолжить?');
        }
        else {
            return true;
        }
    },
    changeImage: function() {
        this.setState({
            preArticleImage: ''
        });
    },
    removeImage: function() {
        this.setState({
            preArticleImage: '',
            articleImage: miniatureBg
        });
    },
    changeActive: function() {
        this.setState({
            isActive: !this.state.isActive
        })
    },
    render: function() {
        var miniatureStyle = {
            backgroundImage: 'url('+this.state.articleImage+')'
        };
        return (
            <div id="editorbox">
                <h3>{this.state.boxHeader}</h3>
                <div className="editorback">
                <a name="topeditor" id="topeditor"></a>
                    <form action="" method="post" encType="multipart/form-data" id="articleform" className="articleform">
                        <input type="text" name="title" id="title" className="article-title" placeholder={ln.title} value={this.state.articleTitle} onChange={this.changeTitle} />
                        <br />
                        {ln.activefrom}:&nbsp;
                        <input type="text" className="datepicker" name="start" placeholder={ln.datestart} value={this.state.articleStart} onChange={this.changeDate}/>
                        <input type="time" name="starttime" id="starttime" value={this.state.articleTimeStart} onChange={this.changeStartTime} placeholder={ln.time+' [hh:mm]'} />
                        <br />
                        {ln.activeto}:&nbsp;
                        <input type="text" className="datepicker" name="end" placeholder={ln.dateend} value={this.state.articleEnd} onChange={this.changeDate} />
                        <input type="time" name="endtime" id="endtime" value={this.state.articleTimeEnd} onChange={this.changeEndTime} placeholder={ln.time+' [hh:mm]'} />
                        <br />
                        <textarea id="maintext" name="maintext" value={this.state.articleContent} onChange={this.changeContent}></textarea>
                        <br />
                        <label htmlFor="category"> {ln.category}: </label>
                        <Categories category={this.state.articleCategory} />
                        <label htmlFor="type"> {ln.type}: </label>
                        <Types types={this.state.articleType} />
                        <label htmlFor="language"> {ln.language}: </label>
                        <Languages language={this.state.articleLng} />
                        &nbsp;&nbsp;&nbsp;&nbsp;
                        <label>
                            <input type="checkbox" name="status" id="status" checked={this.state.isActive} onChange={this.changeActive} />
                            &nbsp;{ln.isactive}
                        </label>
                        <br />
                        <div className="img-upload-preview" style={miniatureStyle}>
                            <span className="img-upload-title">{ln.miniature}</span>
                            <input type="file" name="miniature" id="miniature" className="img-upload-button" title={ln.miniature} onChange={this.changeImage} />
                            <input type="hidden" name="pre_miniature" id="pre_miniature" value={this.state.preArticleImage} />
                            <span className="delete-miniature" onClick={this.removeImage} title={ln.removeminiature}>X</span>
                        </div>
                        <br />
                        <input type="hidden" name="key" id="key" value={this.state.articleId} />
                        <input type="button" name="newarticle" id="newarticle" value={ln.createnewarticle} onClick={this.newArticle} />
                        <input type="submit" name="submit" value={ln.save} />
                    </form>
                </div>
             </div>
        );
    
    }
});

var PopUp = React.createClass({
    getInitialState: function() {
        return {
            isChange: this.props.isChange
        }
    },
    setWindowParam: function() {
        var th = this;
        $('#dropchangewindow').dialog({
            dialogClass: "no-close",
            buttons: [
              {
                text: "OK",
                click: function() {
                  th.setState({
                    isChange: 0
                  });
                  th.props.setChange(0);
                  $(this).dialog('close');
                }
              },
              {
                text: "Cancel",
                click: function() {
                  $(this).dialog('close');
                }
              }
            ]
          });
    },
    componentDidMount: function() {
        this.setWindowParam();
    },
    componentDidUpdate: function() {
        this.setWindowParam();
    },
    render: function() {
        return (
            <div id="dropchangewindow" title="Забыть изменения?">
                <p>Изменения будут утеряны. Продолжить?</p>
            </div>
        );
    }
    
});

var App = React.createClass({
    getInitialState: function() {
        return {
            articlesData: ARTICLES,
            editableData: [],
            isChange: 0
        }
    },
    componentDidMount: function() {
        var th = this;
        $( "#editorbox" ).accordion({
            collapsible: true,
            heightStyle: 'content',
            active: 1
        });
        $( ".datepicker" ).datepicker({ dateFormat: "dd.mm.yy"});
        if (ln.code=='ru') {
            $.datepicker.regional['ru'] = {
                monthNames: ['Январь', 'Февраль', 'Март', 'Апрель',
                'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь',
                'Октябрь', 'Ноябрь', 'Декабрь'],
                dayNamesMin: ['Вс','Пн','Вт','Ср','Чт','Пт','Сб'],
                firstDay: 1,
                };
            $.datepicker.setDefaults($.datepicker.regional['ru']);
        }
        function readURL(input) {
        
            if (input.files && input.files[0]) {
                var reader = new FileReader();
        
                reader.onload = function (e) {
                    $('.img-upload-preview').css('background-image','url('+e.target.result+')');
                };
        
                reader.readAsDataURL(input.files[0]);
            }
        }
        
        $("#miniature").change(function(){
            readURL(this);
        });
    },
    checkChange: function() {
        if(this.state.isChange==1) {
            return confirm ('Изменения будут потеряны. Продолжить?');
        }
        else {
            return true;
        }
    },
    editArticle: function(aid){
        if(this.checkChange()) {
            var editableArticle = ARTICLES.filter(function(el) {
                var searchValue = el.id;
                return searchValue.indexOf(aid) !== -1;
            });
            this.setState({editableData: editableArticle[0]});
            expandArticleForm();
            window.location.hash = 'topeditor';
        }
    },
    setChange: function(c=0) {
        this.setState({
            isChange: c
        });
    },
    render: function() {
        return (
            <div>
                <ArticleForm
                    editableData={this.state.editableData}
                    isChange={this.state.isChange}
                    setChange={this.setChange}
                />
                <ArticleBox
                    articlesData={this.state.articlesData}
                    editArticle={this.editArticle}
                    isChange={this.state.isChange}
                    setChange={this.setChange}
                />
            </div>
            
        );
    }
    /*render: function() {
        return (
            <div>
                <ArticleForm
                    editableData={this.state.editableData}
                    isChange={this.state.isChange}
                    setChange={this.setChange}
                />
                <ArticleBox
                    articlesData={this.state.articlesData}
                    editArticle={this.editArticle}
                    isChange={this.state.isChange}
                    setChange={this.setChange}
                />
                <PopUp
                    isChange={this.state.isChange}
                    setChange={this.setChange}
                />
            </div>
            
        );
    }*/
});


ReactDOM.render(
    <App />,
    document.getElementById('articleapp')
);