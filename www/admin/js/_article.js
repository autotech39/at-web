var jsonData;
let ARTICLES = [];
if(jsonData = sAjax('/api/articles/date/desc','','get')) {
    ARTICLES = JSON.parse(jsonData);
}
let CATEGORY = [];
if(jsonData = sAjax('/api/articles/category','','get')) {
    CATEGORY = JSON.parse(jsonData);
}
let TYPE = [];
if(jsonData = sAjax('/api/articles/type','','get')) {
    TYPE = JSON.parse(jsonData);
}
let LNG = [];
if(jsonData = sAjax('/api/language','','get')) {
    LNG = JSON.parse(jsonData);
}
var d=new Date();
var day=d.getDate();
if (day < 10) {
    day = '0'+day;
}
var month=d.getMonth() + 1;
if (month < 10) {
    month = '0'+month;
}
var year=d.getFullYear();
let currentDate =day + "." + month + "." + year;

var Article = React.createClass({
    getInitialState: function() {
        return {
            articleData: this.props.articleData
        }
    },
    render: function() {
        return (
            
            <div className="articlesting table-row">
                <div className="table-cell">{this.state.articleData['id']}</div>
                <div className="table-cell">{this.state.articleData['title']}</div>
                <div className="table-cell">{this.state.articleData['datestart']}</div>
                <div className="table-cell">{this.state.articleData['dateend']}</div>
                <div className="table-cell">{this.state.articleData['category_name']}</div>
                <div className="table-cell">{this.state.articleData['type_name']}</div>
                <div className="table-cell">{this.state.articleData['language_code']}</div>
                <div className="table-cell">{this.state.articleData['timestamp']}</div>
                <div className="table-cell">{this.state.articleData['status']}</div>
                <div className="table-cell">
                    <span className="articleeditbutton" onClick={this.props.editArticle.bind(null,this.state.articleData['id'])}></span>
                </div>
            </div>
        );
    }
});

var ArticleBox = React.createClass({
    getInitialState: function() {
        return {
            articlesData: this.props.articlesData
        }
    },
    render: function() {
        let th = this;
        return (
            <div id="articlelistbox">
                <h3>{ln.articles}</h3>
                <div>
                    <div className="articlelist table-block">
                        <div className="articlelisthead table-row">
                            <div className="table-cell">ID</div>
                            <div className="table-cell">{ln.title}</div>
                            <div className="table-cell">{ln.datestart}</div>
                            <div className="table-cell">{ln.dateend}</div>
                            <div className="table-cell">{ln.category}</div>
                            <div className="table-cell">{ln.type}</div>
                            <div className="table-cell">{ln.language}</div>
                            <div className="table-cell">{ln.datecreate}</div>
                            <div className="table-cell">{ln.status}</div>
                            <div className="table-cell"></div>
                        </div>
                        {
                            this.state.articlesData.map(function(article){
                                //console.log(article);
                                return (
                                    <Article
                                        key={article.id}
                                        articleData={article}
                                        editArticle={th.props.editArticle}
                                    />
                                )
                            })
                        }
                    </div>
                </div>
             </div>
            
        );
    }
});

var Languages = React.createClass({
    getInitialState: function() {
        return {
            lngData: LNG,
            lngDefault: this.props.language
        }
    },
    render: function() {
        var th = this;
        return (
            <select name="language" id="language" defaultValue={th.state.lngDefault}>
            {
                this.state.lngData.map(function(lng){
                    return (
                        <option key={lng.id} value={lng.id}>{lng.name}</option>
                    )
                })
            }
            </select>
        );
    }
});

var Types = React.createClass({
    getInitialState: function() {
        return {
            typeData: TYPE,
            typeDefault: this.props.type
        }
    },
    render: function() {
        return (
            <select name="type" id="type" defaultValue={this.state.typeDefault}>
            {
                this.state.typeData.map(function(tp){
                    return (
                        <option key={tp.id} value={tp.id}>{tp.name}</option>
                    )
                })
            }
            </select>
        );
    }
});

var Categories = React.createClass({
    getInitialState: function() {
        return {
            catData: CATEGORY,
            catDefault: this.props.categoty
        }
    },
    render: function() {
        return (
            <select name="category" id="category" defaultValue={this.state.catDefault}>
            {
                this.state.catData.map(function(cat){
                    return (
                        <option key={cat.id} value={cat.id}>{cat.name}</option>
                    )
                })
            }
            </select>
        );
    }
});

var ArticleForm = React.createClass({
    getInitialState: function() {
        return {
            boxHeader: ln.newarticle,
            articleId: 0,
            articleTitle: '',
            articleStart: currentDate,
            articleEnd: '',
            articleContent: '',
            articleLng: 1,
            articleCategory: 1,
            articleType: 1,
            articleImage: ''
        }
    },
    componentDidUpdate: function(nextProps) {
        if(this.props.editableData!=nextProps.editableData) {
            var dStart = new Date(nextProps.editableData.datestart);
            var ds = dStart.getDate();
            var ms = dStart.getMonth()+1;
            var ys = dStart.getFullYear();
            if (ds < 10) {
                ds = '0' + ds;
            }
            if (ms < 10) {
                ms = '0' + ms;
            }
            var sDate = ds+'.'+ms+'.'+ys;
            var eDate = '';
            if (!isEmpty(nextProps.editableData.dateend)) {
                var dEnd = new Date(nextProps.editableData.dateend);
                var de = dEnd.getDate();
                var me = dEnd.getMonth()+1;
                var ye = dEnd.getFullYear();
                if (de < 10) {
                    de = '0'+de;
                }
                if (ms < 10) {
                    me = '0'+me;
                }
                eDate = de+'.'+me+'.'+ye;
            }
            
            this.setState({
                articleId: nextProps.editableData.id,
                articleTitle: nextProps.editableData.title,
                articleStart: sDate,
                articleEnd: eDate,
                articleContent: nextProps.editableData.content,
                articleLng: nextProps.editableData.language,
                articleCategory: nextProps.editableData.category,
                articleType: nextProps.editableData.type,
                articleImage: nextProps.editableData.image
            });
        console.log(2);
        }
        console.log(3);
    },
    /*inputUpdate: function(value) {
        
    },*/
    render: function() {
        console.log(1);
        return (
            <div id="editorbox">
                <h3>{this.state.boxHeader}</h3>
                <div>
                <form action="" method="post" encType="multipart/form-data" id="articleform">
                    <input type="text" name="title" id="title" className="article-title" placeholder={ln.title} defaultValue={this.state.articleTitle} />
                    <br />
                    <input type="text" className="datepicker" name="start" placeholder={ln.datestart} defaultValue={this.state.articleStart} />
                    <input type="text" className="datepicker" name="end" placeholder={ln.dateend} defaultValue={this.state.articleEnd} />
                    <br />
                    <textarea id="maintext" name="text" defaultValue={this.state.articleContent}></textarea>
                    <div className="img-upload-preview">
                        <span className="img-upload-title">{ln.miniature}</span>
                        <input type="file" name="miniature" id="miniature" className="img-upload-button" title={ln.miniature} />
                    </div>
                    <label htmlFor="category"> {ln.category}: </label>
                    <Categories category={this.state.articleCategory} />
                    <label htmlFor="type"> {ln.type}: </label>
                    <Types type={this.state.articleType} />
                    <label htmlFor="language"> {ln.language}: </label>
                    <Languages language={this.state.articleLng} />
                    <br />
                    <input type="hidden" name="key" id="key" defaultValue={this.state.articleId} />
                    <input type="submit" name="submit" value={ln.save} />
                </form>
                </div>
             </div>
        );
    }
});



var App = React.createClass({
    getInitialState: function() {
        /*var editableData = {
            id: 0,
            title: '',
            datestart: '',
            dateend: '',
            content: '',
            language: '',
            category: 1,
            type: 1,
            image: ''
        }*/
        return {
            articlesData: ARTICLES,
            editableData: []
        }
    },
    componentDidMount: function() {
        tinymce.init({
            selector: '#maintext',  // change this value according to your HTML
            language: 'ru',
            menubar: false,
            plugins: [
                'advlist autolink lists link image jbimages charmap print preview anchor',
                'searchreplace visualblocks code fullscreen',
                'insertdatetime media table contextmenu paste code'
            ],
            toolbar: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image jbimages'
        });
        $( "#editorbox" ).accordion({
            collapsible: true,
            heightStyle: 'content',
            active: 1
        });
        $( ".datepicker" ).datepicker({ dateFormat: "dd.mm.yy"});
        if (ln.code=='ru') {
            $.datepicker.regional['ru'] = {
                monthNames: ['Январь', 'Февраль', 'Март', 'Апрель',
                'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь',
                'Октябрь', 'Ноябрь', 'Декабрь'],
                dayNamesMin: ['Вс','Пн','Вт','Ср','Чт','Пт','Сб'],
                firstDay: 1,
                };
            $.datepicker.setDefaults($.datepicker.regional['ru']);
        }
        function readURL(input) {
        
            if (input.files && input.files[0]) {
                var reader = new FileReader();
        
                reader.onload = function (e) {
                    //$('#image').attr('src', e.target.result);
                    $('.img-upload-preview').css('background-image','url('+e.target.result+')');
                };
        
                reader.readAsDataURL(input.files[0]);
            }
        }
        
        $("#miniature").change(function(){
            readURL(this);
        });
    },
    editArticle: function(aid){
        var editableArticle = ARTICLES.filter(function(el) {
            var searchValue = el.id;
            return searchValue.indexOf(aid) !== -1;
        });
        this.setState({editableData: editableArticle[0]});
    },
    render: function() {
        return (
            <div>
                <ArticleForm
                    editableData={this.state.editableData}
                />
                <ArticleBox
                    articlesData={this.state.articlesData}
                    editArticle={this.editArticle}
                />
            </div>
            
        );
    }
});


ReactDOM.render(
    <App />,
    document.getElementById('articleapp')
);