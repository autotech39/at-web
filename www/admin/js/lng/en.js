window.ln = {
    code: 'en',
    load: 'Load...',
    login : 'Login',
    email :'Email',
    group : 'Group',
    language : 'Language',
    status : 'Status',
    title: 'Title',
    datestart: 'Start date',
    dateend: 'End date',
    datecreate: 'Create date',
    category: 'Category',
    categories: 'Categories',
    type: 'Type',
    types: 'Types',
    article: 'Article',
    articles: 'Articles',
    newarticle: 'New article',
    editarticle: 'Edit article',
    miniature: 'Miniature',
    save: 'Save',
    createnewarticle: 'Create a new article',
    activefrom: 'Active from',
    activeto: 'to',
    removeminiature: 'Remove miniature',
    isactive: 'Article is active',
    time: 'Time',
    areyouchangestatus: 'User status will be changed. Are you sure?',
    statusischanged: 'User status is changed',
    deviceisregitered: 'Device successfully registered',
    dviceisrenamed: 'Device successfully renamed',
    suredevremove: 'Device and all linked data will removed. Are you sure? Device serial number:',
    surefileremove: 'Are you sure remove file ',
    surercordremove: 'Are you sure remove record ',
    error: {
        1: 'Autentication Error: Login/Password is not valid',
        2: 'Database Connect Error',
        3: 'Logout error. Session could not be destoryed',
        4: 'Unknown session',
        5: 'Your account has already been activated',
        6: 'Database query error',
        7: 'Account not found',
        8: 'Down with errors',
        9: 'Access denied',
        10: 'Invalid parameter',
        11: 'Data not found',
        12: 'Login is busy',
        13: 'Not all fields are filled in',
        14: 'The device can not be registered',
        15: 'Device not found',
        16: 'Error of saving data',
        17: 'Error of removing data'
    },
    success: {
        16: 'Saved',
        17: 'Removed'
    }
};