<?
session_start();
include_once($_SERVER['DOCUMENT_ROOT'].'/core/config.php');
if (empty($MyDB)) {
    $MyDB = new dbconnect;
}
$MyDB->Connect();
?>
<?if(!empty($_GET['v1'])&&$_GET['v1']=='category'):?>
    <?if(empty($_GET['v2'])):?>
        <?
        $MyDB->Text = "SELECT `id`, `name`
        FROM `article_category`
        WHERE 1 ";
        $MyDB->Query();
        $MyDB->Assoc();
        $jsonData = json_encode($MyDB->Data,JSON_UNESCAPED_UNICODE);
        ?>
    <?else:?>
        <?
        $MyDB->Text = "SELECT a.`id`, 
            `title`, 
            `timestamp`, 
            `datestart`, 
            `dateend`,
            `description`, 
            `content`,
            `image`, 
            `category`,
            ac.`name` AS category_name,
            `type`, 
            atp.`name` AS type_name,
            `language`,
            l.`name` AS language_name,
            l.`code` AS language_code
        FROM `article` a
        LEFT JOIN `article_category` ac
        ON a.`category` = ac.`id`
        LEFT JOIN `article_category` atp
        ON a.`category` = atp.`id`
        LEFT JOIN `language` l
        ON a.`language` = l.`id`
        WHERE a.`status`=1 ";
        if (is_numeric($_GET['v2'])&&is_int($_GET['v2']*1)) {
            $MyDB->Text .= "
            AND `category`='".$_GET['v2']."'";
        }
        else {
            $MyDB->Text .= "
            AND ac.`name`='".$_GET['v2']."'";
        }
        if(!empty($_GET['v3'])) {
            $MyDB->Text .= "
            AND l.`code`='".$_GET['v3']."'";
        }
        $MyDB->Query();
        $MyDB->Assoc();
        if(empty($MyDB->Data)) {
            $jsonData = '{"answer": 0,"error": "11","description": "Data not found"}';
        }
        else {
            $jsonData = json_encode($MyDB->Data,JSON_UNESCAPED_UNICODE);
        }
        ?>
    <?endif;?>
<?elseif(!empty($_GET['v1'])&&$_GET['v1']=='type'):?>
    <?if(empty($_GET['v2'])):?>
        <?
        $MyDB->Text = "SELECT `id`, `name`
        FROM `article_type`
        WHERE 1 ";
        $MyDB->Query();
        $MyDB->Assoc();
        $jsonData = json_encode($MyDB->Data,JSON_UNESCAPED_UNICODE);
        ?>
    <?else:?>
        <?
        $MyDB->Text = "SELECT a.`id`, 
            `title`, 
            `timestamp`, 
            `datestart`, 
            `dateend`, 
            `description`,
            `content`,
            `image`, 
            `category`,
            ac.`name` AS category_name,
            `type`, 
            atp.`name` AS type_name,
            `language`,
            l.`name` AS language_name,
            l.`code` AS language_code
        FROM `article` a
        LEFT JOIN `article_category` ac
        ON a.`category` = ac.`id`
        LEFT JOIN `article_category` atp
        ON a.`category` = atp.`id`
        LEFT JOIN `language` l
        ON a.`language` = l.`id`
        WHERE a.`status`=1 ";
        if (is_numeric($_GET['v2'])&&is_int($_GET['v2']*1)) {
            $MyDB->Text .= "
            AND `type`='".$_GET['v2']."'";
        }
        else {
            $MyDB->Text .= "
            AND atp.`name`='".$_GET['v2']."'";
        }
        if(!empty($_GET['v3'])) {
            $MyDB->Text .= "
            AND l.`code`='".$_GET['v3']."'";
        }
        $MyDB->Query();
        $MyDB->Assoc();
        if(empty($MyDB->Data)) {
            $jsonData = '{"answer": 0,"error": "11","description": "Data not found"}';
        }
        else {
            $jsonData = json_encode($MyDB->Data,JSON_UNESCAPED_UNICODE);
        }
        ?>
    <?endif;?>
<?elseif(!empty($_GET['v1'])&&in_array($_GET['v1'],explode(',',LANGUAGES))):?>
    <?if(!empty($_GET['v2'])&&$_GET['v2']!='date'&&$_GET['v2']!='id'):?>
        <?
        $jsonData = '{"answer": 0,"error": "11","description": "Data not found"}';
        ?>
    <?elseif(!empty($_GET['v3'])&&$_GET['v3']!='asc'&&$_GET['v3']!='desc'):?>
        <?
        $jsonData = '{"answer": 0,"error": "11","description": "Data not found"}';
        ?>
    <?else:?>
        <?
        $MyDB->Text = "SELECT a.`id`, 
            `title`, 
            `timestamp`, 
            `datestart`, 
            `dateend`, 
            `description`,
            `content`,
            `image`, 
            `category`,
            ac.`name` AS category_name,
            `type`, 
            atp.`name` AS type_name,
            `language`,
            l.`name` AS language_name,
            l.`code` AS language_code
        FROM `article` a
        LEFT JOIN `article_category` ac
        ON a.`category` = ac.`id`
        LEFT JOIN `article_category` atp
        ON a.`category` = atp.`id`
        LEFT JOIN `language` l
        ON a.`language` = l.`id`
        WHERE a.`status`=1
        AND l.`code`='".$_GET['v1']."'";
        if (!empty($_GET['v2'])&&($_GET['v2']=='date'||$_GET['v2']=='id')) {
            if($_GET['v2']=='date') {
                $sort = 'datestart`, `timestamp';
            }
            elseif($_GET['v2']=='id') {
                $sort = 'id';
            }
            $MyDB->Text .= "
            ORDER BY `".$sort."`";
            if(!empty($_GET['v3'])&&($_GET['v3']=='asc'||$_GET['v3']=='desc')) {
                $MyDB->Text .= " ".strtoupper($_GET['v3']);
            }
        }
        $MyDB->Query();
        $MyDB->Assoc();
        if(empty($MyDB->Data)) {
            $jsonData = '{"answer": 0,"error": "11","description": "Data not found"}';
        }
        else {
            $jsonData = json_encode($MyDB->Data,JSON_UNESCAPED_UNICODE);
        }
        ?>
    <?endif;?>
<?elseif(!empty($_GET['v1'])&&is_numeric($_GET['v1'])&&is_int($_GET['v1']*1)):?>
    <?if(!empty($_GET['v2'])):?>
        <?
        $jsonData = '{"answer": 0,"error": "11","description": "Data not found"}';
        ?>
    <?else:?>
        <?
        $MyDB->Text = "SELECT a.`id`, 
            `title`, 
            `timestamp`, 
            `datestart`, 
            `dateend`, 
            `description`,
            `content`,
            `image`, 
            `category`,
            ac.`name` AS category_name,
            `type`, 
            atp.`name` AS type_name,
            `language`,
            l.`name` AS language_name,
            l.`code` AS language_code
        FROM `article` a
        LEFT JOIN `article_category` ac
        ON a.`category` = ac.`id`
        LEFT JOIN `article_category` atp
        ON a.`category` = atp.`id`
        LEFT JOIN `language` l
        ON a.`language` = l.`id`
        WHERE a.`status`=1 
        AND a.`id`='".$_GET['v1']."'";
        $MyDB->Query();
        $MyDB->Assoc();
        if(empty($MyDB->Data)) {
            $jsonData = '{"answer": 0,"error": "11","description": "Data not found"}';
        }
        else {
            $jsonData = json_encode($MyDB->Data,JSON_UNESCAPED_UNICODE);
        }
        ?>
    <?endif;?>
<?elseif(!empty($_GET['v1'])&&($_GET['v1']=='date'||$_GET['v1']=='id')):?>
    <?if(!empty($_GET['v2'])&&$_GET['v2']!='asc'&&$_GET['v2']!='desc'):?>
        <?
        $jsonData = '{"answer": 0,"error": "11","description": "Data not found"}';
        ?>
    <?else:?>
        <?
        if($_GET['v1']=='date') {
            $sort = 'datestart`, `timestamp';
        }
        elseif($_GET['v1']=='id') {
            $sort = 'id';
        }
        $MyDB->Text = "SELECT a.`id`, 
            `title`, 
            `timestamp`, 
            `datestart`, 
            `dateend`, 
            `description`,
            `content`,
            `image`, 
            `category`,
            ac.`name` AS category_name,
            `type`, 
            atp.`name` AS type_name,
            `language`,
            l.`name` AS language_name,
            l.`code` AS language_code
        FROM `article` a
        LEFT JOIN `article_category` ac
        ON a.`category` = ac.`id`
        LEFT JOIN `article_category` atp
        ON a.`category` = atp.`id`
        LEFT JOIN `language` l
        ON a.`language` = l.`id`
        ORDER BY `".$sort."`
        WHERE a.`status`=1 ";
        if (!empty($_GET['v2'])&&($_GET['v2']=='asc'||$_GET['v2']==='desc')) {
            $MyDB->Text .= " ".strtoupper($_GET['v2']);
        }
        $MyDB->Query();
        $MyDB->Assoc();
        if(empty($MyDB->Data)) {
            $jsonData = '{"answer": 0,"error": "11","description": "Data not found"}';
        }
        else {
            $jsonData = json_encode($MyDB->Data,JSON_UNESCAPED_UNICODE);
        }
        ?>
    <?endif;?>
<?elseif(!empty($_GET['v1'])):?>
    <?
    $jsonData = '{"answer": 0,"error": "11","description": "Data not found"}';
    ?>
<?else:?>
    <?
    $MyDB->Text = "SELECT a.`id`, 
        `title`, 
        `timestamp`, 
        `datestart`, 
        `dateend`, 
        `description`,
        `content`,
        `image`, 
        `category`,
        ac.`name` AS category_name,
        `type`, 
        atp.`name` AS type_name,
        `language`,
        l.`name` AS language_name,
        l.`code` AS language_code
    FROM `article` a
    LEFT JOIN `article_category` ac
    ON a.`category` = ac.`id`
    LEFT JOIN `article_category` atp
    ON a.`category` = atp.`id`
    LEFT JOIN `language` l
    ON a.`language` = l.`id`
    WHERE a.`status`=1 ";
    $MyDB->Query();
    $MyDB->Assoc();
    if(empty($MyDB->Data)) {
        $jsonData = '{"answer": 0,"error": "11","description": "Data not found"}';
    }
    else {
        $jsonData = json_encode($MyDB->Data,JSON_UNESCAPED_UNICODE);
    }
    ?>
<?endif;?>
<?=$jsonData;?>