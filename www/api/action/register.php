<?
//print_r($_POST);
$error = 'ERROR';
if(empty($_POST['login'])) {
    $error_text = 'EMPTY_LOGIN';
}
elseif(empty($_POST['password'])) {
    $error_text = 'EMPTY_PASS';
}
elseif($_POST['password']!=$_POST['re-password']) {
    $error_text = 'PASS_NOT_MATCH';
}
elseif(empty($_POST['email'])) {
    $error_text = 'EMPTY_EMAIL';
}
else {
    $postdata = http_build_query(
        array(
            'response' => $_POST['g-recaptcha-response'],
            'secret' => '6LfC2ycUAAAAAGhBsattlJ2CisD0PVz0QZ4UHsDu'
        )
    );
    
    $opts = array('http' =>
        array(
            'method'  => 'POST',
            'header'  => 'Content-type: application/x-www-form-urlencoded',
            'content' => $postdata
        )
    );
    
    $context  = stream_context_create($opts);
    
    $result = file_get_contents('https://www.google.com/recaptcha/api/siteverify', false, $context);
    
    $final = json_decode($result,true);
    if ($final['success']) {
        $error = 'REGISTER_COMPLET';
        $error_text = 'REG_COMPLET_TEXT';
    }
    else {
        $error_text = 'SEC_CHECK_FAILED';
    }
}
$error = strtolower($error);
$error_text = strtolower($error_text);
header('Location: /registration/finish/?result='.$error.'&extra='.$error_text);
?>