<?
include_once($_SERVER['DOCUMENT_ROOT'].'/core/config.php');
if (empty($MyDB)) {
    $MyDB = new dbconnect;
}
$MyDB->Connect();
$answer = 1;
$phonesStr = '';
$needUpd = '0';
$updVersion = '0';
?>
<?if(!empty($_REQUEST['action'])):?>
<?



?>
<?exit;?>
<?endif;?>

<?if(!empty($_REQUEST['data'])):?>
    <?
    $dataArr = explode(';',$_REQUEST['data']);
    ?>
    <?if(count($dataArr)==15):?>
        <?
        $device = $dataArr[0];
        $skey = $dataArr[1];
        $deviceId = checkDevice($device,$skey);
        ?>
        <?if($deviceId&&$deviceId>0):?>
            <?
            $userId = getUserByDeviceId($deviceId);
            ?>
            <?if($userId&&$userId>0):?>
                <?
                $dateTimeArr = explode(':',$dataArr[3]);
                $dateTime = '20'.$dateTimeArr[0].'-'.$dateTimeArr[1].'-'.$dateTimeArr[2].' '.$dateTimeArr[3].':'.$dateTimeArr[4].':'.$dateTimeArr[5];
                $insertText = "INSERT INTO `device_state`
                (
                    `user`,
                    `device`,
                    `error`,
                    `datetime`,
                    `gps`,
                    `device_charge`,
                    `car_charge`,
                    `current_temp`,
                    `fire_temp`,
                    `temp_speed`,
                    `car_speed`,
                    `accel_data`,
                    `gyro_data`,
                    `mag_data`,
                    `version`
                )
                VALUES (
                    '".$userId."',
                    '".$deviceId."',
                    '".$dataArr[2]."',
                    '".$dateTime."',
                    '".$dataArr[4]."',
                    '".$dataArr[5]."',
                    '".$dataArr[6]."',
                    '".$dataArr[7]."',
                    '".$dataArr[8]."',
                    '".$dataArr[9]."',
                    '".$dataArr[10]."',
                    '".$dataArr[11]."',
                    '".$dataArr[12]."',
                    '".$dataArr[13]."',
                    '".$dataArr[14]."'
                )";
                $MyDB->Text = $insertText;
                $MyDB->Query();
                $MyDB->ErrorNum();
                if (empty($MyDB->Error)) {
                    //echo "OK";
                    $answer = 0;
                    $MyDB->Text = "SELECT `id`, `value`
                    FROM `distribution_param`
                    WHERE `user`='".$userId."'
                    AND `device` = '".$deviceId."'
                    AND (`is_email` IS NULL OR `is_email`='0')
                    ORDER BY `timestamp` DESC
                    LIMIT 3";
                    $MyDB->Query();
                    $MyDB->Assoc();
                    $phoneArr = $MyDB->Data;
                    $p = 0;
                    foreach ($phoneArr as $pk => $phone) {
                        if($p>0) {
                            $phonesStr .= ':';
                        }
                        $phonesStr .= empty($phone['value']) ? 0 : $phone['value'];
                        $p++;
                    }
                    $MyDB->Text = "SELECT `need_update`
                    FROM `devices`
                    WHERE `id`='".$deviceId."'";
                    $MyDB->Query();
                    $MyDB->Assoc();
                    if (!empty($MyDB->Data)) {
                        $needUpd = $MyDB->Data[0]['need_update'];
                        if ($needUpd>0&&$needUpd>$dataArr[14]) {
                            $updVersion = $needUpd;
                        }
                    }
                }
                else {
                    //echo "Database error";
                    $answer = 4;
                }
                //echo '<pre>'.$insertText.'</pre>';
                //$fgc = file_get_contents(ROOT_PATH.'/software/test02.hex');
                //echo $fgc;
                ?>
            <?else:?>
                <?
                $answer = 3;
                ?>
            <?endif;?>
        <?else:?>
            <?
            $answer = 2;
            ?>
        <?endif;?>
    <?else:?>
    <?
    $answer = 1;
    ?>
    
    <?endif;?>

<?elseif(!empty($_REQUEST['states'])):?>
    <?
    $dataArr = explode(';',$_REQUEST['states']);
    ?>
    <?//if(count($dataArr)==23):?>
        <?
        $device = $dataArr[0];
        $skey = $dataArr[1];
        $deviceId = checkDevice($device,$skey);
        ?>
        <?if($deviceId&&$deviceId>0):?>
            <?
            $userId = getUserByDeviceId($deviceId);
            ?>
            <?if($userId&&$userId>0):?>
                <?
                $finishData = array();
                /*foreach ($dataArr as $dak => $dataValue) {
                    $finishData[$dk]
                }*/
                for ($d=0;$d<23;$d++) {
                    if (empty($dataArr[$d])) {
                        //$finishData[$d] = "NULL";
                        $finishData[$d] = "'0'";
                    }
                    else {
                        $finishData[$d] = "'".$dataArr[$d]."'";
                    }
                }
                $dateTimeArr = explode(':',$dataArr[3]);
                $dateTime = '20'.$dateTimeArr[0].'-'.$dateTimeArr[1].'-'.$dateTimeArr[2].' '.$dateTimeArr[3].':'.$dateTimeArr[4].':'.$dateTimeArr[5];
                $insertText = "INSERT INTO `diagnostic_result`
                (
                    `user`,
                    `device`,
                    `micConnectionFailure`,
                    `micFailure`,
                    `rightSpeakerFailure`,
                    `leftSpeakerFailure`,
                    `speakersFailure`,
                    `ignitionLineFailure`,
                    `uimFailure`,
                    `statuslndicatorFailure`,
                    `batteryFailure`,
                    `batteryVoltageLow`,
                    `crashSensorFailure`,
                    `firmwarelmageCorruption`,
                    `commModulelnterfaceFailure`,
                    `gnssReceiverFailure`,
                    `raimProblem`,
                    `gnssAntennaFailure`,
                    `commModuleFailure`,
                    `eventsMemoryOverflow`,
                    `crashProfileMemoryOverflow`,
                    `otherCriticalFailires`,
                    `otherNotCriticalFailures`
                )
                VALUES (
                    '".$userId."',
                    '".$deviceId."',
                    ".$finishData[2].",
                    ".$finishData[3].",
                    ".$finishData[4].",
                    ".$finishData[5].",
                    ".$finishData[6].",
                    ".$finishData[7].",
                    ".$finishData[8].",
                    ".$finishData[9].",
                    ".$finishData[10].",
                    ".$finishData[11].",
                    ".$finishData[12].",
                    ".$finishData[13].",
                    ".$finishData[14].",
                    ".$finishData[15].",
                    ".$finishData[16].",
                    ".$finishData[17].",
                    ".$finishData[18].",
                    ".$finishData[19].",
                    ".$finishData[20].",
                    ".$finishData[21].",
                    ".$finishData[22]."
                )";
                $MyDB->Text = $insertText;
                $MyDB->Query();
                $MyDB->ErrorNum();
                if (empty($MyDB->Error)) {
                    //echo "OK";
                    $answer = 0;
                    /*$MyDB->Text = "SELECT `id`, `value`
                    FROM `diagnostic_result`
                    WHERE `user`='".$userId."'
                    AND `device` = '".$deviceId."'
                    AND (`is_email` IS NULL OR `is_email`='0')
                    ORDER BY `timestamp` DESC
                    LIMIT 3";
                    $MyDB->Query();
                    $MyDB->Assoc();
                    $phoneArr = $MyDB->Data;
                    $p = 0;
                    foreach ($phoneArr as $pk => $phone) {
                        if($p>0) {
                            $phonesStr .= ':';
                        }
                        $phonesStr .= empty($phone['value']) ? 0 : $phone['value'];
                        $p++;
                    }
                    $MyDB->Text = "SELECT `need_update`
                    FROM `devices`
                    WHERE `id`='".$deviceId."'";
                    $MyDB->Query();
                    $MyDB->Assoc();
                    if (!empty($MyDB->Data)) {
                        $needUpd = $MyDB->Data[0]['need_update'];
                        if ($needUpd>0&&$needUpd>$dataArr[14]) {
                            $updVersion = $needUpd;
                        }
                    }*/
                }
                else {
                    //echo "Database error";
                    $answer = 4;
                }
                //echo '<pre>'.$insertText.'</pre>';
                //$fgc = file_get_contents(ROOT_PATH.'/software/test02.hex');
                //echo $fgc;
                ?>
            <?else:?>
                <?
                $answer = 3;
                ?>
            <?endif;?>
        <?else:?>
            <?
            $answer = 2;
            ?>
        <?endif;?>
    <?//else:?>
    <?
    //$answer = 1;
    ?>
    
    <?//endif;?>
<?elseif (!empty($_REQUEST['message'])):?>
    <?
    //print_r($_REQUEST);
    $messageArr = explode(';',$_REQUEST['message']);
    $device = array_shift($messageArr);
    $skey = array_shift($messageArr);
    $deviceId = checkDevice($device,$skey);
    $strNumber = array_shift($messageArr);
    //echo $strNumber;
    //print_r($messageArr);
    ?>
     <?if($deviceId&&$deviceId>0):?>
        <?
        $userId = getUserByDeviceId($deviceId);
        ?>
        <?if($userId&&$userId>0):?>
            <?
            /*
            $dateTimeArr = explode(':',$dataArr[3]);
            $dateTime = '20'.$dateTimeArr[0].'-'.$dateTimeArr[1].'-'.$dateTimeArr[2].' '.$dateTimeArr[3].':'.$dateTimeArr[4].':'.$dateTimeArr[5];
            */
            $quantityParam = 4;
            $paramArr = array();
            for ($i=0;$i<$strNumber;$i++) {
                /*$min = $i*$quantityParam;
                $max = $i*$quantityParam+$quantityParam-1;*/
                for ($j=0;$j<$quantityParam;$j++) {
                    $k = $i*$quantityParam+$j;
                    $paramArr[$i][$j] = $messageArr[$k];
                }
                //$paramArr[$i] = 
            }
            //print_r($paramArr);
            $insertData = '';
            $l = 0;
            foreach ($paramArr as $pk => $param) {
                if($l>0) {
                    $insertData .= ',';
                }
                $status = 1;
                if (empty($param[3])) {
                    $status = 0;
                }
                $dateTimeArr = explode(':',$param[1]);
                $dateTime = '20'.$dateTimeArr[0].'-'.$dateTimeArr[1].'-'.$dateTimeArr[2].' '.$dateTimeArr[3].':'.$dateTimeArr[4].':'.$dateTimeArr[5];
                $insertData .= '(';
                $insertData .= "'".$userId."','".$deviceId."','".$param[0]."','".$dateTime."','".$param[2]."','".$param[3]."','".$status."'";
                $insertData .= ')';
                $l++;
            }
            if ($insertData != '') {
                $insertText = '';
                $insertText .=  "INSERT INTO `devicemessage`
                (`user`, `device`, `error_code`, `datetime`, `gps`, `text`, `status`)
                VALUES 
                ";
                $insertText .= $insertData;
                $MyDB->Text = $insertText;
                $MyDB->Query();
                $MyDB->ErrorNum();
                if (empty($MyDB->Error)) {
                    $answer = 0;
                }
                else {
                    $answer = 4;
                }
            }
            //echo $insertText;
            ?>
        <?else:?>
            <?
            $answer = 3;
            ?>
        <?endif;?>
    <?else:?>
        <?
        $answer = 2;
        ?>
    <?endif;?>
<?endif;?>
<?
if ($answer==0&&empty($_REQUEST['message'])&&empty($_REQUEST['states'])) {
    $fullAnswer = $answer.';'.$updVersion.';'.$phonesStr;
}
else {
    $fullAnswer = $answer;
}
echo $fullAnswer;
?>