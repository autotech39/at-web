<?
include_once($_SERVER['DOCUMENT_ROOT'].'/core/config.php');
$actionList = array('DIAGNOSTIC_RESULT_ADD','DEVICE_MESSAGE_ADD','DISTRIBUTION_PARAM_GET');
if (empty($MyDB)) {
    $MyDB = new dbconnect;
}
$MyDB->Connect();
?>
<?if(!empty($_REQUEST['data'])):?>
    <?
    //var_dump($_REQUEST['data']);
    $dataArr = json_decode($_REQUEST['data'],ASSOC);
    //print_r($dataArr);
    ?>
    <?if(empty($dataArr['device'])||empty($dataArr['deviceKey'])):?>
Unknown device
    <?else:?>
        <?
        $deviceId = checkDevice($dataArr['device'],$dataArr['deviceKey']);
        ?>
        <?if($deviceId&&$deviceId>0):?>
            <?
            $userId = getUserByDeviceId($deviceId);
            ?>
            <?if($userId&&$userId>0):?>
                <?
                if(in_array($dataArr['type'],$actionList)) {
                    $isInclude = true;
                    if(file_exists('action/'.strtolower($dataArr['type']).'.php')) {
                        include('action/'.strtolower($dataArr['type']).'.php');
                    }
                }
                ?>
            <?else:?>
Device is not register
            <?endif;?>
        <?else:?>
Unknown device
        <?endif;?>
    <?endif;?>
<?else:?>
Invalid data\n

<?endif;?>