<?
class RegAuth {
	public $login;
	public $email;
	public $pass;
	public $repass;
	public $user = array();
	private $crypt_pass;
	//private $referer;
	//public $modules_path = '/modules';
	//public $this_module_path = '/regauth';
	//public $in = 'login/';
	//public $out = 'logout/';
	//public $reg = 'registration/';
	//public $pasr = 'passrestore/';
	//public $passres = '';
	//public $register = '';
	private $logfolder = 'regauth';
	private $logfile = 'userauth';
	private $cryptSymbols;
    private $requestCode;
	private $accountString;
	public $answer = '';
	private $userGroup = 1;
	public $accountActivatePage = 'http://'.SITE_URL.'/registration/confirm';
	public $passwordResetPage = 'http://'.SITE_URL.'/passforget/reset';
	private $reservWorts = array('login','email','password','language','status','group','groupname','request','admin','administrator','system','sysadmin','root','localhost','database','path','param','set','get','article','articles','category','categories','type','types');
    
    function __construct() {
        $symb = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz!@#$%&*?_';
        $sln = strlen($symb)-1;
        $this->cryptSymbols = substr($symb, mt_rand(0,$sln),1).substr($symb, mt_rand(0,$sln),1);
        $this->requestCode = dechex(time()*mt_rand(1000000001,9999999999));
		$this->requestCode .= crypt($this->requestCode,$this->cryptSymbols);
    }
    
	private function ActivateString($code=0,$login=0,$email=0) {
		if (empty($code)) {
			$code=$this->requestCode;
		}
		if (empty($login)) {
			$login=$this->login;
		}
		if (empty($email)) {
			$email=$this->email;
		}
		$str = $login.'#^'.$code.'#^'.$email;
		$this->accountString = base64_encode($str);
	}
	public function SignIn($remember=0,$email_login=0) {
		if (empty($DataBase)) {
			$DataBase = new dbconnect;
			$DataBase->Connect();
		}
		$DataBase->Text = "SELECT  `sys_user`.`id`, `sys_user`.`name`, `sys_user`.`password`, `language`.`code` AS lng FROM `sys_user` LEFT JOIN `language` ON `sys_user`.`language` = `language`.`id` WHERE `sys_user`.`status`=1 AND ";
		if ($email_login==1) {
			$DataBase->Text .= " `sys_user`.`email`='".$this->email."'";
		}
		else {
			$DataBase->Text .= " `sys_user`.`name`='".$this->login."'";
		}
		$DataBase->Query();
		$DataBase->Assoc();
		if (empty($DataBase->Data)) {
			writeLog($this->logfile,'Login error. Unknown user',$this->logfolder);
			$this->answer = '{"answer": 0, "error": "1", "description": "Login/Password is not valid"}';
			return false;
		}
		else {
			$user_array = $DataBase->Data;
			if($user_array[0]['password'] == crypt($this->pass,$user_array[0]['password'])) {
				$_SESSION['USER_ID'] = $user_array[0]['id'];
				$_SESSION['USER_NAME'] = $user_array[0]['name'];
				$_SESSION['LNG'] = $user_array[0]['lng'];
				//$_SESSION['USER_GROUP'] = $user_array[0]['group'];
				$DataBase->Text = "SELECT `id`,`name` FROM `sys_user_group` WHERE `id` = (SELECT `group` FROM `sys_user_group_join` WHERE `user`='".$user_array[0]['id']."' LIMIT 1)";
				$DataBase->Query();
				$DataBase->Assoc();
				//$group_array = $DataBase->Data;
				foreach ($DataBase->Data as $gk => $group_array) {
					$_SESSION['USER_GROUP_NAME'][$gk] = $group_array['name'];
					$_SESSION['USER_GROUP'][$gk] = $group_array['id'];
				}
				//$_SESSION['USER_GROUP_NAME'] = $group_array[0]['name']
				if ($remember==1) {
					setcookie('USER_ID',$user_array[0]['id'],time()+COOKIE_TIME,"/", ".".$_SERVER['SERVER_NAME']);
					setcookie('USER_NAME',$user_array[0]['name'],time()+COOKIE_TIME,"/", ".".$_SERVER['SERVER_NAME']);
					setcookie('USER_KEY',crypt($user_array[0]['password'],$user_array[0]['password']),time()+COOKIE_TIME,"/", ".".$_SERVER['SERVER_NAME']);
					//$_COOKIE['USER_NAME'] = $user_array[0]['name'];
				}
				/*$symb = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz!@#$%&*?_';
				$sln = strlen($symb)-1;
				$this->cryptSymbols = substr($symb, mt_rand(0,$sln),1).substr($symb, mt_rand(0,$sln),1);
				$reqCode = dechex(time()*mt_rand(1000000001,9999999999));
				$reqCode .= crypt($reqCode,$rSymb);*/
				$userType = implode(',',$_SESSION['USER_GROUP']);
				$DataBase->Text = "UPDATE `sys_user` SET `request_code`= '".$this->requestCode."' WHERE `id`='".$user_array[0]['id']."'";
				$DataBase->Query();
				writeLog($this->logfile,'Success login. User ID: '.$user_array[0]['id'],$this->logfolder);
				$this->answer = '{"answer": 1, "user": "'.$user_array[0]['id'].'", "type": "'.$userType.'", "name": "'.$user_array[0]['name'].'", "session": "'.session_id().'","language":"'.$user_array[0]['lng'].'"}';
				return true;
				//header('location:'.$_SERVER['HTTP_REFERER']);
			}
			else {
				writeLog($this->logfile,'Login error. Password error. User ID: '.$user_array[0]['id'],$this->logfolder);
				$this->answer = '{"answer": 0, "error": "1", "description": "Login/Password is not valid"}';
				return false;
			}
		}
	}
	public function AutoSignIn() {
		if (!empty($_COOKIE['USER_ID'])&&!empty($_COOKIE['USER_NAME'])&&!empty($_COOKIE['USER_KEY'])) {
			if (empty($DataBase)) {
				$DataBase = new dbconnect;
				$DataBase->Connect();
			}
			$DataBase->Text = "SELECT  `sys_user`.`id`, `sys_user`.`name`, `sys_user`.`password`, `language`.`code` AS lng FROM `sys_user` LEFT JOIN `language` ON `sys_user`.`language` = `language`.`id` WHERE `sys_user`.`status`=1 AND  `sys_user`.`name`='".$_COOKIE['USER_NAME']."' AND `sys_user`.`id`='".$_COOKIE['USER_ID']."'";
			$DataBase->Query();
			$DataBase->Assoc();
			if (empty($DataBase->Data)) {
				writeLog($this->logfile,'Error of auto sign in. Database error',$this->logfolder);
				$this->answer = '{"answer": 0, "error": "1", "description": "Login/Password is not valid"}';
				return false;
			}
			else {
				$user_array = $DataBase->Data;
				if(crypt($user_array[0]['password'],$user_array[0]['password']) == $_COOKIE['USER_KEY']) {
					$_SESSION['USER_ID'] = $user_array[0]['id'];
					$_SESSION['USER_NAME'] = $user_array[0]['name'];
					$_SESSION['LNG'] = $user_array[0]['lng'];
					//$_SESSION['USER_GROUP'] = $user_array[0]['group'];
					$DataBase->Text = "SELECT `id`,`name` FROM `sys_user_group` WHERE `id` IN (SELECT `group` FROM `sys_user_group_join` WHERE `user`='".$user_array[0]['id']."')";
					$DataBase->Query();
					$DataBase->Assoc();
					//$group_array = $DataBase->Data;
					foreach ($DataBase->Data as $gk => $group_array) {
						$_SESSION['USER_GROUP_NAME'][$gk] = $group_array['name'];
						$_SESSION['USER_GROUP'][$gk] = $group_array['id'];
					}
					//$_SESSION['USER_GROUP_NAME'] = $group_array[0]['name']
					if ($remember==1) {
						setcookie('USER_ID',$user_array[0]['id'],time()+COOKIE_TIME,"/", ".".$_SERVER['SERVER_NAME']);
						setcookie('USER_NAME',$user_array[0]['name'],time()+COOKIE_TIME,"/", ".".$_SERVER['SERVER_NAME']);
						setcookie('USER_KEY',crypt($user_array[0]['password'],$user_array[0]['password']),time()+COOKIE_TIME,"/", ".".$_SERVER['SERVER_NAME']);
						//$_COOKIE['USER_NAME'] = $user_array[0]['name'];
					}
					/*$symb = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz!@#$%&*?_';
					$sln = strlen($symb)-1;
					$rSymb = substr($symb, mt_rand(0,$sln),1).substr($symb, mt_rand(0,$sln),1);
					$reqCode = dechex(time()*mt_rand(1000000001,9999999999));
					$reqCode .= crypt($reqCode,$rSymb);**/
					$userType = implode(',',$_SESSION['USER_GROUP']);
					$DataBase->Text = "UPDATE `sys_user` SET `request_code`= '".$this->requestCode."' WHERE `id`='".$user_array[0]['id']."'";
					$DataBase->Query();
					writeLog($this->logfile,'Auto sign in (1). User ID: '.$user_array[0]['id'],$this->logfolder);
					$this->answer = '{"answer": 1, "user": "'.$user_array[0]['id'].'", "type": "'.$userType.'", "name": "'.$user_array[0]['name'].'", "session": "'.session_id().'","language":"'.$user_array[0]['lng'].'"}';
					return true;
				}
				else {
					writeLog($this->logfile,'Auto sign in (2). User ID: '.$user_array[0]['id'],$this->logfolder);
					$this->answer = '{"answer": 0, "error": "1", "description": "Login/Password is not valid"}';
					return false;
				}
			}
		}
		else {
			if (!empty($_SESSION['USER_ID'])) {
				writeLog($this->logfile,'Auto sign in (3). User ID: '.$_SESSION['USER_ID'],$this->logfolder);
				$userType = implode(',',$_SESSION['USER_GROUP']);
				$this->answer = '{"answer": 1, "user": "'.$_SESSION['USER_ID'].'", "type": "'.$userType.'", "name": "'.$_SESSION['USER_NAME'].'", "session": "'.session_id().'","language":"'.$_SESSION['LNG'].'"}';
			}
			return false;
		}
	}
	public function SignOut() {
		$userId = $_SESSION['USER_ID'];
		setcookie('USER_ID','',time()-100000,"/", ".".$_SERVER['SERVER_NAME']);
		setcookie('USER_NAME','',time()-100000,"/", ".".$_SERVER['SERVER_NAME']);
		setcookie('USER_KEY','',time()-100000,"/", ".".$_SERVER['SERVER_NAME']);
		unset($_COOKIE);
		unset($_SESSION);
		session_destroy();
		if(empty($_SESSION)) {
			$this->answer = '{"answer": 1,"user": "null","type": "null","name": "null","session": "'.session_id().'"}';
		}
		else {
			$this->answer = '{"answer": 0,"error": "3","description": "Logout error. Session could not be destoryed"}';
		}
		writeLog($this->logfile,'Logout. User ID: '.$userId,$this->logfolder);
		//header('Location: http://'.SITE_URL.'/'.MODULE_PATH.'/regauth/out.php');
		//exit;
	}
	public function GetUser() {
		$user_array['ID'] = !empty($_SESSION['USER_ID']) ? $_SESSION['USER_ID'] : '';
		$user_array['GROUP'] = !empty($_SESSION['USER_GROUP']) ? $_SESSION['USER_GROUP'] : '';
		$user_array['GROUP_NAME'] = !empty($_SESSION['USER_GROUP_NAME']) ? $_SESSION['USER_GROUP_NAME'] : '';
		$user_array['NAME'] = !empty($_SESSION['USER_NAME']) ? $_SESSION['USER_NAME'] : '';
		$user_array['EMAIL'] = !empty($_SESSION['USER_EMAIL']) ? $_SESSION['USER_EMAIL'] : '';
		$user_array['FIRSTNAME'] = !empty($_SESSION['USER_FIRSTNAME']) ? $_SESSION['USER_FIRSTNAME'] : '';
		$user_array['LASTNAME'] = !empty($_SESSION['USER_LASTNAME']) ? $_SESSION['USER_LASTNAME'] : '';
		if (empty($user_array['ID'])) {
			return false;
		}
		else {
			$this->user = $user_array;
			return $this->user;
		}
	}
	public function CheckUser($id=0) {
		if ($id==0) {
			$chk = empty($_SESSION['USER_ID']) ? false : true;
		}
		else {
			if (empty($_SESSION['USER_ID'])) {
				$chk = false;
			}
			else {
				$chk = ($_SESSION['USER_ID'] == $id) ? true : false;
			}
		}
		return $chk;
	}
	public function CheckGroup($group) {
		if (empty($_SESSION['USER_GROUP'])) {
			$chk = false;
		}
		else {
			$chk = in_array($group, $_SESSION['USER_GROUP']) ? true : false;
		}
		return $chk;
	}
	public function CheckUserData($value,$param='name') {
		if($param!='email') {
			$param = 'name';
		}
		if (empty($DataBase)) {
			$DataBase = new dbconnect;
			$DataBase->Connect();
		}
		$DataBase->Text = "SELECT `id` FROM `sys_user`  WHERE  `".$param."`='".$value."'";
		$DataBase->Query();
		$DataBase->Assoc();
		$result = false;
		if(!empty($DataBase->Data)&&$DataBase->Data[0]['id']>0) {
			$result = true;
		}
		return $result;
	}
	public function GetUserData($param='name',$value,$full=true) {
		if($param!='email') {
			$param = 'name';
		}
		if (empty($DataBase)) {
			$DataBase = new dbconnect;
			$DataBase->Connect();
		}
		$request = '';
		if ($full) {
			$request = "`request_code`, ";
		}
		$DataBase->Text = "SELECT su.`id`, 
			su.`name` AS \"login\",
			`email`, 
			$request
			`status`, 
			`datetime`, 
			ln.`code` AS \"language\",
			ug.`id` AS \"group\",
			ug.`name` AS \"groupname\"
		FROM `sys_user` su
		LEFT JOIN `language` ln
		ON su.`language` = ln.`id`
		LEFT JOIN `sys_user_group_join` ugj
		ON su.`id` = ugj.`user`
		LEFT JOIN `sys_user_group` ug
		ON ugj.`group` = ug.`id`
		WHERE su.`".$param."`='".$value."'
		LIMIT 1";
		$DataBase->Query();
		$DataBase->Assoc();
		$result = false;
		if(!empty($DataBase->Data)&&$DataBase->Data[0]['id']>0) {
			$result = $DataBase->Data[0];
			$this->answer = json_encode($result);
		}
		else {
			$this->answer = '{"answer":"0","error":"11","description":"Data not found"}';
		}
		return $result;
	}
	public function GetUsersData($param=0) {
		$paramArr = array('login','email','language','group','groupname');
		if (empty($DataBase)) {
			$DataBase = new dbconnect;
			$DataBase->Connect();
		}
		$DataBase->Text = "SELECT su.`id`, 
			su.`name` AS \"login\",
			`email`, 
			`status`, 
			`datetime`, 
			ln.`code` AS \"language\",
			ug.`id` AS \"group\",
			ug.`name` AS \"groupname\"
		FROM `sys_user` su
		LEFT JOIN `language` ln
		ON su.`language` = ln.`id`
		LEFT JOIN `sys_user_group_join` ugj
		ON su.`id` = ugj.`user`
		LEFT JOIN `sys_user_group` ug
		ON ugj.`group` = ug.`id`";
		$DataBase->Query();
		$DataBase->Assoc();
		$result = false;
		if(!empty($DataBase->Data)) {
			if(empty($param)) {
				$result = $DataBase->Data;
				$this->answer = json_encode($result);
			}
			elseif (!in_array($param,$paramArr)) {
				$result = false;
				$this->answer = '{"answer":"0","error":"10","description":"Invalid parameter"}';
			}
			else {
				$result = array();
				$i = 0;
				foreach ($DataBase->Data as $k => $data) {
					$result[$i][$param] = $data[$param];
					$i++;
				}
				$this->answer = json_encode($result);
			}
		}
		else {
			$this->answer = '{"answer":"0","error":"11","description":"Data not found"}';
		}
		return $result;
	}
	public function UserNameByEmail($email) {
		if (empty($DataBase)) {
			$DataBase = new dbconnect;
			$DataBase->Connect();
		}
		$DataBase->Text = "SELECT `name` FROM `sys_user`  WHERE  `email`='".$email."' LIMIT 1";
		$DataBase->Query();
		$DataBase->Assoc();
		$result = false;
		if(!empty($DataBase->Data[0]['name'])) {
			$result = $DataBase->Data[0]['name'];
		}
		return $result;
	}
	public function Registration($username,$email,$password,$sendMail=true) {
		$result = -1;
		if ($this->CheckUserData($username,'name')||in_array($username,$this->reservWorts)) {
			$this->answer = '{"answer": 0,"error": "12","description": "Login is busy"}';
			$result = -2;
		}
		if (empty($DataBase)) {
			$DataBase = new dbconnect;
			$DataBase->Connect();
		}
		$cryptPass = crypt($password,$this->cryptSymbols);
		$DataBase->Text = "INSERT INTO `sys_user` (
			`name`, 
			`password`, 
			`email`, 
			`request_code`, 
			`status`, 
			`language`
		) 
		VALUES (
			'$username',
			'$cryptPass',
			'$email',
			'".$this->requestCode."',
			'0',
			(SELECT `id` FROM `language` WHERE `code` = '".SITE_LANG."' LIMIT 1)
		)";
		$DataBase->Query();
		$DataBase->LastId();
		$DataBase->ErrorNum();
		if (!empty($DataBase->Error)) {
			$this->answer = '{"answer": 0,"error": "6","description": "Database query error: '.$DataBase->Error.'"}';
		}
		else {
			$DataBase->Text = "INSERT INTO `sys_user_group_join` (
				`user`, 
				`group`
			) 
			VALUES (
				'".$DataBase->LastInsertId."',
				'".$this->userGroup."'
			)";
			
			$DataBase->Query();
			$DataBase->ErrorNum();
			if (!empty($DataBase->Error)) {
				$this->answer = '{"answer": 1,"error": "8","description": "Down with errors: '.$DataBase->Error.'","query":"'.$DataBase->Text.'"}';
				
				$result = 0;
			}
			else {
				$this->answer = '{"answer": 1,"error": "0","description": "User is registered"}';
				$result = 1;
			}
			$this->ActivateString($this->requestCode,$username,$email);
			$string = $this->accountString;
			if ($sendMail) {
				$link_src = $this->accountActivatePage.'?st='.$string;
				sendEmail ('register_confirm',array($email),array(),SITE_LANG,$link_src);
			}
		}
		return $result;
		
	}
	public function AccountActivate($string) {
		$return = 0;
		$str = base64_decode($string);
		$arr = explode('#^',$str);
		if (empty($DataBase)) {
			$DataBase = new dbconnect;
			$DataBase->Connect();
		}
		$DataBase->Text = "SELECT  `sys_user`.`id`, `sys_user`.`name`, `sys_user`.`password`, `sys_user`.`status`,  `language`.`code` AS lng FROM `sys_user` LEFT JOIN `language` ON `sys_user`.`language` = `language`.`id` WHERE  `sys_user`.`name`='".$arr[0]."' AND `sys_user`.`email`='".$arr[2]."' AND `sys_user`.`request_code`='".$arr[1]."'";
		$DataBase->Query();
		$DataBase->Assoc();
		if (empty($DataBase->Data)) {
			$this->answer = '{"answer": 0,"error": "7","description": "Account not found"}';
			$return = -2;
		}
		elseif ($DataBase->Data[0]['status']==1) {
			$this->answer = '{"answer": 0,"error": "5","description": "Your account has already been activated"}';
			$return = 0;
		}
		elseif ($DataBase->Data[0]['status']==0) {
			$uid = $DataBase->Data[0]['id'];
			$uname = $DataBase->Data[0]['name'];
			$DataBase->Text = "UPDATE `sys_user` SET `status`=1 WHERE `id`='".$uid."' AND`name`='".$arr[0]."' AND `email`='".$arr[2]."' AND `request_code`='".$arr[1]."'";
			$DataBase->Query();
			$DataBase->ErrorNum();
			if (!empty($DataBase->Error)) {
				$this->answer = '{"answer": 0,"error": "6","description": "Database query error: '.$DataBase->Error.'"}';
				$return = -1;
			}
			else {
				$this->answer = '{"answer": 1,"user": "'.$uid.'","name": "'.$uname.'","event": "Your account is activated"}';
				mkdir(ROOT_PATH."/users/".$arr[2], 0777);
				$return = 1;
			}
		}
		return $return;
	}
	public function GetPasswordRestore($email) {
		//$login = $this->UserNameByEmail($email);
		$userData = $this->GetUserData('email',$email);
		if ($userData['status']==1) {
			$this->ActivateString(0,$userData['login'],$email);
			$string = base64_encode($this->accountString.'@'.$this->cryptSymbols.'@'.$userData['request_code']);
			$link_src = $this->passwordResetPage.'?rp='.$string;
			$link = ' <a href="'.$link_src.'">'.$link_src.'</a>';
			sendEmail ('restore_password',array($email),array(),SITE_LANG,$link);
		}
		else {
			$string = false;
		}
		return $string;
	}
	public function SetPasswordRestore($string,$password) {
		$firstStr = base64_decode($string);
		$firstArr = explode('@',$firstStr);
		$secondStr = base64_decode($firstArr[0]);
		$secondArr = explode('#^',$secondStr);
		//echo $firstStr;
		//echo '<br>';
		//print_r($firstArr);
		//echo '<br>';
		//echo $secondStr;
		//echo '<br>';
		//print_r($secondArr);
		//echo '<br>';
		if (empty($DataBase)) {
			$DataBase = new dbconnect;
			$DataBase->Connect();
		}
		$DataBase->Text = "SELECT  `sys_user`.`id` FROM `sys_user` WHERE  `sys_user`.`name`='".$secondArr[0]."' AND `sys_user`.`email`='".$secondArr[2]."' AND `sys_user`.`request_code`='".$firstArr[2]."'";
		$DataBase->Query();
		$DataBase->Assoc();
		$return = false;
		if(!empty($DataBase->Data[0]['id'])) {
			$uid = $DataBase->Data[0]['id'];
			$cryptPass = crypt($password,$secondArr[1]);
			$DataBase->Text = "UPDATE `sys_user` SET `request_code`='".$firstArr[1]."', `password`='".$cryptPass."' WHERE `id`='".$uid."' AND`name`='".$secondArr[0]."' AND `email`='".$secondArr[2]."'";
			$DataBase->Query();
			$DataBase->ErrorNum();
			if(empty($DataBase->Error)) {
				$return = true;
			}
		}
		return $return;
	}
}
?>